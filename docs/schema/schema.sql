-- phpMyAdmin SQL Dump
-- version 4.9.5deb2~bpo10+1
-- https://www.phpmyadmin.net/
--
-- Počítač: localhost:3306
-- Vytvořeno: Ned 20. zář 2020, 12:40
-- Verze serveru: 10.3.23-MariaDB-0+deb10u1
-- Verze PHP: 7.3.19-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `paro2`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `appeals`
--

CREATE TABLE `appeals` (
  `id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `info_link` varchar(255) DEFAULT NULL COMMENT 'odkaz na informace o výzvě',
  `year` year(4) NOT NULL COMMENT 'ročník',
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='výzvy / vyhlášené ročníky';

-- --------------------------------------------------------

--
-- Struktura tabulky `attachments`
--

CREATE TABLE `attachments` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `attachment_type_id` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `attachment_types`
--

CREATE TABLE `attachment_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `attachment_types`
--

INSERT INTO `attachment_types` (`id`, `name`, `modified`, `created`) VALUES
(1, 'Fotogalerie', '2020-09-20 11:01:00', '2020-09-20 11:01:00'),
(2, 'Ostatní přílohy', '2020-09-20 11:01:00', '2020-09-20 11:01:00');

-- --------------------------------------------------------

--
-- Struktura tabulky `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `identity_provider_id` int(11) DEFAULT NULL,
  `identity_key` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `custom_users`
--

CREATE TABLE `custom_users` (
  `id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL COMMENT 'email, ale nevynucovat',
  `password` varchar(128) NOT NULL COMMENT 'argon2id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='vlastní kmeny uživatelů';

-- --------------------------------------------------------

--
-- Struktura tabulky `custom_users_to_custom_categories`
--

CREATE TABLE `custom_users_to_custom_categories` (
  `id` int(11) NOT NULL,
  `custom_user_id` int(11) NOT NULL,
  `custom_user_category_id` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `custom_user_categories`
--

CREATE TABLE `custom_user_categories` (
  `id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `domains`
--

CREATE TABLE `domains` (
  `id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `identity_providers`
--

CREATE TABLE `identity_providers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='globální dostupné zdroje identit';

-- --------------------------------------------------------

--
-- Struktura tabulky `organizations`
--

CREATE TABLE `organizations` (
  `id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `organizations_to_users`
--

CREATE TABLE `organizations_to_users` (
  `id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `organization_settings`
--

CREATE TABLE `organization_settings` (
  `id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `phases`
--

CREATE TABLE `phases` (
  `id` int(11) NOT NULL,
  `appeal_id` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `minimum_votes_count` int(11) DEFAULT NULL COMMENT 'minimum hlasů pro úspěch v této fázi paro',
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `phase_to_providers`
--

CREATE TABLE `phase_to_providers` (
  `id` int(11) NOT NULL,
  `phase_id` int(11) NOT NULL,
  `identity_provider_id` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='poskytovatelé identit povolení pro danou fázi';

-- --------------------------------------------------------

--
-- Struktura tabulky `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `appeal_id` int(11) NOT NULL,
  `title_image_id` int(11) DEFAULT NULL,
  `proposed_by_contact_id` int(11) NOT NULL,
  `project_status_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `public_interest` text DEFAULT NULL COMMENT 'veřejný přínos projektu',
  `gps_longitude` decimal(9,6) DEFAULT NULL,
  `gps_latitude` decimal(11,8) DEFAULT NULL,
  `address` varchar(255) NOT NULL COMMENT 'adresa místa realizace pro geolokaci (napr. geocoding api)',
  `progress` int(11) NOT NULL DEFAULT 0 COMMENT 'procentni progress projektu 1-100',
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `projects_to_categories`
--

CREATE TABLE `projects_to_categories` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `project_budget_items`
--

CREATE TABLE `project_budget_items` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `is_final` tinyint(1) NOT NULL COMMENT '0 => navrhovaný rozpočet, 1 => rozpočet schválený úřadem',
  `total_price` int(11) NOT NULL,
  `item_price` int(11) NOT NULL,
  `item_count` int(11) NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `project_categories`
--

CREATE TABLE `project_categories` (
  `id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `project_logs`
--

CREATE TABLE `project_logs` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_log_type_id` int(11) NOT NULL,
  `date_when` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `project_log_types`
--

CREATE TABLE `project_log_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `project_log_types`
--

INSERT INTO `project_log_types` (`id`, `name`, `modified`, `created`) VALUES
(1, 'Deník / Historie projektu', '2020-09-20 11:16:51', '2020-09-20 11:16:51'),
(2, 'Posouzení proveditelnosti', '2020-09-20 11:16:51', '2020-09-20 11:16:51');

-- --------------------------------------------------------

--
-- Struktura tabulky `project_status`
--

CREATE TABLE `project_status` (
  `id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_public` tinyint(1) NOT NULL COMMENT 'zda je projekt veřejně dostupný (napr. v galerii)',
  `is_voting_enabled` tinyint(1) NOT NULL COMMENT 'zda je možné o projektu hlasovat (ještě vyžaduje identity_providers u prislusne phase)',
  `is_default_for_proposals` tinyint(1) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL COMMENT 'argon2id',
  `verification_code` varchar(32) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vypisuji data pro tabulku `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `modified`, `created`) VALUES
(1, 'Manažer portálu Participativního Rozpočtu', '2020-09-20 11:48:20', '2020-09-20 11:48:20');

-- --------------------------------------------------------

--
-- Struktura tabulky `votes`
--

CREATE TABLE `votes` (
  `id` int(11) NOT NULL,
  `identity_provider_id` int(11) NOT NULL,
  `phase_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `identity_data` text NOT NULL COMMENT 'plný obsah vrácené identity',
  `identity_key` varchar(255) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `appeals`
--
ALTER TABLE `appeals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Klíče pro tabulku `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `attachment_type_id` (`attachment_type_id`);

--
-- Klíče pro tabulku `attachment_types`
--
ALTER TABLE `attachment_types`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `identity_provider_id` (`identity_provider_id`);
ALTER TABLE `contacts` ADD FULLTEXT KEY `name` (`name`);

--
-- Klíče pro tabulku `custom_users`
--
ALTER TABLE `custom_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniq_org_username` (`organization_id`,`username`),
  ADD KEY `organization_id` (`organization_id`),
  ADD KEY `contact_id` (`contact_id`);

--
-- Klíče pro tabulku `custom_users_to_custom_categories`
--
ALTER TABLE `custom_users_to_custom_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_user_id` (`custom_user_id`),
  ADD KEY `custom_user_category_id` (`custom_user_category_id`);

--
-- Klíče pro tabulku `custom_user_categories`
--
ALTER TABLE `custom_user_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Klíče pro tabulku `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `domain` (`domain`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Klíče pro tabulku `identity_providers`
--
ALTER TABLE `identity_providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Klíče pro tabulku `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `is_enabled` (`is_enabled`),
  ADD KEY `admin_user_id` (`admin_user_id`);

--
-- Klíče pro tabulku `organizations_to_users`
--
ALTER TABLE `organizations_to_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_id` (`organization_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_role_id` (`user_role_id`);

--
-- Klíče pro tabulku `organization_settings`
--
ALTER TABLE `organization_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_id` (`organization_id`),
  ADD KEY `name` (`name`);

--
-- Klíče pro tabulku `phases`
--
ALTER TABLE `phases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appeal_id` (`appeal_id`);

--
-- Klíče pro tabulku `phase_to_providers`
--
ALTER TABLE `phase_to_providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phase_id` (`phase_id`),
  ADD KEY `identity_provider_id` (`identity_provider_id`);

--
-- Klíče pro tabulku `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appeal_id` (`appeal_id`),
  ADD KEY `title_image_id` (`title_image_id`),
  ADD KEY `progress` (`progress`),
  ADD KEY `proposed_by_contact_id` (`proposed_by_contact_id`),
  ADD KEY `project_status_id` (`project_status_id`);

--
-- Klíče pro tabulku `projects_to_categories`
--
ALTER TABLE `projects_to_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Klíče pro tabulku `project_budget_items`
--
ALTER TABLE `project_budget_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Klíče pro tabulku `project_categories`
--
ALTER TABLE `project_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Klíče pro tabulku `project_logs`
--
ALTER TABLE `project_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `date_when` (`date_when`),
  ADD KEY `project_log_type_id` (`project_log_type_id`);

--
-- Klíče pro tabulku `project_log_types`
--
ALTER TABLE `project_log_types`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `project_status`
--
ALTER TABLE `project_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `password` (`password`);

--
-- Klíče pro tabulku `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `identity_provider_id` (`identity_provider_id`),
  ADD KEY `phase_id` (`phase_id`),
  ADD KEY `identity_key` (`identity_key`),
  ADD KEY `project_id` (`project_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `appeals`
--
ALTER TABLE `appeals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `attachment_types`
--
ALTER TABLE `attachment_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `custom_users`
--
ALTER TABLE `custom_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `custom_users_to_custom_categories`
--
ALTER TABLE `custom_users_to_custom_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `custom_user_categories`
--
ALTER TABLE `custom_user_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `domains`
--
ALTER TABLE `domains`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `identity_providers`
--
ALTER TABLE `identity_providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `organizations_to_users`
--
ALTER TABLE `organizations_to_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `organization_settings`
--
ALTER TABLE `organization_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `phase_to_providers`
--
ALTER TABLE `phase_to_providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `projects_to_categories`
--
ALTER TABLE `projects_to_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `project_budget_items`
--
ALTER TABLE `project_budget_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `project_categories`
--
ALTER TABLE `project_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `project_logs`
--
ALTER TABLE `project_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `project_log_types`
--
ALTER TABLE `project_log_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `project_status`
--
ALTER TABLE `project_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pro tabulku `votes`
--
ALTER TABLE `votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `appeals`
--
ALTER TABLE `appeals`
  ADD CONSTRAINT `appeals_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `attachments`
--
ALTER TABLE `attachments`
  ADD CONSTRAINT `attachments_ibfk_1` FOREIGN KEY (`attachment_type_id`) REFERENCES `attachment_types` (`id`) ON UPDATE CASCADE;

--
-- Omezení pro tabulku `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`identity_provider_id`) REFERENCES `identity_providers` (`id`) ON UPDATE CASCADE;

--
-- Omezení pro tabulku `custom_users`
--
ALTER TABLE `custom_users`
  ADD CONSTRAINT `custom_users_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `custom_users_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `custom_users_to_custom_categories`
--
ALTER TABLE `custom_users_to_custom_categories`
  ADD CONSTRAINT `custom_users_to_custom_categories_ibfk_1` FOREIGN KEY (`custom_user_id`) REFERENCES `custom_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `custom_users_to_custom_categories_ibfk_2` FOREIGN KEY (`custom_user_category_id`) REFERENCES `custom_user_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `custom_user_categories`
--
ALTER TABLE `custom_user_categories`
  ADD CONSTRAINT `custom_user_categories_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `domains`
--
ALTER TABLE `domains`
  ADD CONSTRAINT `domains_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_ibfk_1` FOREIGN KEY (`admin_user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Omezení pro tabulku `organizations_to_users`
--
ALTER TABLE `organizations_to_users`
  ADD CONSTRAINT `organizations_to_users_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `organizations_to_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `organizations_to_users_ibfk_3` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`id`) ON UPDATE CASCADE;

--
-- Omezení pro tabulku `organization_settings`
--
ALTER TABLE `organization_settings`
  ADD CONSTRAINT `organization_settings_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `phases`
--
ALTER TABLE `phases`
  ADD CONSTRAINT `phases_ibfk_1` FOREIGN KEY (`appeal_id`) REFERENCES `appeals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `phase_to_providers`
--
ALTER TABLE `phase_to_providers`
  ADD CONSTRAINT `phase_to_providers_ibfk_1` FOREIGN KEY (`identity_provider_id`) REFERENCES `identity_providers` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `phase_to_providers_ibfk_2` FOREIGN KEY (`phase_id`) REFERENCES `phases` (`id`) ON UPDATE CASCADE;

--
-- Omezení pro tabulku `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`appeal_id`) REFERENCES `appeals` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`title_image_id`) REFERENCES `attachments` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `projects_ibfk_3` FOREIGN KEY (`proposed_by_contact_id`) REFERENCES `contacts` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `projects_ibfk_4` FOREIGN KEY (`project_status_id`) REFERENCES `project_status` (`id`) ON UPDATE CASCADE;

--
-- Omezení pro tabulku `projects_to_categories`
--
ALTER TABLE `projects_to_categories`
  ADD CONSTRAINT `projects_to_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `project_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `projects_to_categories_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `project_budget_items`
--
ALTER TABLE `project_budget_items`
  ADD CONSTRAINT `project_budget_items_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `project_categories`
--
ALTER TABLE `project_categories`
  ADD CONSTRAINT `project_categories_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `project_logs`
--
ALTER TABLE `project_logs`
  ADD CONSTRAINT `project_logs_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_logs_ibfk_2` FOREIGN KEY (`project_log_type_id`) REFERENCES `project_log_types` (`id`) ON UPDATE CASCADE;

--
-- Omezení pro tabulku `project_status`
--
ALTER TABLE `project_status`
  ADD CONSTRAINT `project_status_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `votes`
--
ALTER TABLE `votes`
  ADD CONSTRAINT `votes_ibfk_1` FOREIGN KEY (`identity_provider_id`) REFERENCES `identity_providers` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `votes_ibfk_2` FOREIGN KEY (`phase_id`) REFERENCES `phases` (`id`),
  ADD CONSTRAINT `votes_ibfk_3` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
