const postcss = require('postcss');
const prefixer = require('postcss-prefixer');
const fs = require('fs');

const _in = 'bootstrap-4.5.2.min.css'
const _out = 'bootstrap-4.5.2.paro2.min.css'


fs.readFile(_in, (err, css) => {
  postcss([prefixer({prefix:'paro2-',})])
    .process(css, { from: _in, to: _out })
    .then(result => {
      fs.writeFile(_out, result.css, () => true)
      if ( result.map ) {
        fs.writeFile(_out.concat('.map'), result.map, () => true)
      }
    })
})
