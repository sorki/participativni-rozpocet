;(function ($, window, document, undefined) {

    "use strict";

    // polyfills

    if (!String.prototype.includes) {
        String.prototype.includes = function (search, start) {
            'use strict';

            if (search instanceof RegExp) {
                throw TypeError('first argument must not be a RegExp');
            }
            if (start === undefined) {
                start = 0;
            }
            return this.indexOf(search, start) !== -1;
        };
    }

    let constants = {
        API_VERSION: 'v1',
        API_URL_EL_ORGANIZATION_ID: '{organization_id}',
        API_URL_EL_APPEAL_ID: '{appeal_id}',
        OUTPUT_MODE_PROJECTS: 'projects',
        OUTPUT_MODE_SCHEDULE: 'schedule',
        OUTPUT_MODE_PROJECT_DETAIL: 'project_detail',
        TEXT_PARTICIPATORY_BUDGET: ' - Galerie projektů',
        TEXT_NO_PROJECTS_AVAILABLE: 'Tento ročník neobsahuje žádné projekty',
        TEXT_NO_PROJECTS_FOUND: 'Parametrům vyhledávání neodpovídá žádný projekt',
        TEXT_ACTION_PROPOSE_PROJECT: 'Navrhnout projekt',
        TEXT_ACTION_SHOW_SCHEDULE: 'Harmonogram',
        TEXT_ACTION_SHOW_PROJECTS: 'Projekty',
        TEXT_LABEL_ORGANIZATIONS: 'Organizace:',
        TEXT_LABEL_APPEALS: 'Ročníky:',
        TEXT_LABEL_CATEGORIES: 'Kategorie:',
        TEXT_LABEL_PROJECT_STATES: 'Stav projektu:',
        TEXT_LABEL_SEARCH: 'Klíčová slova:',
        TEXT_SCHEDULE_CURRENT_PHASE: 'Právě probíhá',
        TEXT_SCHEDULE_VOTING_ENABLED: 'Hlasováním lze podpořit projekty',
        TEXT_SCHEDULE_PROPOSALS_ENABLED: 'Lze zasílat návrhy na projekty',
        TEXT_SELECT_PLACEHOLDER: 'vyberte si z možností',
        TEXT_SEARCH_PLACEHOLDER: 'zadejte hledaná slova',
        FILTER_TOGGLER: 'Vyhledávání',
        TEXT_FAILED_TO_LOAD_ORGANIZATIONS: 'Nepodařilo se načíst seznam organizací',
        TEXT_FAILED_TO_LOAD_ORGANIZATION_DETAIL: 'Organizace nebyla nalezena',
        TEXT_FAILED_TO_LOAD_APPEAL_DETAIL: 'Ročník nebyl nalezen',
        TEXT_VOTE: 'Hlasovat pro tento projekt',
        TEXT_BUDGET_PROPOSED: 'Rozpočet projektu - návrh',
        TEXT_BUDGET_PROPOSED_SHORT: '(návrh)',
        TEXT_BUDGET_APPROVED: 'Rozpočet projektu - schválený',
        TEXT_BUDGET_APPROVED_SHORT: '(schváleno)',
        TEXT_PROJECT_FEASIBILITY: 'Posouzení proveditelnosti',
        TEXT_PROJECT_HISTORY: 'Historie projektu',
        TEXT_SUM_OVERALL: 'Celkem',
        TEXT_BUDGET_COLUMN_ITEM_DESCRIPTION: 'Popis položky',
        TEXT_BUDGET_COLUMN_ITEM_COUNT: 'Počet jednotek',
        TEXT_BUDGET_COLUMN_ITEM_PRICE: 'Cena za položku',
        TEXT_BUDGET_COLUMN_ITEM_PRICE_SUM: 'Cena celkem',
        TEXT_PROJECT_LOG_COLUMN_TITLE: 'Nadpis',
        TEXT_PROJECT_LOG_COLUMN_TEXT: 'Text',
        TEXT_PROJECT_LOG_COLUMN_WHEN: 'Datum',
    };

    let defaults = {
        // target API host, full without relative path, eg. https://example.com
        host: false,
        // ID of organization or false
        organization: false,
        // ID of appeal or false
        appeal: false,
        // ID of project if you want to display single project presentation first
        project: false,
        // bool, overriden by configuring specific appeal ID
        showLatestAppeal: true,
        // string, either 'projects' or 'schedule', default 'projects'
        default_output_mode: constants.OUTPUT_MODE_PROJECTS,
        // static texts this widget displays in various places
        texts: {
            header_appended_participatory_budget: constants.TEXT_PARTICIPATORY_BUDGET,
            no_projects_available: constants.TEXT_NO_PROJECTS_AVAILABLE,
            no_projects_found: constants.TEXT_NO_PROJECTS_FOUND,
            action_show_schedule: constants.TEXT_ACTION_SHOW_SCHEDULE,
            action_show_projects: constants.TEXT_ACTION_SHOW_PROJECTS,
            action_propose_project: constants.TEXT_ACTION_PROPOSE_PROJECT,
            label_organizations: constants.TEXT_LABEL_ORGANIZATIONS,
            label_appeals: constants.TEXT_LABEL_APPEALS,
            label_categories: constants.TEXT_LABEL_CATEGORIES,
            label_project_states: constants.TEXT_LABEL_PROJECT_STATES,
            label_search: constants.TEXT_LABEL_SEARCH,
            schedule_current_phase: constants.TEXT_SCHEDULE_CURRENT_PHASE,
            schedule_voting_enabled: constants.TEXT_SCHEDULE_VOTING_ENABLED,
            schedule_proposals_enabled: constants.TEXT_SCHEDULE_PROPOSALS_ENABLED,
            // string or false, to disable placeholder
            select_placeholder: constants.TEXT_SELECT_PLACEHOLDER,
            // string or false, to disable placeholder
            search_placeholder: constants.TEXT_SEARCH_PLACEHOLDER,
            filter_toggler: constants.FILTER_TOGGLER,
            failed_to_load_organizations: constants.TEXT_FAILED_TO_LOAD_ORGANIZATIONS,
            failed_to_load_organization_detail: constants.TEXT_FAILED_TO_LOAD_ORGANIZATION_DETAIL,
            failed_to_load_appeal_detail: constants.TEXT_FAILED_TO_LOAD_APPEAL_DETAIL,
            add_vote: constants.TEXT_VOTE,
            budget_proposed: constants.TEXT_BUDGET_PROPOSED,
            budget_proposed_short: constants.TEXT_BUDGET_PROPOSED_SHORT,
            budget_approved: constants.TEXT_BUDGET_APPROVED,
            budget_approved_short: constants.TEXT_BUDGET_APPROVED_SHORT,
            project_history: constants.TEXT_PROJECT_HISTORY,
            project_feasibility: constants.TEXT_PROJECT_FEASIBILITY,
            sum_overall: constants.TEXT_SUM_OVERALL,
            budget_column_description: constants.TEXT_BUDGET_COLUMN_ITEM_DESCRIPTION,
            budget_column_item_count: constants.TEXT_BUDGET_COLUMN_ITEM_COUNT,
            budget_column_item_price: constants.TEXT_BUDGET_COLUMN_ITEM_PRICE,
            budget_column_item_price_sum: constants.TEXT_BUDGET_COLUMN_ITEM_PRICE_SUM,
            project_log_column_title: constants.TEXT_PROJECT_LOG_COLUMN_TITLE,
            project_log_column_text: constants.TEXT_PROJECT_LOG_COLUMN_TEXT,
            project_log_column_when: constants.TEXT_PROJECT_LOG_COLUMN_WHEN,
        }
    };

    function ParticipativniRozpocet(element, options) {
        this.element = $(element);
        this.options = $.extend(true, defaults, options);
        this.init();
    }

    $.extend(ParticipativniRozpocet.prototype, {
        // constants
        API_LIST_ORGANIZATIONS: 'organizations.json',
        API_ORGANIZATION_DETAIL: 'organization/' + constants.API_URL_EL_ORGANIZATION_ID + '/info.json',
        API_ORGANIZATION_APPEAL_DETAIL: 'organization/' + constants.API_URL_EL_ORGANIZATION_ID + '/appeal/' + constants.API_URL_EL_APPEAL_ID + '/info.json',
        API_ORGANIZATION_APPEAL_PROJECTS: 'organization/' + constants.API_URL_EL_ORGANIZATION_ID + '/appeal/' + constants.API_URL_EL_APPEAL_ID + '/projects.json',
        // variables
        showOrganizationsSelector: true,
        currentOrganizationId: false,
        currentAppealId: false,
        currentProjectId: false,
        currentCategoryId: false,
        currentStateId: false,
        currentSearchPhrase: false,
        currentOutputMode: defaults.default_output_mode,
        el_header: false,
        el_btn_show_schedule: false,
        el_btn_show_projects: false,
        el_filter: false,
        el_message: false,
        el_filter_organizations: false,
        el_filter_appeals: false,
        el_filter_categories: false,
        el_filter_states: false,
        el_filter_search: false,
        el_output: false,
        el_schedule_output: false,
        el_projects_output: false,
        // ajax cache, last responses only
        organizations: false,
        organization_detail: false,
        organization_appeal_detail: false,
        organization_appeal_projects: false,
        // functions
        getAPIUrl(action) {
            let target = this.options.host + "/api/" + constants.API_VERSION + "/" + action;
            if (target.includes(constants.API_URL_EL_ORGANIZATION_ID)) {
                target = target.replace(constants.API_URL_EL_ORGANIZATION_ID, this.currentOrganizationId);
            }
            if (target.includes(constants.API_URL_EL_APPEAL_ID)) {
                target = target.replace(constants.API_URL_EL_APPEAL_ID, this.currentAppealId);
            }
            return target;
        },
        checkOptions() {
            if (typeof this.options.host !== 'string') {
                this.options.host = window.location.origin;
            }
            if (typeof this.options.organization !== 'number') {
                this.options.organization = false;
            } else {
                this.showOrganizationsSelector = false;
                this.options.organization = parseInt(this.options.organization);
                this.currentOrganizationId = this.options.organization;
            }
            if (typeof this.options.appeal !== 'number') {
                this.options.appeal = false;
                this.options.showLatestAppeal = true;
            } else {
                this.options.appeal = parseInt(this.options.appeal);
                this.currentAppealId = this.options.appeal;
                this.options.showLatestAppeal = false;
            }
            if (typeof this.options.project !== 'number') {
                this.options.project = false;
            } else {
                this.currentProjectId = this.options.project;
            }
            if (typeof this.options.texts.select_placeholder === 'boolean') {
                if (this.options.texts.select_placeholder === true) {
                    this.options.texts.select_placeholder = defaults.texts.select_placeholder;
                }
            } else if (typeof this.options.texts.select_placeholder !== 'string') {
                this.options.texts.select_placeholder = defaults.texts.select_placeholder;
            }
            if (typeof this.options.texts.search_placeholder === 'boolean') {
                if (this.options.texts.search_placeholder === true) {
                    this.options.texts.search_placeholder = defaults.texts.search_placeholder;
                }
            } else if (typeof this.options.texts.search_placeholder !== 'string') {
                this.options.texts.search_placeholder = defaults.texts.search_placeholder;
            }
            if (typeof this.options.default_output_mode !== 'string'
                || ![constants.OUTPUT_MODE_PROJECTS, constants.OUTPUT_MODE_SCHEDULE].includes(this.options.default_output_mode)
            ) {
                this.options.default_output_mode = constants.OUTPUT_MODE_PROJECTS;
            }
        },
        addHTMLWidgets() {
            let $target = this.element;
            $target.addClass('paro2-container-fluid');
            // reset container
            $target.empty();
            let uniqueFilterSectionId = 'filter-' + Math.random().toString(36).substring(7);
            // add filter and output sections
            this.el_header = $('<div class="paro2-row paro2-bg-dark paro2-header-container">' +
                '<h1 class="paro2-col-md-8 paro2-text-light paro2-pt-1"></h1>' +
                '<div class="paro2-col-md paro2-text-md-right paro2-navbar-dark">' +
                '<button class="paro2-navbar-toggler paro2-d-md-none" type="button" data-toggle="paro2-collapse" data-target="#' + uniqueFilterSectionId + '" aria-controls="' + uniqueFilterSectionId + '" aria-expanded="false" aria-label="Toggle navigation"><span class="paro2-navbar-toggler-icon"></span> ' + this.options.texts.filter_toggler + '</button>' +
                '<a class="paro2-btn paro2-m-2 paro2-btn-info paro2-text-dark paro2-btn-schedule">' + this.options.texts.action_show_schedule + '</a>' +
                '<a class="paro2-btn paro2-m-2 paro2-btn-info paro2-text-dark paro2-btn-projects">' + this.options.texts.action_show_projects + '</a>' +
                '<a class="paro2-btn paro2-m-2 paro2-btn-success paro2-btn-propose" target="_blank">' + this.options.texts.action_propose_project + '</a>' +
                '</div>' +
                '</div>');
            this.el_btn_show_projects = $(".paro2-btn-projects", this.el_header);
            this.el_btn_show_schedule = $(".paro2-btn-schedule", this.el_header);
            this.el_filter = $('<div class="paro2-row paro2-navbar paro2-navbar-light paro2-navbar-expand-md paro2-filter-container">' +
                '<div class="paro2-collapse paro2-navbar-collapse paro2-row" id="' + uniqueFilterSectionId + '"></div>' +
                '</div>');
            this.el_message = $("<div class='paro2-alert paro2-d-none paro2-border-0 paro2-alert-info paro2-text-center'>Žádné projekty nebyly nalezeny</div>");
            this.el_output = $('<div class="paro2-row paro2-output-container">' +
                '<div class="paro2-col paro2-row paro2-projects-container"></div>' +
                '<div class="paro2-col paro2-schedule-container"><ul class="paro2-timeline"></ul></div>' +
                '<div class="paro2-col paro2-col-md-8 paro2-offset-md-2 paro2-project-detail-container"></div>' +
                '</div>');
            this.el_schedule_output = $(".paro2-schedule-container", this.el_output);
            this.el_projects_output = $(".paro2-projects-container", this.el_output);
            this.el_project_detail_output = $(".paro2-project-detail-container", this.el_output);
            $target.append(this.el_header, this.el_filter, this.el_message, this.el_output);
            this.el_filter_states = $("<select class='paro2-col-md'></select>");
            let el_filter_organizations_form_group = $("<div class='paro2-form-group paro2-organizations-form-group paro2-col-md'>" +
                "<label for='paro2-select-organization'>" + this.options.texts.label_organizations + "</label>" +
                "<select name='paro2-select-organization' class='paro2-col-md'></select>" +
                "</div>");
            let el_filter_appeals_form_group = $("<div class='paro2-form-group paro2-col-md'>" +
                "<label for='paro2-select-appeal'>" + this.options.texts.label_appeals + "</label>" +
                "<select name='paro2-select-appeal' class='paro2-col-md'></select>" +
                "</div>");
            let el_filter_categories_form_group = $("<div class='paro2-form-group paro2-col-md'>" +
                "<label for='paro2-select-category'>" + this.options.texts.label_categories + "</label>" +
                "<select name='paro2-select-category' class='paro2-col-md'></select>" +
                "</div>");
            let el_filter_states_form_group = $("<div class='paro2-form-group paro2-col-md'>" +
                "<label for='paro2-select-states'>" + this.options.texts.label_project_states + "</label>" +
                "<select name='paro2-select-states' class='paro2-col-md'></select>" +
                "</div>");
            let el_filter_search_form_group = $("<div class='paro2-form-group paro2-col-md'>" +
                "<label for='paro2-search'>" + this.options.texts.label_search + "</label>" +
                "<input name='paro2-search' type='text' class='paro2-col-md'>" +
                "</div>");
            this.el_filter_search = $("input", el_filter_search_form_group).first();
            this.el_filter_categories = $("select", el_filter_categories_form_group).first();
            this.el_filter_organizations = $("select", el_filter_organizations_form_group).first();
            this.el_filter_appeals = $("select", el_filter_appeals_form_group).first();
            this.el_filter_states = $("select", el_filter_states_form_group).first();

            this.el_filter_organizations.change(() => {
                console.log('organizations on-change');
                let selectedOrganizationId = parseInt(this.el_filter_organizations.val());
                if (selectedOrganizationId !== this.currentOrganizationId) {
                    this.currentOrganizationId = selectedOrganizationId;
                    this.fetchOrganizationDetail();
                }
            });
            this.el_filter_appeals.change(() => {
                console.log('appeals on-change');
                let selectedAppealId = parseInt(this.el_filter_appeals.val());
                if (selectedAppealId !== this.currentAppealId) {
                    this.currentAppealId = selectedAppealId;
                    this.fetchAppealDetail();
                }
            });
            this.el_btn_show_schedule.click(() => {
                this.toggleOutputMode(constants.OUTPUT_MODE_SCHEDULE);
            });
            this.el_btn_show_projects.click(() => {
                this.toggleOutputMode(constants.OUTPUT_MODE_PROJECTS);
            });
            this.el_filter_states.change(() => {
                this.updateFilter();
            });
            this.el_filter_categories.change(() => {
                this.updateFilter();
            });
            this.el_filter_search.bind('change keyup', () => {
                this.updateFilter();
            });

            if (typeof this.options.texts.search_placeholder === 'string') {
                this.el_filter_search.prop('placeholder', this.options.texts.search_placeholder);
            }
            $("#" + uniqueFilterSectionId, this.el_filter).first().append(el_filter_organizations_form_group, el_filter_appeals_form_group, el_filter_categories_form_group, el_filter_states_form_group, el_filter_search_form_group);
        },
        toggleOutputMode(outputMode) {
            let otherMode = outputMode === constants.OUTPUT_MODE_SCHEDULE ? constants.OUTPUT_MODE_PROJECTS : constants.OUTPUT_MODE_SCHEDULE;
            this.currentOutputMode = this.currentOutputMode === outputMode ? otherMode : outputMode;
            this.updateOutput();
        },
        setErrorMessage(message) {
            let isEmpty = !message || 0 === message.length;
            console.log('setErrorMessage', message, isEmpty);
            this.el_message.toggleClass('paro2-d-none', isEmpty);
            this.el_message.text(message);
        },
        updateHeader(context) {
            context = typeof context === 'object' ? context : this;
            if (context.showOrganizationsSelector === false) {
                let $h1 = $("h1", context.el_header);
                $h1.text(context.organization_detail.organization.name + this.options.texts.header_appended_participatory_budget);
            }
        },
        updateSchedule(context) {
            console.log('updateSchedule');
            context = typeof context === 'object' ? context : this;
            let $list = $("ul", this.el_schedule_output);
            $list.empty();
            if(context.organization_appeal_detail === false) {
                console.log('updateSchedule abort, no appeal detail available');
                return;
            }
            $(context.organization_appeal_detail.appeal.phases).each((key, value) => {
                $list.append($("<li class='" + (value.currently_active ? 'paro2-current' : '') + "'>" +
                    '<span class="paro2-text-dark paro2-font-weight-bold paro2-text-decoration-none paro2-h5">' + value.name + '</span>' +
                    '<span class="paro2-float-right paro2-text-dark paro2-text-decoration-none">' + context.getAsFormattedDate(value.date_start) + ' - ' + context.getAsFormattedDate(value.date_end) + '</span>' +
                    (value.currently_active ? '<p class="paro2-text-success">' + context.options.texts.schedule_current_phase + "</p>" : '') +
                    (value.allows_proposals_submission ? '<p class="paro2-text-success">' + context.options.texts.schedule_proposals_enabled + "</p>" : '') +
                    (value.allows_voting ? '<p class="paro2-text-success">' + context.options.texts.schedule_voting_enabled + "</p>" : '') +
                    '<p>' + value.description + '</p>' +
                    "</li>"));
            });
        },
        openProjectDetail(project_id) {
            project_id = parseInt(project_id);
            console.log('openProjectDetail ' + project_id);
            let projectData = null;
            $(this.organization_appeal_detail.appeal.projects).each((key, value) => {
                if (!isNaN(project_id) && value.id === project_id) {
                    projectData = value;
                }
            });
            if (!projectData) {
                console.log('cannot display project ' + project_id);
                return;
            }
            this.currentOutputMode = constants.OUTPUT_MODE_PROJECT_DETAIL;
            this.el_project_detail_output.empty();
            let $projectDetail = $(
                '<div class="paro2-row paro2-bg-danger">' +
                '<div class="paro2-col-md-7 paro2-text-light paro2-font-weight-bold paro2-pt-4">' +
                '<a class="paro2-display-4 paro2-text-light paro2-back-to-projects" href="#"><i class="fas fa-arrow-left"></i></a>' +
                '<h2 class="paro2-text-center paro2-display-2">' + projectData.name + '</h2>' +
                '</div>' +
                '<div class="paro2-col-md-5 pr-0">' +
                '<img class="paro2-mw-100" src="' + this.options.host + projectData.image + '" alt="">' +
                '<div class="paro2-alert paro2-alert-success paro2-font-weight-bold paro2-position-absolute paro2-mr-3 paro2-mt-3 paro2-border-dark" style="top: 0; right: 0;">' + projectData.current_status_text + '</div>' +
                '</div>' +
                '</div>' +

                '<div class="paro2-text-center paro2-p-2">' +
                (projectData.voting ? '<a href="' + this.options.host + projectData.voting.url + '" class="paro2-btn paro2-btn-success">' + this.options.texts.add_vote + '</a>' : '') +
                '</div>' +

                '<div class="paro2-row paro2-bg-light paro2-h4 paro2-text-dark paro2-p-2">' +
                '<div class="paro2-col-md-4 paro2-mb-2"><i class="fa fa-user paro2-mr-2"></i> ' + projectData.proposer.name + '</div>' +
                '<div class="paro2-col-md-4 paro2-mb-2"><i class="fa fa-tag paro2-mr-2"></i> ' + Object.values(projectData.categories).join(', ') + '</div>' +
                (projectData.approved_price_total > 0 ?
                        '<div class="paro2-col-md-4 paro2-mb-2"><i class="fa fa-money-bill paro2-mr-2"></i> ' + this.getAsCurrency(projectData.approved_price_total) + ' ' + this.options.texts.budget_approved_short + '</div>'
                        : '<div class="paro2-col-md-4 paro2-mb-2"><i class="fa fa-money-bill paro2-mr-2"></i> ' + this.getAsCurrency(projectData.proposed_price_total) + ' ' + this.options.texts.budget_proposed_short + '</div>'
                ) +
                '<div class="paro2-col-md-4 paro2-mb-2"><i class="fa fa-map-marker paro2-mr-2"></i> ' + projectData.address + '</div>' +
                '<div class="paro2-col-md-4 paro2-mb-2"><i class="fa fa-calendar paro2-mr-2"></i> ' + this.organization_appeal_detail.appeal.year + '</div>' +
                '<div class="paro2-col-md-4 paro2-mb-2"><i class="fa fa-thumbs-up paro2-mr-2"></i> ' + projectData.votes_overall + '</div>' +
                '</div>' +

                '<div class="paro2-row paro2-bg-dark paro2-text-light">' +
                '<div class="paro2-col-md-8 paro2-offset-2 paro2-p-2 paro2-text-justify">' + projectData.description + '</div>' +
                '</div>' +

                '<hr/>' +

                '<div class="paro2-table-responsive">' +
                '<h3 class="">' + this.options.texts.budget_proposed + '</h3>' +
                '<table class="paro2-budget-proposed paro2-table-bordered paro2-table">' +
                '<thead class="paro2-thead-dark"><tr>' +
                '<th>' + this.options.texts.budget_column_description + '</th>' +
                '<th class="paro2-text-right">' + this.options.texts.budget_column_item_count + '</th>' +
                '<th class="paro2-text-right">' + this.options.texts.budget_column_item_price + '</th>' +
                '<th class="paro2-text-right">' + this.options.texts.budget_column_item_price_sum + '</th>' +
                '</tr></thead>' +
                '<tbody>' +
                '</tbody>' +
                '</table>' +
                '</div>' +

                '<hr/>' +

                (projectData.approved_price_total > 0 ? (
                    '<div class="paro2-table-responsive">' +
                    '<h3 class="">' + this.options.texts.budget_approved + '</h3>' +
                    '<table class="paro2-budget-approved paro2-table-bordered paro2-table">' +
                    '<thead class="paro2-thead-dark"><tr>' +
                    '<th>' + this.options.texts.budget_column_description + '</th>' +
                    '<th class="paro2-text-right">' + this.options.texts.budget_column_item_count + '</th>' +
                    '<th class="paro2-text-right">' + this.options.texts.budget_column_item_price + '</th>' +
                    '<th class="paro2-text-right">' + this.options.texts.budget_column_item_price_sum + '</th>' +
                    '</tr></thead>' +
                    '<tbody>' +
                    '</tbody>' +
                    '</table>' +
                    '</div>' +

                    '<hr/>') : '') +

                '<div class="paro2-table-responsive">' +
                '<h3 class="">' + this.options.texts.project_feasibility + '</h3>' +
                '<table class="paro2-feasibility paro2-table-bordered paro2-table">' +
                '<colgroup><col class="w-25"><col class="w-50"><col class="col-25"></colgroup>' +
                '<thead class="paro2-thead-dark"><tr>' +
                '<th>' + this.options.texts.project_log_column_title + '</th>' +
                '<th>' + this.options.texts.project_log_column_text + '</th>' +
                '<th>' + this.options.texts.project_log_column_when + '</th>' +
                '</tr></thead>' +
                '<tbody>' +
                '</tbody>' +
                '</table>' +
                '</div>' +

                '<hr/>' +

                '<div class="paro2-table-responsive">' +
                '<h3 class="">' + this.options.texts.project_history + '</h3>' +
                '<table class="paro2-history paro2-table-bordered paro2-table">' +
                '<colgroup><col class="w-25"><col class="w-50"><col class="col-25"></colgroup>' +
                '<thead class="paro2-thead-dark"><tr>' +
                '<th>' + this.options.texts.project_log_column_title + '</th>' +
                '<th>' + this.options.texts.project_log_column_text + '</th>' +
                '<th>' + this.options.texts.project_log_column_when + '</th>' +
                '</tr></thead>' +
                '<tbody>' +
                '</tbody>' +
                '</table>' +
                '</div>' +

                '</div>'
            );
            let $proposedBudget = $("table.paro2-budget-proposed tbody", $projectDetail);
            $(projectData.budget.proposed).each((key, value) => {
                $proposedBudget.append($("<tr>" +
                    '<td>' + value.description + '</td>' +
                    '<td class="paro2-text-right">' + value.item_count + '</td>' +
                    '<td class="paro2-text-right">' + this.getAsCurrency(value.item_price) + '</td>' +
                    '<td class="paro2-text-right">' + this.getAsCurrency(value.total_price) + '</td>' +
                    "</tr>"));
            });
            $("table.paro2-budget-proposed", $projectDetail).append($("<tfoot class='paro2-thead-dark'>" +
                "<tr>" +
                "<th colspan='3'>" + this.options.texts.sum_overall + "</th>" +
                "<th class='paro2-text-right'>" + this.getAsCurrency(projectData.proposed_price_total) + "</th>" +
                "</tr>" +
                "</tfoot>"));
            if (projectData.approved_price_total > 0) {
                let $approvedBudget = $("table.paro2-budget-approved tbody", $projectDetail);
                $(projectData.budget.approved).each((key, value) => {
                    $approvedBudget.append($("<tr>" +
                        '<td>' + value.description + '</td>' +
                        '<td class="paro2-text-right">' + value.item_count + '</td>' +
                        '<td class="paro2-text-right">' + this.getAsCurrency(value.item_price) + '</td>' +
                        '<td class="paro2-text-right">' + this.getAsCurrency(value.total_price) + '</td>' +
                        "</tr>"));
                });
                $("table.paro2-budget-approved", $projectDetail).append($("<tfoot class='paro2-thead-dark'>" +
                    "<tr>" +
                    "<th colspan='3'>" + this.options.texts.sum_overall + "</th>" +
                    "<th class='paro2-text-right'>" + this.getAsCurrency(projectData.approved_price_total) + "</th>" +
                    "</tr>" +
                    "</tfoot>"));
            }
            let $feasibility = $("table.paro2-feasibility tbody", $projectDetail);
            $(projectData.feasibility_statements).each((key, value) => {
                $feasibility.append($("<tr>" +
                    '<td>' + value.title + '</td>' +
                    '<td>' + value.text + '</td>' +
                    '<td>' + this.getAsFormattedDate(value.timestamp) + '</td>' +
                    "</tr>"));
            });
            let $history = $("table.paro2-history tbody", $projectDetail);
            $(projectData.project_history).each((key, value) => {
                $history.append($("<tr>" +
                    '<td>' + value.title + '</td>' +
                    '<td>' + value.text + '</td>' +
                    '<td>' + this.getAsFormattedDate(value.timestamp) + '</td>' +
                    "</tr>"));
            });

            this.el_project_detail_output.append($projectDetail);

            let $backButton = $("a.paro2-back-to-projects", this.el_project_detail_output);
            $backButton.bind('click', () => {
                this.currentOutputMode = constants.OUTPUT_MODE_PROJECTS;
                this.updateOutput();
            });

            this.updateOutput();
        },
        getAsCurrency(value, currency = 'CZK') {
            value = parseFloat(value);
            value = isNaN(value) ? 0 : value;
            return (value).toLocaleString('cs-CZ', {
                style: 'currency',
                currency: 'CZK'
            });
        },
        getAsFormattedDate(value) {
            return (new Date(value * 1000)).toLocaleDateString('cs-CZ');
        },
        updateProjects(context) {
            console.log('updateProjects');
            context = typeof context === 'object' ? context : this;
            context.el_projects_output.empty();
            if (context.organization_appeal_detail === false) {
                console.log('updateProjects abort, no appeal detail available');
                return;
            }
            $(context.organization_appeal_detail.appeal.projects).each((key, value) => {
                let $projectCard = $('<div data-paro2-projekt-id="' + value.id + '" class="paro2-projekt paro2-col-md-6 paro2-col-lg-4"><div class="paro2-card">' +
                    (value.image ? '<img src="' + this.options.host + value.image + '" class="paro2-card-img-top" alt="' + value.current_status_text + '">' : '') +
                    '<span class="paro2-position-absolute paro2-bg-success paro2-p-2 paro2-rounded-bottom paro2-border-dark paro2-border-bottom paro2-border-left" style="top:0;right:0;">' + value.current_status_text + '</span>' +
                    '<div class="paro2-card-body">' +
                    '<h5 class="paro2-card-title">' + value.name + '</h5>' +
                    //'<p>' + value.description + '</p>' +
                    '<p>' + context.options.texts.label_categories + ' ' + Object.values(value.categories).join(', ') + '</p>' +
                    '<a href="#" class="paro2-stretched-link paro2-projekt-link"></a>' +
                    '</div>' +
                    '<div class="paro2-card-footer text-right">' +
                    '<i class="fas fa-thumbs-up paro2-mr-2"></i> ' + value.votes_overall +
                    '</div>' +
                    '</div></div>');
                $("a", $projectCard).click(() => {
                    context.openProjectDetail(value.id);
                });
                context.el_projects_output.append($projectCard);
            });
        },
        updateFilter(context) {
            context = typeof context === 'object' ? context : this;
            if (!context.organization_appeal_detail) {
                return;
            }
            let suitableProjectIds = [];
            let desiredState = parseInt(context.el_filter_states.val());
            let desiredCategory = parseInt(context.el_filter_categories.val());
            let desiredKeywords = context.el_filter_search.val().trim().toLowerCase();
            $(context.organization_appeal_detail.appeal.projects).each((key, value) => {
                let suitable = true;
                if (!isNaN(desiredState)) {
                    suitable = parseInt(value.current_status_id) === desiredState;
                }
                if (suitable && !isNaN(desiredCategory)) {
                    suitable = Object.keys(value.categories).includes(desiredCategory.toString(10));
                }
                if (suitable && desiredKeywords !== '') {
                    suitable = value.name.toLowerCase().includes(desiredKeywords)
                        || value.description.toLowerCase().includes(desiredKeywords);
                }
                if (suitable) {
                    suitableProjectIds.push(value.id);
                }
            });
            $(".paro2-projekt", context.el_projects_output).each(function () {
                $(this).toggle(suitableProjectIds.includes($(this).data('paro2-projekt-id')));
            });
            context.setErrorMessage(suitableProjectIds.length < 1 ? context.options.texts.no_projects_found : '');
        },
        updateAppeal(context) {
            context = typeof context === 'object' ? context : this;
            let $propose = $(".paro2-btn-propose", context.el_header);
            $propose.toggle(context.organization_appeal_detail.appeal.proposals.currently_open);
            $propose.prop('href', context.organization_appeal_detail.appeal.proposals.url);
            let targetOutputMode = constants.OUTPUT_MODE_PROJECTS;
            if (context.organization_appeal_detail.appeal.projects_count < 1) {
                context.setErrorMessage(this.options.texts.no_projects_available);
                targetOutputMode = constants.OUTPUT_MODE_SCHEDULE;
            }
            if (typeof context.currentProjectId === 'number') {
                $(context.organization_appeal_detail.appeal.projects).each((key, value) => {
                    if (value.id === context.currentProjectId) {
                        targetOutputMode = constants.OUTPUT_MODE_PROJECT_DETAIL;
                        context.openProjectDetail(context.currentProjectId);
                    }
                });
            }
            this.currentOutputMode = targetOutputMode;
            this.updateOutput();
            let $schedule = $(".paro-btn-schedule", context.el_header);
            $schedule.toggle($(context.organization_appeal_detail.appeal.phases).length !== 0);
            context.updateSchedule(context);
            context.updateProjects(context);
        },
        fetchOrganizations(callback) {
            console.log('fetchOrganizations');
            $.get(this.getAPIUrl(this.API_LIST_ORGANIZATIONS),
                (data) => {
                    this.organizations = data;
                    this.updateWidgets();
                    if (typeof callback === 'function') {
                        callback(this);
                    }
                }
            ).fail(() => {
                this.organizations = false;
                this.setErrorMessage(this.options.texts.failed_to_load_organizations);
                console.error('check if the host option is configured properly');
            });
        },
        fetchOrganizationDetail(callback) {
            console.log('fetchOrganizationDetail');
            $.get(this.getAPIUrl(this.API_ORGANIZATION_DETAIL),
                (data) => {
                    this.organization_detail = data;
                    this.updateWidgets();
                    this.updateHeader(this);
                    if (typeof callback === 'function') {
                        callback(this);
                    }
                }
            ).fail(() => {
                this.organization_detail = false;
                this.setErrorMessage(this.options.texts.failed_to_load_organization_detail);
                console.error('check if the host option is configured properly, and if the organization is enabled');
            });
        },
        fetchAppealDetail(callback) {
            console.log('fetchAppealDetail');
            $.get(this.getAPIUrl(this.API_ORGANIZATION_APPEAL_DETAIL),
                (data) => {
                    this.organization_appeal_detail = data;
                    this.updateWidgets();
                    this.updateAppeal();
                    if (typeof callback === 'function') {
                        callback(this);
                    }
                }
            ).fail(() => {
                this.organization_appeal_detail = false;
                this.setErrorMessage(this.options.texts.failed_to_load_appeal_detail);
                this.updateProjects();
                this.updateSchedule();
                console.error('check if the host option is configured properly, and if the organization is enabled and has at least one appeal set-up');
            });
        },
        updateWidgetOrganizationSelect() {
            if (typeof this.organizations !== 'object') {
                return;
            }
            this.el_filter_organizations.empty();
            let firstOrganizationId = false;
            let hasCurrentOrganizationId = false;
            $.each(this.organizations.organizations, (key, value) => {
                if (firstOrganizationId === false) {
                    firstOrganizationId = value.id;
                }
                if (typeof this.currentOrganizationId === 'number' && value.id === this.currentOrganizationId) {
                    hasCurrentOrganizationId = true;
                }
                this.el_filter_organizations.append(new Option(value.name, value.id));
            })
            this.el_filter_organizations.closest('div').toggleClass('paro2-d-none', !this.showOrganizationsSelector);
            this.el_filter_organizations.val(hasCurrentOrganizationId ? this.currentOrganizationId : firstOrganizationId).change();
        },
        updateWidgetAppealsSelect() {
            if (typeof this.organization_detail !== 'object') {
                return;
            }
            this.el_filter_appeals.empty();
            let firstAppealId = false;
            let hasCurrentAppealId = false;
            $.each(this.organization_detail.organization.appeals, (key, value) => {
                if (firstAppealId === false) {
                    firstAppealId = value.id;
                }
                if (typeof this.currentAppealId === 'number' && value.id === this.currentAppealId) {
                    hasCurrentAppealId = true;
                }
                this.el_filter_appeals.append(new Option(value.year, value.id));
            })
            this.el_filter_appeals.val(hasCurrentAppealId ? this.currentAppealId : firstAppealId).change();
        },
        updateWidgetCategoriesSelect() {
            if (typeof this.organization_detail !== 'object') {
                return;
            }
            this.el_filter_categories.empty();
            let hasCurrentCategoryId = false;
            if (typeof this.options.texts.select_placeholder === 'string') {
                this.el_filter_categories.append(new Option(this.options.texts.select_placeholder));
            }
            $.each(this.organization_detail.organization.project_categories, (key, value) => {
                if (typeof this.currentCategoryId === 'number' && this.currentCategoryId === key) {
                    hasCurrentCategoryId = true;
                }
                this.el_filter_categories.append(new Option(value, key));
            })
            console.log('updateWidgetCategoriesSelect', hasCurrentCategoryId, this.currentCategoryId);
            if (hasCurrentCategoryId) {
                this.el_filter_categories.val(this.currentCategoryId);
            } else {
                this.currentCategoryId = false;
            }
            this.el_filter_categories.change();
        },
        updateWidgetStatesSelect() {
            if (typeof this.organization_detail !== 'object') {
                return;
            }
            this.el_filter_states.empty();
            let hasCurrentStateId = false;
            if (typeof this.options.texts.select_placeholder === 'string') {
                this.el_filter_states.append(new Option(this.options.texts.select_placeholder));
            }
            $.each(this.organization_detail.organization.project_states, (key, value) => {
                if (typeof this.currentStateId === 'number' && this.currentStateId === key) {
                    hasCurrentStateId = true;
                }
                this.el_filter_states.append(new Option(value, key));
            })
            console.log('updateWidgetStatesSelect', hasCurrentStateId, this.currentStateId);
            if (hasCurrentStateId) {
                this.el_filter_states.val(this.currentStateId);
            } else {
                this.currentStateId = false;
            }
            this.el_filter_states.change();
        },
        updateOutput() {
            console.log('updateOutput ' + this.currentOutputMode);
            this.el_projects_output.toggleClass('paro2-d-none', this.currentOutputMode !== constants.OUTPUT_MODE_PROJECTS);
            this.el_btn_show_projects.toggleClass('paro2-d-none', this.currentOutputMode !== constants.OUTPUT_MODE_SCHEDULE);
            this.el_schedule_output.toggleClass('paro2-d-none', this.currentOutputMode !== constants.OUTPUT_MODE_SCHEDULE);
            this.el_btn_show_schedule.toggleClass('paro2-d-none', this.currentOutputMode !== constants.OUTPUT_MODE_PROJECTS);
            this.el_filter.toggleClass('paro2-d-none', this.currentOutputMode === constants.OUTPUT_MODE_PROJECT_DETAIL);
            this.el_project_detail_output.toggleClass('paro2-d-none', this.currentOutputMode !== constants.OUTPUT_MODE_PROJECT_DETAIL);
        },
        updateWidgets(context) {
            context = typeof context === "undefined" ? this : context;
            context.updateWidgetOrganizationSelect();
            context.updateWidgetAppealsSelect();
            context.updateWidgetCategoriesSelect();
            context.updateWidgetStatesSelect();
            context.updateOutput();
        },
        fetchAPI() {
            this.fetchOrganizations();
            if (this.currentOrganizationId !== false) {
                this.fetchOrganizationDetail();
            }
            if (this.currentAppealId !== false) {
                this.fetchAppealDetail();
            }
        },
        init() {
            this.checkOptions();
            this.addHTMLWidgets();
            this.fetchAPI();
        }
    });

    $.fn.participativniRozpocet = function (options) {
        return this.each(function () {
            if (!$.data(this, "participativniRozpocet")) {
                $.data(this, "participativniRozpocet", new ParticipativniRozpocet(this, options));
            }
        });
    }
})(jQuery, window, document);
