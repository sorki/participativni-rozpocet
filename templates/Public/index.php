<?php
/**
 * @var $this AppView
 */

use App\Model\Entity\Organization;
use App\View\AppView;

/** @var null|Organization $organization */
$organization = $this->Identity->get('organizations.0');
$hasOrganization = $organization instanceof Organization;

$this->assign('title', __('Participativní rozpočet'));
?>
<script type="text/javascript">
    function loginFirst(ev) {
        alert('Musíte se nejdříve přihlásit');
        if (ev) {
            ev.preventDefault();
        }
    }
</script>
<div class="card m-2">
    <h2 class="card-header"><?= __('Jak si vytvořit svůj participativní rozpočet?') ?></h2>
    <div class="card-body">
        <ol>
            <li><?= $this->Html->link(__('Registrujte se na této stránce a přihlaste se'), ['_name' => 'register']) ?></li>
            <li><?= $this->Html->link(__('Vytvořte si novou organizaci (např. obec)'), ['_name' => 'organization_add']) ?></li>
            <li><?= $this->Html->link(__('Přidejte si ročník a jednotlivé fáze participativního rozpočtu'), $hasOrganization ? ['_name' => 'appeal_add', 'organization_id' => $organization->id] : 'javascript:loginFirst();') ?></li>
            <li><?= $this->Html->link(__('Pokud nechcete vlastní webové stránky o participativním rozpočtu, můžete celý proces realizovat zde'), $hasOrganization ? ['_name' => 'public_organization_detail', 'organization_id' => $organization->id] : 'javascript:loginFirst();') ?></li>
            <li><?= sprintf(
                    __('Pokud chcete participativní rozpočet prezentovat na svých webových stránkách, použijte naše %s nebo si udělejte integraci sami pomocí %s'),
                    $this->Html->link(__('Widgety'), ['_name' => 'public_widgets']),
                    $this->Html->link(__('API'), ['_name' => 'api_info'])
                ) ?></li>
        </ol>
    </div>
</div>

<div class="card m-2">
    <h2 class="card-header"><?= __('FAQ: Často kladené dotazy') ?></h2>
    <div class="card-body table-responsive">
        <table class="table table-striped table-bordered">
            <thead class="thead-dark">
            <tr>
                <th><?= __('Otázka') ?></th>
                <th><?= __('Odpověď') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $faqs = [
                [
                    'Kolik to stojí&nbsp;?',
                    'Software je zdarma ke stažení na ' . $this->Html->link('Gitlab', 'https://gitlab.com/otevrenamesta/participativni-rozpocet') . ', pokud jej ale chcete využívat jako službu, tak pro členy spolku Otevřených Měst je zdarma, pro ostatní provozní poplatek 1.000 Kč ročně',
                ],
                [
                    'Kdo může hlasovat o navržených projektech / navrhovat projekty&nbsp;?',
                    'V každé fázi, ve které lze hlasovat nebo navrhovat projekty, si sami nastavíte povolené způsoby ověření uživatelů.<br/>Podporujeme jak hlasování bez ověření, tak ověření e-mailem nebo i <strong>eIdentita (NIA) s ověřením věku a bydliště hlasujícího občana</strong>'
                ],
                [
                    'Chtěli bychom rozpočet na vlastní doméně, ale nechceme si webové stránky dělat sami',
                    'Pokud máte vlastní doménu, na které chcete s občany komunikovat, například participativni-rozpocet.medlanky.cz, stačí ji nasměrovat na naše servery a doménu si přidat k vlastní organizaci v její administraci, vše ostatní již funguje automaticky'
                ]
            ];
            foreach ($faqs as $faq):
                ?>
                <tr>
                    <td class="font-weight-bold"><?= $faq[0] ?></td>
                    <td><?= $faq[1] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
