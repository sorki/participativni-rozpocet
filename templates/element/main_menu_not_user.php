<?php
/**
 * @var $this AppView
 */

use App\View\AppView;

?>
<ul class="navbar-nav ml-auto">
    <li class="nav-item">
        <?= $this->Html->link(__('Přihlásit se'), ['_name' => 'login'], ['class' => 'btn btn-success m-2']) ?>
    </li>

    <li class="nav-item">
        <?= $this->Html->link(__('Registrovat se'), ['_name' => 'register'], ['class' => 'btn btn-primary m-2']) ?>
    </li>
</ul>
