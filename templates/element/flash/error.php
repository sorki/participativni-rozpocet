<?php
/**
 * @var AppView $this
 * @var array $params
 * @var string $message
 */

use App\View\AppView;

if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger" onclick="this.classList.add('d-none');"><?= $message ?></div>
