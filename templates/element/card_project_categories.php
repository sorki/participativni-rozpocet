<?php

use App\Model\Entity\Organization;
use App\Model\Entity\ProjectCategory;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $categories ProjectCategory[]
 * @var $limit int
 */
if (!isset($limit) || !is_numeric($limit)) {
    $limit = 0;
} else {
    $limit = intval($limit);
}
usort($categories, function (ProjectCategory $a, ProjectCategory $b) {
    return strcmp($a->name, $b->name);
});
$counter = 0;
?>

<div class="card m-2">
    <h2 class="card-header"><?= __('Kategorie projektů') ?></h2>
    <div class="card-body table-responsive">
        <div class="alert alert-info">
            <?= __('Jeden projekt může být zařazen ve více kategoriích. Může jít například o odbor (Sport, Kultura, Školství, Zdraví, Doprava, Zeleň, ...), cenu projektu (Levné, Středně nákladné, Velmi nákladné, ...) nebo jiné') ?>
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('Název kategorie') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($categories as $category): ?>
                <?php $counter++; ?>
                <?php if ($limit === 0 || $counter < $limit): ?>
                    <tr>
                        <td><?= $category->id ?></td>
                        <td><?= $category->name ?></td>
                        <td>
                            <?= $this->Html->link(__('Upravit'), ['controller' => 'ProjectCategories', 'action' => 'addModify', 'organization_id' => $organization->id, 'category_id' => $category->id]) ?>
                            ,
                            <?= $this->Form->postLink(
                                __('Smazat'),
                                ['controller' => 'ProjectCategories', 'action' => 'delete', 'organization_id' => $organization->id, 'category_id' => $category->id, '_method' => 'POST'],
                                ['confirm' => __('Opravdu chcete smazat tuto kategorii?'), 'class' => 'text-danger']
                            ) ?>
                        </td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <td colspan="4"><?= $this->Html->link(sprintf(__('A dalších %d ...'), count($categories) - $limit), ['controller' => 'ProjectCategories', 'action' => 'index', 'organization_id' => $organization->id]) ?></td>
                    </tr>
                    <?php break; endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Přidat novou Kategorii'), ['action' => 'addModify', 'controller' => 'ProjectCategories', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>
