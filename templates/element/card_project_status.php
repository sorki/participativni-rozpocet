<?php

use App\Model\Entity\Organization;
use App\Model\Entity\ProjectStatus;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $status ProjectStatus[]
 * @var $limit int
 */
if (!isset($limit) || !is_numeric($limit)) {
    $limit = 0;
} else {
    $limit = intval($limit);
}
usort($status, function (ProjectStatus $a, ProjectStatus $b) {
    return strcmp($a->name, $b->name);
});
$counter = 0;
?>

<div class="card m-2">
    <h2 class="card-header"><?= __('Stavy projektů') ?></h2>
    <div class="card-body table-responsive">
        <div class="alert alert-info">
            <?= __('Jeden projekt může být v jednu chvíli pouze v jednom z následujících stavů') ?>
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('Název') ?></th>
                <th><?= __('Je projekt veřejný?') ?></th>
                <th><?= __('Lze projekt podpořit hlasováním?') ?></th>
                <th><?= __('Je výchozí pro nové navržené projekty?') ?></th>
                <th><?= __('Může navrhovatel projekt upravovat?') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($status as $state): ?>
                <?php $counter++; ?>
                <?php if ($limit === 0 || $counter < $limit): ?>
                    <tr>
                        <td><?= $state->id ?></td>
                        <td><?= $state->name ?></td>
                        <td><?= $state->is_public ? __('Veřejný') : __('Neveřejný') ?></td>
                        <td><?= $state->is_voting_enabled ? __('Hlasovatelný') : __('Ne') ?></td>
                        <td><?= $state->is_default_for_proposals ? __('Výchozí') : __('Ne') ?></td>
                        <td><?= $state->is_editable_by_proposer ? __('Lze upravovat') : __('Ne') ?></td>
                        <td>
                            <?= $this->Html->link(__('Upravit'), ['controller' => 'ProjectStatus', 'action' => 'addModify', 'organization_id' => $organization->id, 'state_id' => $state->id]) ?>
                            ,
                            <?= $this->Form->postLink(
                                __('Smazat'),
                                ['controller' => 'ProjectStatus', 'action' => 'delete', 'organization_id' => $organization->id, 'state_id' => $state->id, '_method' => 'POST'],
                                ['confirm' => __('Opravdu chcete smazat tento stav?'), 'class' => 'text-danger']
                            ) ?>
                        </td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <td colspan="7"><?= $this->Html->link(sprintf(__('A dalších %d ...'), count($status) - $limit), ['controller' => 'ProjectStatus', 'action' => 'index', 'organization_id' => $organization->id]) ?></td>
                    </tr>
                    <?php break; endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Přidat nový Stav projektů'), ['action' => 'addModify', 'controller' => 'ProjectStatus', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>
