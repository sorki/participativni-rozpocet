<?php
/**
 * @var $this AppView
 */

use App\View\AppView;

?>
<ul class="navbar-nav ml-auto">
<!--    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="sectionsDropdown" role="button" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <?= __('Administrace') ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="sectionsDropdown">
        </div>
    </li>-->
    <li class="nav-item">
        <?= $this->Html->link(__('Moje Organizace'), ['action' => 'index', 'controller' => 'Organizations'], ['class' => 'btn btn-success m-2']) ?>
    </li>
    <li class="nav-item">
        <?= $this->Html->link(__('Odhlásit se'), ['_name' => 'logout'], ['class' => 'btn btn-danger m-2']) ?>
    </li>
</ul>
