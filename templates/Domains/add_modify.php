<?php

use App\Model\Entity\Domain;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $domain Domain
 */

$this->assign('title', $domain->isNew() ? __('Přidání nové domény') : __('Úprava domény'));
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->create($domain);
        echo $this->Form->control(Domain::FIELD_DOMAIN, ['label' => __('Doména bez protokolu, např. rozpocet.brno.cz')]);
        echo $this->Form->control(Domain::FIELD_IS_ENABLED, ['label' => __('Je doména povolena?'), 'type' => 'checkbox']);
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>
