<?php

use App\Model\Entity\Organization;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 */

$this->assign('title', $organization->isNew() ? __('Vytvoření nové organizace') : __('Úprava organizace'));

echo $this->Form->create($organization);
?>
    <div class="card m-2">
        <h2 class="card-header">
            <?= $this->fetch('title') ?>
        </h2>
        <div class="card-body">
            <?= $this->Form->control(Organization::FIELD_NAME, ['label' => __('Název obce nebo instituce, např. Brno - Medlánky')]) ?>
            <?= $this->Form->control(Organization::FIELD_IS_ENABLED, ['label' => __('Je organizace povolena (pokud není, nebude zobrazena ve veřejné galerii projektů a její data nebudou dostupná skrze API)'), 'type' => 'checkbox']) ?>
        </div>
        <div class="card-footer">
            <?= $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php
echo $this->Form->end();
