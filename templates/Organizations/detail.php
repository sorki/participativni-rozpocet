<?php

use App\Model\Entity\Organization;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 */

$this->assign('title', $organization->name);
?>
<div class="card m-2">
    <h2 class="card-header"><?= $organization->name ?></h2>
    <div class="card-body">
        <?= sprintf("%s %s", __('Organizace povolena?'), $organization->is_enabled ? __('ANO') : __('NE')) ?>
        <div class="alert alert-info m-2">
            <?= __('Pokud organizace není povolena, není vidět ve veřejné galerii projektů, není dostupná v API a nefungují ani její dotační portály (domény), pokud jsou nastaveny') ?>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Upravit organizaci'), ['action' => 'addModify', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<div class="card m-2">
    <h3 class="card-header"><?= __('Ročníky') ?></h3>
    <div class="table-responsive card-body">
        <div class="alert alert-info">
            <?= __('Ročník (nebo Výzva) je řada po sobě jdoucích etap průběhu participativního rozpočtu. Jednotlivé etapy, projekty, atd. můžete spravovat v detailu příslušné výzvy') ?>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th><?= __('Rok') ?></th>
                <th><?= __('Odkaz na informace') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($organization->appeals as $appeal): ?>
                <tr>
                    <td><?= $this->Html->link($appeal->year, ['controller' => 'Appeals', 'action' => 'detail', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id], ['class' => 'text-success']) ?></td>
                    <td><?= empty($appeal->info_link) ? '' : $this->Html->link($appeal->info_link, $appeal->info_link, ['class' => 'text-dark']) ?></td>
                    <td>
                        <?= $this->Html->link(
                            __('Otevřít'),
                            ['controller' => 'Appeals', 'action' => 'detail', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id],
                            ['class' => 'text-success'],
                        ) ?>
                        ,
                        <?= $this->Html->link(
                            __('Upravit'),
                            ['controller' => 'Appeals', 'action' => 'addModify', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id],
                        ) ?>
                        ,
                        <?= $this->Html->link(
                            __('Vytvořit kopii'),
                            ['controller' => 'Appeals', 'action' => 'makeCopy', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id],
                            ['class' => 'text-primary'],
                        ) ?>
                        ,
                        <?= $this->Form->postLink(
                            __('Smazat'),
                            ['controller' => 'Appeals', 'action' => 'delete', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id, '_method' => 'POST'],
                            ['class' => 'text-danger'],
                        ) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Vytvořit nový ročník'), ['action' => 'addModify', 'controller' => 'Appeals', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<?php
echo $this->element('card_project_status', ['organization' => $organization, 'status' => $organization->project_status, 'limit' => 10]);
echo $this->element('card_project_categories', ['organization' => $organization, 'categories' => $organization->project_categories, 'limit' => 5]);
?>

<div class="card m-2">
    <h3 class="card-header"><?= __('Domény') ?></h3>
    <div class="table-responsive card-body">
        <div class="alert alert-info">
            <?= __('Domény které jsou povoleny pro danou organizaci, aby přistupovaly k API, zobrazovaly projekty uživatelům a přesměrovávaly na hlasování o projektech') ?>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th><?= __('Doména') ?></th>
                <th><?= __('Povolena?') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($organization->domains as $domain): ?>
                <tr>
                    <td><?= $this->Html->link($domain->domain, ['controller' => 'Domains', 'action' => 'addModify', 'appeal_id' => $domain->id, 'organization_id' => $organization->id, 'domain_id' => $domain->id], ['class' => 'text-success']) ?></td>
                    <td><?= $domain->is_enabled ? __('Povolena') : __('Zakázána') ?></td>
                    <td>
                        <?= $this->Html->link(__('Upravit'), ['controller' => 'Domains', 'action' => 'addModify', 'organization_id' => $organization->id, 'domain_id' => $domain->id]) ?>
                        ,
                        <?= $this->Form->postLink(
                            __('Smazat'),
                            ['controller' => 'Domains', 'action' => 'delete', 'organization_id' => $organization->id, 'domain_id' => $domain->id, '_method' => 'POST'],
                            ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat tuto doménu?')]
                        ) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Přidat doménu'), ['action' => 'addModify', 'controller' => 'Domains', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>
