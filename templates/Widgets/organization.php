<?php

use App\Model\Entity\Organization;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 */
$this->assign('title', sprintf("%s - %s", $organization->name, __('Galerie projektů')));

$this->Html->css([
    '/widgets/common/bootstrap-prefix/bootstrap-4.5.2.paro2.min.css',
    '/widgets/jquery.paro2-widgets.css'
], ['block' => true]);
$this->Html->script([
    '/widgets/common/bootstrap-prefix/bootstrap-4.5.2.bundle.paro2.min.js',
    '/widgets/jquery.paro2-widgets.js'
], ['block' => true]);
$this->Html->scriptBlock(sprintf('$(function () { $("#gallery").participativniRozpocet({organization:%d, texts:{label_organizations:"Obec"}}); });', $organization->id), ['block' => true]);
?>
<div id="gallery" class="w-100 mb-4">

</div>

