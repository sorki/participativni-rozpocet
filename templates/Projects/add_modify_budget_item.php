<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectBudgetItem;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $project Project
 * @var $item ProjectBudgetItem
 * @var $appeal Appeal
 * @var $organization Organization
 */

$this->assign('title', $item->isNew() ? __('Nová položka rozpočtu') : __('Upravit položku'));

echo $this->Form->create($item);
?>
<div class="card m-2">
    <h2 class="card-header">
        <?= $this->fetch('title') ?>
    </h2>
    <div class="card-body">
        <?php
        echo $this->Form->control('description', ['label' => __('Popis položky')]);
        echo $this->Form->control('item_price', ['label' => __('Cena za 1 kus')]);
        echo $this->Form->control('item_count', ['label' => __('Počet kusů')]);
        echo $this->Form->control('is_final', ['type' => 'checkbox', 'label' => __('Zaškrtněte pokud se jedná o položku finální, vysoutěženou při realizaci')]);
        ?>
    </div>
    <div class="card-footer">
        <?= $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success m-2']) ?>
        <?= $this->Form->button(__('Uložit a vytvořit další položku'), ['type' => 'submit', 'class' => 'btn btn-success m-2', 'name' => 'SAVE_AND_CREATE_NEW']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
