<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Attachment;
use App\Model\Entity\AttachmentType;
use App\Model\Entity\Organization;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectBudgetItem;
use App\Model\Entity\ProjectLog;
use App\Model\Entity\ProjectLogType;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\Utility\Hash;

/**
 * @var $this AppView
 * @var $project Project
 * @var $appeal Appeal
 * @var $organization Organization
 * @var $emptyLog ProjectLog
 * @var $emptyBudgetItem ProjectBudgetItem
 * @var $emptyAttachment Attachment
 * @var $attachmentTypes array
 */

$this->assign('title', h($project->name));
?>
<div class="card m-2">
    <div class="card-header">
        <h2>
            <?= $this->fetch('title') ?>
        </h2>
        <?php
        echo $this->Html->link(__('Upravit'), ['action' => 'addModify', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id], ['class' => 'btn btn-success']);
        ?>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-bordered">
            <thead class="thead-dark">
            <tr>
                <th><?= __('Popis') ?></th>
                <th><?= __('Obsah') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $listing = [
                'description' => __('Popis'),
                'progress' => __('Postup realizace projektu (%)'),
                'address' => __('Adresa / Místo realizace'),
                'public_interest' => __('Veřejný přínos projektu'),
                'gps_latitude' => __('GPS Latitude'),
                'gps_longitude' => __('GPS Longitude'),
                'project_status.name' => __('Stav projektu')
            ];
            foreach ($listing as $path => $description):
                ?>
                <tr>
                    <td><?= $description ?></td>
                    <td><?= Hash::get($project, $path) ?></td>
                </tr>
            <?php
            endforeach;
            ?>
            <tr>
                <td><?= __('Kategorie') ?></td>
                <td>
                    <?php
                    foreach ($project->project_categories as $category) {
                        echo $category->name . ', ';
                    }
                    ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="card m-2">
    <div class="card-header">
        <h2><?= __('Rozpočet projektu') ?></h2>
        <?= $this->Html->link(__('Přidat položku'), ['action' => 'addModifyBudgetItem', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="card-body table-responsive">
        <?php
        $budgets = [
            __('Navržený') => false,
            __('Schválený') => true,
        ];
        foreach ($budgets as $title => $approvedState):
            ?>
            <h3><?= $title ?></h3>
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th><?= __('Položka') ?></th>
                    <th><?= __('Položková cena') ?></th>
                    <th><?= __('Počet položek') ?></th>
                    <th><?= __('Cena celkem') ?></th>
                    <th><?= __('Akce') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($project->project_budget_items as $item): ?>
                    <?php if ($item->is_final !== $approvedState) {
                        continue;
                    } ?>
                    <tr>
                        <td><?= $item->description ?></td>
                        <td><?= Number::currency($item->item_price, 'CZK') ?></td>
                        <td><?= $item->item_count ?></td>
                        <td><?= Number::currency($item->total_price, 'CZK') ?></td>
                        <td>
                            <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyBudgetItem', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'item_id' => $item->id], ['class' => 'text-success']) ?>
                            ,
                            <?= $this->Html->link(__('Smazat'), ['action' => 'deleteBudgetItem', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'item_id' => $item->id], ['class' => 'text-danger']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endforeach; ?>
    </div>
    <div class="card-footer" id="rozpocet">
        <strong><?= __('Rychlé přidání položky') ?></strong>
        <?php
        echo $this->Form->create($emptyBudgetItem, [
            'url' => ['action' => 'addModifyBudgetItem', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id, '#' => 'rozpocet']
        ]);

        echo $this->Form->control('description', ['label' => __('Popis položky'), 'type' => 'string']);
        echo $this->Form->control('item_price', ['label' => __('Cena 1 kusu')]);
        echo $this->Form->control('item_count', ['label' => __('Počet kusů')]);
        echo $this->Form->control('is_final', ['type' => 'checkbox', 'label' => __('Zaškrtněte pokud se jedná o položku finální, vysoutěženou při realizaci')]);

        echo $this->Form->submit(__('Přidat'));
        echo $this->Form->end();
        ?>
    </div>
</div>

<div class="card m-2">
    <div class="card-header">
        <h2><?= __('Přílohy') ?></h2>
        <?= $this->Html->link(__('Přidat přílohu'), ['action' => 'addModifyAttachment', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-bordered">
            <thead class="thead-dark">
            <tr>
                <th><?= __('Název') ?></th>
                <th><?= __('Typ přílohy') ?></th>
                <th><?= __('Velikost') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($project->attachments as $attachment): ?>
                <tr>
                    <td><?= sprintf("%s%s", $attachment->title, $attachment->id === $project->title_image_id ? __(' (titulní foto)') : '') ?></td>
                    <td><?= AttachmentType::getLabel($attachment->attachment_type_id) ?></td>
                    <td><?= Number::toReadableSize($attachment->filesize) ?></td>
                    <td>
                        <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyAttachment', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'attachment_id' => $attachment->id], ['class' => 'text-success']) ?>
                        ,
                        <?= $this->Html->link(__('Smazat'), ['action' => 'deleteAttachment', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'attachment_id' => $attachment->id], ['class' => 'text-danger']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer" id="prilohy">
        <strong><?= __('Rychlé přidání přílohy') ?></strong>
        <?php
        echo $this->Form->create($emptyAttachment, [
            'type' => 'file',
            'url' => ['action' => 'addModifyAttachment', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id, '#' => 'prilohy']
        ]);

        echo $this->Form->control('title', ['label' => __('Název přílohy'), 'type' => 'string']);
        echo $this->Form->control('attachment_type_id', ['label' => __('Typ přílohy'), 'options' => $attachmentTypes]);
        echo $this->Form->control('filedata', ['type' => 'file', 'label' => __('Soubor')]);
        echo $this->Form->control('is_title_image', ['label' => __('Zaškrtněte pokud je příloha titulním obrázkem projektu'), 'type' => 'checkbox']);

        echo $this->Form->submit(__('Přidat'));
        echo $this->Form->end();
        ?>
    </div>
</div>

<?php foreach (ProjectLogType::ALL_TYPES as $logTypeId): ?>
    <div class="card m-2">
        <div class="card-header">
            <h2><?= ProjectLogType::getLabel($logTypeId) ?></h2>
            <?= $this->Html->link(__('Přidat nový záznam'), ['action' => 'addModifyLog', 'log_type' => $logTypeId, 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th><?= __('Nadpis') ?></th>
                    <th><?= __('Text') ?></th>
                    <th><?= __('Platné od') ?></th>
                    <th><?= __('Akce') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($project->getLogsByType($logTypeId) as $log): ?>
                    <tr>
                        <td><?= $log->title ?></td>
                        <td><?= $log->description ?></td>
                        <td><?= $log->date_when->nice() ?></td>
                        <td>
                            <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyLog', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'log_type' => $log->project_log_type_id, 'log_id' => $log->id], ['class' => 'text-success']) ?>
                            ,
                            <?= $this->Html->link(__('Smazat'), ['action' => 'deleteLog', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'log_type' => $log->project_log_type_id, 'log_id' => $log->id], ['class' => 'text-danger']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="card-footer" id="logs<?= $logTypeId ?>">
            <strong><?= __('Rychlé přidání záznamu') ?></strong>
            <?php
            echo $this->Form->create($emptyLog, [
                'url' => ['action' => 'addModifyLog', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id, 'log_type' => $logTypeId, '#' => 'logs' . $logTypeId]
            ]);

            echo $this->Form->control('title', ['label' => __('Nadpis')]);
            echo $this->Form->control('description', ['label' => __('Obsah'), 'type' => 'string']);
            echo $this->Form->control('date_when', ['label' => __('Platné od')]);

            echo $this->Form->submit(__('Přidat'));
            echo $this->Form->end();
            ?>
        </div>
    </div>
<?php endforeach; ?>
