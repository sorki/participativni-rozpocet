<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Attachment;
use App\Model\Entity\AttachmentType;
use App\Model\Entity\Organization;
use App\Model\Entity\Project;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $project Project
 * @var $attachment Attachment
 * @var $appeal Appeal
 * @var $organization Organization
 * @var $attachmentTypes array
 */

$this->assign('title', $attachment->isNew() ? __('Nová příloha') : __('Upravit přílohu'));

echo $this->Form->create($attachment, ['type' => 'file']);
?>
<div class="card m-2">
    <h2 class="card-header">
        <?= $this->fetch('title') ?>
    </h2>
    <div class="card-body">
        <?php
        echo $this->Form->control('title', ['label' => __('Název souboru')]);
        echo $this->Form->control('attachment_type_id', ['options' => $attachmentTypes, 'label' => __('Typ přílohy')]);
        echo $this->Form->control('is_title_image', ['label' => __('Zaškrtněte pokud je příloha titulním obrázkem projektu'), 'type' => 'checkbox', 'checked' => $project->title_image_id === $attachment->id]);
        if ($attachment->isNew()) {
            echo $this->Form->control('filedata', ['type' => 'file', 'label' => __('Soubor')]);
        } else {
            echo $this->Form->control('original_filename', ['label' => __('Název souboru'), 'disabled' => true]);
            echo $this->Form->control('filesize', ['label' => __('Velikost souboru'), 'disabled' => true]);
            if ($attachment->attachment_type_id === AttachmentType::TYPE_PHOTO) {
                echo $this->Html->image(['action' => 'downloadAttachment', 'organization_id' => $organization->id, 'appeal_id' => $project->appeal_id, 'project_id' => $project->id, 'attachment_id' => $attachment->id, 'force' => false], ['class' => 'mw-100']) . '<br/>';
            }
        }
        ?>
    </div>
    <div class="card-footer">
        <?= $this->Form->button(__('Uložit'), ['type' => 'submit', 'class' => 'btn btn-success']) ?>
        <?= $this->Html->link(__('Stáhnout přílohu'), ['action' => 'downloadAttachment', 'organization_id' => $organization->id, 'appeal_id' => $project->appeal_id, 'project_id' => $project->id, 'attachment_id' => $attachment->id, 'force' => true], ['class' => 'btn btn-primary']); ?>

    </div>
</div>
<?= $this->Form->end() ?>
