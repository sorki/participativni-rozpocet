<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectLog;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $project Project
 * @var $log ProjectLog
 * @var $appeal Appeal
 * @var $organization Organization
 */

$this->assign('title', $log->isNew() ? __('Nový záznam') : __('Upravit záznam'));

echo $this->Form->create($log);
?>
<div class="card m-2">
    <h2 class="card-header">
        <?= $this->fetch('title') ?>
    </h2>
    <div class="card-body">
        <?php
        echo $this->Form->control('title', ['label' => __('Nadpis')]);
        echo $this->Form->control('description', ['label' => __('Obsah')]);
        echo $this->Form->control('date_when', ['label' => __('Platné od')]);
        ?>
    </div>
    <div class="card-footer">
        <?= $this->Form->submit(__('Uložit')) ?>
    </div>
</div>
<?= $this->Form->end() ?>
