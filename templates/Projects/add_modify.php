<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\Model\Entity\Project;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $appeal Appeal
 * @var $project Project
 * @var $projectStates array
 * @var $projectCategories array
 */

$this->assign('title', $project->isNew() ? __('Nový projekt') : h($project->name));

echo $this->Form->create($project);
?>
<div class="card m-2">
    <div class="card-header">
        <h2><?= $this->fetch('title') ?></h2>
        <p>
            <?=
            __('Formulář slouží k evidenci papírově přijatého návrhu projektu, nebo drobných úpravám.') .
            '<br/>' .
            __('Pro úpravu rozpočtu, příloh, fotografií, záznamů o průběhu realizace nebo vyjádření úřadu musíte do detailu projektu.')
            ?>
        </p>
        <?php
        if (!$project->isNew()) {
            echo $this->Html->link(__('Otevřít detail projektu'), ['action' => 'detail', 'project_id' => $project->id, 'appeal_id' => $appeal->id, 'organization_id' => $organization->id], ['class' => 'btn btn-success']);
        }
        ?>
    </div>
    <div class="card-body">
        <strong><?= __('Zařazení projektu') ?></strong>
        <?php
        echo $this->Form->control('project_status_id', ['options' => $projectStates, 'label' => __('Aktuální stav projektu')]);
        echo $this->Form->control('project_categories._ids', ['options' => $projectCategories, 'label' => __('Kategorie do kterých projekt zapadá'), 'empty' => true, 'class' => 'select2']);
        ?>
        <hr/>
        <strong><?= __('Základní informace o projektu') ?></strong>
        <?php
        echo $this->Form->control('name', ['label' => __('Název projektu')]);
        echo $this->Form->control('description', ['label' => __('Popis projektu'), 'data-use-quill' => 'data-use-quill']);
        echo $this->Form->control('public_interest', ['label' => __('Veřejný přínos projektu')]);
        ?>
        <hr/>
        <strong><?= __('Kontakty na navrhovatele') ?></strong>
        <?php
        $canModifyContact = $project->isNew() || empty($project->contact->identity_provider_id);
        echo $this->Form->control('contact.name', ['label' => __('Jméno a Příjmení'), 'disabled' => !$canModifyContact]);
        echo $this->Form->control('contact.email', ['label' => __('E-mailová adresa'), 'disabled' => !$canModifyContact]);
        echo $this->Form->control('contact.phone', ['label' => __('Telefonní číslo'), 'disabled' => !$canModifyContact]);
        ?>
        <hr/>
        <strong><?= __('Umístění') ?></strong>
        <?php
        echo $this->Form->control('address', ['label' => __('Adresa místa realizace projektu')]);
        echo $this->Form->control('gps_latitude', ['label' => __('Zeměpisná šířka (GPS, např. 50.0588364)'), 'step' => "0.0000001"]);
        echo $this->Form->control('gps_longitude', ['label' => __('Zeměpisná délka (GPS, např. 14.4125592)'), 'step' => "0.0000001"]);
        ?>
        <hr>
        <strong><?= __('Postup realizace projektu') ?></strong>
        <?php
        echo $this->Form->control('progress', ['label' => __('Procentní vyjádření stavu realizace projektu, 0-100%'), 'step' => 1]);
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit(__('Uložit'));
        ?>
    </div>
</div>
<?php
echo $this->Form->end();
?>
