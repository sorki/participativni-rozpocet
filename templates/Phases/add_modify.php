<?php
/**
 * @var $this AppView
 * @var $appeal Appeal
 * @var $phase Phase
 * @var $organization Organization
 * @var $identity_providers array
 */

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\Model\Entity\Phase;
use App\View\AppView;

$this->assign('title', $phase->isNew() ? __('Nová fáze') : sprintf("%s: %s", __('Fáze'), $phase->name));

?>
<div class="m-2 card">
    <h2 class="card-header">
        <?= $this->fetch('title') ?>
    </h2>
    <div class="card-body">
        <h3><?= __('Základní informace o fázi') ?></h3>
        <?php
        echo $this->Form->create($phase);

        echo $this->Form->control(Phase::FIELD_NAME, ['label' => __('Název fáze')]);
        echo $this->Form->control(Phase::FIELD_DESCRIPTION, ['label' => __('Popis fáze'), 'data-use-quill' => 'data-use-quill']);
        ?>
        <hr/>
        <h3><?= __('Průběh fáze') ?></h3>
        <?php

        echo $this->Form->control(Phase::FIELD_DATE_START, ['label' => __('Začátek'), 'default' => date('Y-m-d')]);
        echo $this->Form->control(Phase::FIELD_DATE_END, ['label' => __('Konec'), 'default' => date('Y-m-d')]);
        ?>
        <hr/>
        <h3><?= __('Nastavení příjmu návrhů projektů') ?></h3>
        <?php
        echo $this->Form->control(Phase::FIELD_ALLOWS_PROPOSALS_SUBMISSION, ['label' => __('Je v této fázi dovoleno navrhovat projekty?'), 'type' => 'checkbox']);
        ?>
        <hr/>
        <h3><?= __('Nastavení hlasování') ?></h3>
        <?php
        echo $this->Form->control(Phase::FIELD_ALLOWS_VOTING, ['label' => __('Je v této fázi dovoleno hlasovat / podporovat projekty?'), 'type' => 'checkbox']);
        echo $this->Form->control(Phase::FIELD_MINIMUM_VOTES_COUNT, ['label' => __('Pokud je v této fázi povoleno hlasovat pro projekty, uveďte minimální počet hlasů (pro elektronické hlasování), které musí projekt získat, aby v této fázi uspěl')]);
        ?>
        <hr/>
        <h3><?= __('Nastavení ověření uživatelů') ?></h3>
        <?php
        echo $this->Form->control(Phase::FIELD_IDENTITY_PROVIDERS . '._ids', ['label' => __('Pokud je v této fázi povoleno hlasovat nebo navrhovat projekty, vyberte způsoby ověření uživatele, které jsou přípustné'), 'class' => 'select2', 'empty' => true, 'options' => $identity_providers]);
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit();
        echo $this->Form->end();
        ?>
    </div>
</div>
