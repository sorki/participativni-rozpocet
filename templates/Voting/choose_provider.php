<?php

use App\Controller\Component\VotingComponent;
use App\Model\Entity\Project;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $providers array (provider-id => provider-name)
 * @var $allowedRedirectUrl string
 * @var $component VotingComponent
 * @var $project Project
 */
?>
<div class="row">
    <div class="col-md-8 offset-md-2 text-center p-4 mt-4 bg-light">
        <div class="h2">
            <?= __('Vyberte si prosím způsob ověření vašeho hlasu') ?>
        </div>
        <hr/>
        <div class="row">
            <?php foreach ($providers as $provider_id => $provider_name): ?>
                <div class="col-12">
                    <div class="d-flex text-center alert alert-success">
                        <?= $this->Html->link($provider_name, [
                            '_name' => 'voting_interactive',
                            'otp' => $component->getOTP(),
                            'provider_id' => $provider_id,
                        ], [
                            'class' => 'stretched-link text-dark text-decoration-none'
                        ]) ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <hr/>
        <?php
        echo $this->Html->link(__('Klikněte zde pro návrat do galerie projektů'), $allowedRedirectUrl ?? '/', ['class' => 'text-dark underline']);
        ?>
    </div>
</div>

