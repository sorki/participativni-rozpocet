<?php
/**
 * @var $this AppView
 */

use App\View\AppView;

?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= __('Participativní rozpočet') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css([
        'bootstrap-4.5.2.min.css',
        'paro2.css',
        'fontawesome-5.14.0.min.css',
    ]) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>
<body class="bg-dark">

<div class="container clearfix mt-2">
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>
</div>

<?= $this->fetch('script') ?>
</body>
</html>
