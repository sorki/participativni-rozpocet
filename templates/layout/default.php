<?php
/**
 * @var $this AppView
 */

use App\View\AppView;
use Cake\Routing\Router;

?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css([
        'bootstrap-4.5.2.min.css',
        'paro2.css',
        'fontawesome-5.14.0.min.css',
        'quill-1.3.6.snow.css',
        'select2-4.0.13.min.css',
    ]) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Participativní Rozpočet</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavbar"
            aria-controls="mainNavbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="mainNavbar">
        <?= $this->Html->link(__('Galerie projektů'), ['_name' => 'public_gallery'], ['class'=>'btn btn-success mr-auto']) ?>
        <?php
        if ($this->isUser()) {
            echo $this->element('main_menu_user');
        } else {
            echo $this->element('main_menu_not_user');
        }
        ?>
    </div>
</nav>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb rounded-0">

        <li class="breadcrumb-item">
            <?= $this->Html->link(__('Hlavní stránka'), '/') ?>
        </li>
        <?php if (isset($crumbs)): ?>
            <?php foreach ($crumbs as $crumb_name => $crumb_path): ?>
                <li class="breadcrumb-item text-light">
                    <?= $this->Html->link($crumb_name, (is_string($crumb_path) && Router::routeExists(['_name' => $crumb_path])) ? ['_name' => $crumb_path] : $crumb_path) ?>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if ($this->getRequest() && $this->getRequest()->getRequestTarget() !== '/'): ?>
            <li class="breadcrumb-item active" aria-current="page">
                <?= $this->Html->link($this->fetch('title'), $this->getRequest()->getRequestTarget()) ?>
            </li>
        <?php endif; ?>
    </ol>
</nav>

<div class="container-fluid clearfix mt-2">
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>
</div>

<?= $this->Html->script([
    'jquery-3.5.1.min.js',
    'popper-1.16.1.js',
    'fontawesome-5.14.0.min.js',
    'bootstrap-4.5.2.min.js',
    'bs-custom-file-input-1.3.4.min.js',
    'quill-1.3.6.js',
    'quill-textarea.js',
    'select2-4.0.13.min.js',
    'select2-apply.js',
]) ?>
<?= $this->fetch('script') ?>
</body>
</html>
