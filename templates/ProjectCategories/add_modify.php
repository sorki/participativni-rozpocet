<?php

use App\Model\Entity\Organization;
use App\Model\Entity\ProjectCategory;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $category ProjectCategory
 */

$this->assign('title', $category->isNew() ? __('Vytvořit novou kategorii') : __('Upravit kategorii'))
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->create($category);
        echo $this->Form->control('name', ['label' => __('Název kategorie')]);
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit();
        echo $this->Form->end();
        ?>
    </div>
</div>
