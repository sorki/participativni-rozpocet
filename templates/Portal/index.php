<?php
/**
 * @var $this AppView
 * @var $organization Organization|null
 */

use App\Model\Entity\Organization;
use App\View\AppView;

?>
<noscript>
    Pro zobrazení obsahu musíte mít v prohlížeči povolený Javascript
</noscript>
<style type="text/css">
    /* Tento styl je nezbytný jen pro tento portál, v případě použití widgetu na jiných stránkách, se widget přizpůsobí příslušnému cíli */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    #master-container {
        width: 100%;
        min-height: 100%;
    }
</style>
<div id="master-container">
</div>
<link rel="stylesheet" href="/widgets/common/bootstrap-prefix/bootstrap-4.5.2.paro2.min.css">
<link rel="stylesheet" href="/widgets/jquery.paro2-widgets.css">
<link rel="stylesheet" href="/css/fontawesome-5.14.0.min.css"/>
<script type="text/javascript" src="/widgets/common/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="/widgets/common/bootstrap-prefix/bootstrap-4.5.2.bundle.paro2.min.js"></script>
<script type="text/javascript" src="/widgets/jquery.paro2-widgets.js"></script>
<script src="/js/fontawesome-5.14.0.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#master-container").participativniRozpocet({
            organization: <?= $organization ? $organization->id : 'false' ?>

        });
    });
</script>
