<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $appeal Appeal
 * @var $organization Organization
 */

$this->assign('title', sprintf("%s %d", __('Ročník'), $appeal->year));
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?= __('Odkaz na informace o výzvě') ?>: <?= $appeal->info_link ?>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Upravit'), ['action' => 'addModify', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
        <?= $this->Html->link(__('Vytvořit kopii'), ['action' => 'makeCopy', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id], ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<div class="card m-2">
    <h2 class="card-header">
        <?= __('Projekty') ?>
    </h2>
    <div class="card-body table-responsive">
        <table class="table table-bordered">
            <thead class="thead-dark">
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('Název') ?></th>
                <th><?= __('Stav') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($appeal->projects as $project): ?>
                <tr>
                    <td><?= $project->id ?></td>
                    <td><?= h($project->name) ?></td>
                    <td><?= $project->project_status->name ?></td>
                    <td>
                        <?= $this->Html->link(__('Detail'), ['action' => 'detail', 'controller' => 'Projects', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id], ['class' => 'text-success']) ?>
                        ,
                        <?= $this->Html->link(__('Upravit'), ['action' => 'addModify', 'controller' => 'Projects', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id], ['class' => 'text-primary']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Přidat projekt'), ['action' => 'addModify', 'controller' => 'Projects', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<div class="card m-2">
    <h2 class="card-header"><?= __('Fáze / Harmonogram') ?></h2>
    <div class="card-body table-responsive">
        <div class="alert alert-info">
            <?= __('Jednotlivé fáze, tak jak jsou nezbytné pro celý průběh ročníku participativního rozpočtu') ?>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th><?= __('Od (včetně)') ?></th>
                <th><?= __('Do (včetně)') ?></th>
                <th><?= __('Název') ?></th>
                <th><?= __('Popis') ?></th>
                <th><?= __('Minimální počet elektronických hlasů pro úspěch projektu') ?></th>
                <th><?= __('Lze navrhovat projekty?') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($appeal->phases as $phase): ?>
                <tr>
                    <td><?= $phase->date_start->format('d.m.Y') ?></td>
                    <td><?= $phase->date_end->format('d.m.Y') ?></td>
                    <td><?= $phase->name ?></td>
                    <td><?= $phase->description ?></td>
                    <td><?= empty($phase->identity_providers) ? __('V této fázi nelze hlasovat') : ($phase->minimum_votes_count > 0 ? $phase->minimum_votes_count : __('Minimální počet hlasů není vyžadován')) ?></td>
                    <td><?= $phase->allows_proposals_submission ? __('ANO') : __('V této fázi nelze navrhovat projekty') ?></td>
                    <td>
                        <?= $this->Html->link(__('Upravit'), ['action' => 'addModify', 'controller' => 'Phases', 'appeal_id' => $phase->appeal_id, 'phase_id' => $phase->id, 'organization_id' => $organization->id]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Přidat další fázi'), ['action' => 'addModify', 'controller' => 'Phases', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>
