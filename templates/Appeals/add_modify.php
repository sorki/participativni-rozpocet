<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $appeal Appeal
 * @var $organization Organization
 */

$this->assign('title', $appeal->isNew() ? __('Přidání nového ročníku') : __('Úprava ročníku'));
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->create($appeal);
        echo $this->Form->control(Appeal::FIELD_YEAR, ['label' => __('Rok')]);
        echo $this->Form->control(Appeal::FIELD_INFO_LINK, ['label' => __('Odkaz na informace k tomuto ročníku')]);
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>
