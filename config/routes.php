<?php

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

const REGEX_ANY_NUMBER = '[0-9]+';
const REGEX_POSITIVE_NUMBER = '[1-9][0-9]*';

/** @var RouteBuilder $routes */
$routes->setRouteClass(DashedRoute::class);

$routes->scope('/', function (RouteBuilder $routes) {

    // basic
    $routes->connect('/', ['controller' => 'Public', 'action' => 'index'])->setMethods(['GET']);

    // users
    $routes->connect('/login',
        ['controller' => 'Users', 'action' => 'login'],
        ['_name' => 'login']
    )->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/logout',
        ['controller' => 'Users', 'action' => 'logout'],
        ['_name' => 'logout']
    )->setMethods(['GET']);
    $routes->connect('/register',
        ['controller' => 'Users', 'action' => 'register'],
        ['_name' => 'register']
    )->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // public organization presentation, js widget use-cases
    $routes->connect('/public/{organization_id}',
        ['controller' => 'Widgets', 'action' => 'organization'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'public_organization_detail']
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/public',
        ['controller' => 'Widgets', 'action' => 'discover'],
        ['_name' => 'public_gallery']
    )->setMethods(['GET']);
    $routes->connect('/public/widgets',
        ['controller' => 'Widgets', 'action' => 'index'],
        ['_name' => 'public_widgets']
    )->setMethods(['GET']);

    // public portal, SaaS
    $routes->connect('/portal/index',
        ['controller' => 'Portal', 'action' => 'index'],
        ['_name' => 'portal_index']
    )->setMethods(['GET']);

    // organizations
    $routes->connect('/organizations',
        ['controller' => 'Organizations', 'action' => 'index'],
        ['_name' => 'my_organizations']
    )->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/edit',
        ['controller' => 'Organizations', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/add',
        ['controller' => 'Organizations', 'action' => 'addModify'],
        ['_name' => 'organization_add']
    )->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}',
        ['controller' => 'Organizations', 'action' => 'detail'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'organization_detail']
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/delete',
        ['controller' => 'Organizations', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'organization_delete']
    )->setPass(['organization_id'])->setMethods(['POST']);

    // domains
    $routes->connect('/organizations/{organization_id}/domains/{domain_id}/edit',
        ['controller' => 'Domains', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'domain_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'domain_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/domains/add',
        ['controller' => 'Domains', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/domains/{domain_id}/delete',
        ['controller' => 'Domains', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'domain_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'domain_id'])->setMethods(['POST']);

    // project categories
    $routes->connect('/organizations/{organization_id}/categories',
        ['controller' => 'ProjectCategories', 'action' => 'index'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/categories/{category_id}/edit',
        ['controller' => 'ProjectCategories', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'category_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'category_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/categories/{category_id}/delete',
        ['controller' => 'ProjectCategories', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'category_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'category_id'])->setMethods(['POST']);
    $routes->connect('/organizations/{organization_id}/categories/add',
        ['controller' => 'ProjectCategories', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // project states
    $routes->connect('/organizations/{organization_id}/states',
        ['controller' => 'ProjectStatus', 'action' => 'index'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/states/{state_id}/edit',
        ['controller' => 'ProjectStatus', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'state_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'state_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/states/{state_id}/delete',
        ['controller' => 'ProjectStatus', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'state_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'state_id'])->setMethods(['POST']);
    $routes->connect('/organizations/{organization_id}/states/add',
        ['controller' => 'ProjectStatus', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'project_status_add']
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // appeals
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/edit',
        ['controller' => 'Appeals', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/add',
        ['controller' => 'Appeals', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'appeal_add']
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}',
        ['controller' => 'Appeals', 'action' => 'detail'],
        ['appeal_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'appeal_detail']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/copy',
        ['controller' => 'Appeals', 'action' => 'makeCopy'],
        ['appeal_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'appeal_copy']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/delete',
        ['controller' => 'Appeals', 'action' => 'delete'],
        ['appeal_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'appeal_delete']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['POST']);

    // phases
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/phases/{phase_id}/edit',
        ['controller' => 'Phases', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'phase_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'phase_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/phases/add',
        ['controller' => 'Phases', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/phases/{phase_id}',
        ['controller' => 'Phases', 'action' => 'detail'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'phase_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'phase_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // projects
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects',
        ['controller' => 'Projects', 'action' => 'index'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/edit',
        ['controller' => 'Projects', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/add',
        ['controller' => 'Projects', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}',
        ['controller' => 'Projects', 'action' => 'detail'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // project logs
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/logs/{log_type}/{log_id}/edit',
        ['controller' => 'Projects', 'action' => 'addModifyLog'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'log_id' => REGEX_POSITIVE_NUMBER, 'log_type' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'log_type', 'log_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/logs/{log_type}/add',
        ['controller' => 'Projects', 'action' => 'addModifyLog'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'log_type' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'log_type'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/logs/{log_type}/{log_id}/delete',
        ['controller' => 'Projects', 'action' => 'deleteLog'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'log_id' => REGEX_POSITIVE_NUMBER, 'log_type' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'log_type', 'log_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // project budget items
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/budget-items/{item_id}/edit',
        ['controller' => 'Projects', 'action' => 'addModifyBudgetItem'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'item_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'item_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/budget-items/add',
        ['controller' => 'Projects', 'action' => 'addModifyBudgetItem'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/budget-items/{item_id}/delete',
        ['controller' => 'Projects', 'action' => 'deleteBudgetItem'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'item_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'item_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // project attachments
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/edit',
        ['controller' => 'Projects', 'action' => 'addModifyAttachment'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/add',
        ['controller' => 'Projects', 'action' => 'addModifyAttachment'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/delete',
        ['controller' => 'Projects', 'action' => 'deleteAttachment'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/download',
        ['controller' => 'Projects', 'action' => 'downloadAttachment', 'force' => false],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id', 'force'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/download/force',
        ['controller' => 'Projects', 'action' => 'downloadAttachment', 'force' => true],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id', 'force'])->setMethods(['GET']);

    // custom_users
    $routes->connect('/organizations/{organization_id}/custom_users',
        ['controller' => 'CustomUsers', 'action' => 'index'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/custom_users/{user_id}/edit',
        ['controller' => 'CustomUsers', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'user_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'user_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/custom_users/add',
        ['controller' => 'CustomUsers', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/custom_users/{user_id}',
        ['controller' => 'CustomUsers', 'action' => 'detail'],
        ['user_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'user_id'])->setMethods(['GET']);

    // voting
    $routes->connect('/vote/result',
        ['controller' => 'Voting', 'action' => 'result'],
        ['_name' => 'voting_final_status']
    )->setMethods(['GET']);
    $routes->connect('/vote/choose-provider',
        ['controller' => 'Voting', 'action' => 'chooseProvider'],
        ['_name' => 'voting_choose_provider']
    )->setMethods(['GET']);
    $routes->connect('/vote/{provider_id}/{otp}',
        ['controller' => 'Voting', 'action' => 'interactive'],
        ['_name' => 'voting_interactive', 'provider_id' => REGEX_POSITIVE_NUMBER, 'otp' => '[0-9a-zA-Z]{16}']
    )->setPass(['provider_id', 'otp'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/vote/{provider_id}',
        ['controller' => 'Voting', 'action' => 'interactive'],
        ['_name' => 'voting_non_interactive', 'provider_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['provider_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/vote/metadata/{organization_id}/{provider_id}',
        ['controller' => 'Voting', 'action' => 'metadata'],
        ['_name' => 'voting_metadata', 'provider_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'provider_id'])->setMethods(['GET']);

    // api
    $routes->redirect('/api', '/api/v1/info.json');
    $routes->redirect('/api/v1', '/api/v1/info.json');
    $routes->connect('/api/v1/info.json',
        ['controller' => 'Api', 'action' => 'info'],
        ['_name' => 'api_info']
    )->setMethods(['GET']);
    $routes->connect('/api/v1/organizations.json',
        ['controller' => 'Api', 'action' => 'organizations']
    )->setMethods(['GET']);
    $routes->connect('/api/v1/organization/{organization_id}/info.json',
        ['controller' => 'Api', 'action' => 'organization'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_organization_info']
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/info.json',
        ['controller' => 'Api', 'action' => 'appeal'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_appeal_info']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/projects.json',
        ['controller' => 'Api', 'action' => 'appealProjects'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_appeal_projects']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/add_project',
        ['controller' => 'Api', 'action' => 'appealProposeProject'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_appeal_propose_project']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/project/{project_id}/add_vote',
        ['controller' => 'Api', 'action' => 'projectAddVote'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_project_add_vote']
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/project/{project_id}/attachment/{attachment_id}',
        ['controller' => 'Api', 'action' => 'projectAttachmentDownload'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_project_download_attachment']
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id'])->setMethods(['GET']);

});
