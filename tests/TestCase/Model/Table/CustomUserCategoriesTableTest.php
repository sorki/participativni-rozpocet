<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CustomUserCategoriesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CustomUserCategoriesTable Test Case
 */
class CustomUserCategoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var CustomUserCategoriesTable
     */
    protected $CustomUserCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.CustomUserCategories',
        'app.Organizations',
        'app.CustomUsersToCustomCategories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('CustomUserCategories') ? [] : ['className' => CustomUserCategoriesTable::class];
        $this->CustomUserCategories = $this->getTableLocator()->get('CustomUserCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->CustomUserCategories);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
