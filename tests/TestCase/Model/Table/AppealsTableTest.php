<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AppealsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AppealsTable Test Case
 */
class AppealsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var AppealsTable
     */
    protected $Appeals;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Appeals',
        'app.Organizations',
        'app.Phases',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Appeals') ? [] : ['className' => AppealsTable::class];
        $this->Appeals = $this->getTableLocator()->get('Appeals', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Appeals);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
