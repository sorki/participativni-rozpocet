<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectLogTypesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectLogTypesTable Test Case
 */
class ProjectLogTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var ProjectLogTypesTable
     */
    protected $ProjectLogTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ProjectLogTypes',
        'app.ProjectLogs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ProjectLogTypes') ? [] : ['className' => ProjectLogTypesTable::class];
        $this->ProjectLogTypes = $this->getTableLocator()->get('ProjectLogTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ProjectLogTypes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
