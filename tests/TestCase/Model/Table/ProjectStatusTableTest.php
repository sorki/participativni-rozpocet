<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectStatusTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectStatusTable Test Case
 */
class ProjectStatusTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var ProjectStatusTable
     */
    protected $ProjectStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ProjectStatus',
        'app.Organizations',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ProjectStatus') ? [] : ['className' => ProjectStatusTable::class];
        $this->ProjectStatus = $this->getTableLocator()->get('ProjectStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ProjectStatus);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
