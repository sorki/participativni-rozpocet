<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrganizationsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrganizationsTable Test Case
 */
class OrganizationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var OrganizationsTable
     */
    protected $Organizations;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Organizations',
        'app.Users',
        'app.Appeals',
        'app.CustomUserCategories',
        'app.CustomUsers',
        'app.Domains',
        'app.OrganizationSettings',
        'app.OrganizationsToUsers',
        'app.ProjectCategories',
        'app.ProjectStatus',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Organizations') ? [] : ['className' => OrganizationsTable::class];
        $this->Organizations = $this->getTableLocator()->get('Organizations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Organizations);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
