<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PhaseToProvidersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PhaseToProvidersTable Test Case
 */
class PhaseToProvidersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var PhaseToProvidersTable
     */
    protected $PhaseToProviders;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.PhaseToProviders',
        'app.Phases',
        'app.IdentityProviders',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('PhaseToProviders') ? [] : ['className' => PhaseToProvidersTable::class];
        $this->PhaseToProviders = $this->getTableLocator()->get('PhaseToProviders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->PhaseToProviders);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
