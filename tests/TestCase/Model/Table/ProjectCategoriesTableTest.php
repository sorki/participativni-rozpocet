<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectCategoriesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectCategoriesTable Test Case
 */
class ProjectCategoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var ProjectCategoriesTable
     */
    protected $ProjectCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ProjectCategories',
        'app.Organizations',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ProjectCategories') ? [] : ['className' => ProjectCategoriesTable::class];
        $this->ProjectCategories = $this->getTableLocator()->get('ProjectCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ProjectCategories);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
