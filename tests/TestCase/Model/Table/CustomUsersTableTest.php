<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CustomUsersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CustomUsersTable Test Case
 */
class CustomUsersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var CustomUsersTable
     */
    protected $CustomUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.CustomUsers',
        'app.Organizations',
        'app.Contacts',
        'app.CustomUsersToCustomCategories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('CustomUsers') ? [] : ['className' => CustomUsersTable::class];
        $this->CustomUsers = $this->getTableLocator()->get('CustomUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->CustomUsers);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
