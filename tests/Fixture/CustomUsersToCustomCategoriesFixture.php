<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CustomUsersToCustomCategoriesFixture
 */
class CustomUsersToCustomCategoriesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'custom_user_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'custom_user_category_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'custom_user_id' => ['type' => 'index', 'columns' => ['custom_user_id'], 'length' => []],
            'custom_user_category_id' => ['type' => 'index', 'columns' => ['custom_user_category_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'custom_users_to_custom_categories_ibfk_2' => ['type' => 'foreign', 'columns' => ['custom_user_category_id'], 'references' => ['custom_user_categories', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'custom_users_to_custom_categories_ibfk_1' => ['type' => 'foreign', 'columns' => ['custom_user_id'], 'references' => ['custom_users', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_general_ci'
        ],
    ];
    // phpcs:enable

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'custom_user_id' => 1,
                'custom_user_category_id' => 1,
                'modified' => '2020-09-26 10:24:20',
            ],
        ];
        parent::init();
    }
}
