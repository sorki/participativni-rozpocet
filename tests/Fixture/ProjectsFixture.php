<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProjectsFixture
 */
class ProjectsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'appeal_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'title_image_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'proposed_by_contact_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'project_status_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_general_ci', 'comment' => '', 'precision' => null],
        'description' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_general_ci', 'comment' => '', 'precision' => null],
        'public_interest' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8mb4_general_ci', 'comment' => 'veřejný přínos projektu', 'precision' => null],
        'gps_longitude' => ['type' => 'decimal', 'length' => 9, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'gps_latitude' => ['type' => 'decimal', 'length' => 11, 'precision' => 8, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'address' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_general_ci', 'comment' => 'adresa místa realizace pro geolokaci (napr. geocoding api)', 'precision' => null],
        'progress' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => 'procentni progress projektu 1-100', 'precision' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'created' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'appeal_id' => ['type' => 'index', 'columns' => ['appeal_id'], 'length' => []],
            'title_image_id' => ['type' => 'index', 'columns' => ['title_image_id'], 'length' => []],
            'progress' => ['type' => 'index', 'columns' => ['progress'], 'length' => []],
            'proposed_by_contact_id' => ['type' => 'index', 'columns' => ['proposed_by_contact_id'], 'length' => []],
            'project_status_id' => ['type' => 'index', 'columns' => ['project_status_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'projects_ibfk_4' => ['type' => 'foreign', 'columns' => ['project_status_id'], 'references' => ['project_status', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'projects_ibfk_3' => ['type' => 'foreign', 'columns' => ['proposed_by_contact_id'], 'references' => ['contacts', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'projects_ibfk_2' => ['type' => 'foreign', 'columns' => ['title_image_id'], 'references' => ['attachments', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'projects_ibfk_1' => ['type' => 'foreign', 'columns' => ['appeal_id'], 'references' => ['appeals', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_general_ci'
        ],
    ];
    // phpcs:enable

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'appeal_id' => 1,
                'title_image_id' => 1,
                'proposed_by_contact_id' => 1,
                'project_status_id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'public_interest' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'gps_longitude' => 1.5,
                'gps_latitude' => 1.5,
                'address' => 'Lorem ipsum dolor sit amet',
                'progress' => 1,
                'modified' => '2020-09-26 10:24:25',
                'created' => '2020-09-26 10:24:25',
            ],
        ];
        parent::init();
    }
}
