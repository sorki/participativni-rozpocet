<?php

namespace App\View\Helper;

class FormHelper extends \Cake\View\Helper\FormHelper
{
    public function control($fieldName, array $options = []): string
    {
        $class = isset($options['class']) ? $options['class'] : '';
        if ($this->isFieldError($fieldName)) {
            $class .= ' is-invalid';
        }

        if ((isset($options['type']) && $options['type'] === 'checkbox') || isset($options['checked']) || (isset($options['value']) && is_bool($options['value']))) {
            $options['templateVars']['customType'] = 'form-check';
            $options['nestedInput'] = false;
        }

        if ((isset($options['type']) && $options['type'] === 'select') || (isset($options['options']) && is_iterable($options['options']))) {
            $class .= ' select2';
        }

        if (isset($options['type']) && $options['type'] === 'file') {
            if (!isset($options['templates']) || !is_array($options['templates'])) {
                $options['templates'] = [];
            }
            $options['templates']['formgroup'] = '{{label}}{{input}}';
            $options['templates']['label'] = '<label class="custom-file-label" {{attrs}}>{{text}}{{tooltip}}</label>';
            $options['templateVars']['customType'] = 'custom-file mb-2';
        }

        $options['templateVars']['class'] = $class;
        return parent::control($fieldName, $options);
    }

    public function submit(?string $caption = null, array $options = []): string
    {
        if (!isset($options['class']) || empty($options['class'])) {
            $options['class'] = 'btn btn-success';
        }
        return parent::submit($caption, $options);
    }
}
