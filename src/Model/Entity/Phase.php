<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Phase Entity
 *
 * @property int $id
 * @property int $appeal_id
 * @property bool $allows_proposals_submission
 * @property bool $allows_voting
 * @property FrozenDate $date_start
 * @property FrozenDate $date_end
 * @property int|null $minimum_votes_count
 * @property string $name
 * @property string|null $description
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Appeal $appeal
 * @property PhaseToProvider[] $phase_to_providers
 * @property Vote[] $votes
 * @property IdentityProvider[] $identity_providers
 */
class Phase extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'appeal_id' => true,
        'allows_proposals_submission' => true,
        'allows_voting' => true,
        'date_start' => true,
        'date_end' => true,
        'minimum_votes_count' => true,
        'name' => true,
        'description' => true,
        'modified' => true,
        'created' => true,
        'appeal' => true,
        'phase_to_providers' => true,
        'votes' => true,
        'identity_providers' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_APPEAL_ID = 'appeal_id';
    public const FIELD_ALLOWS_PROPOSALS_SUBMISSION = 'allows_proposals_submission';
    public const FIELD_DATE_START = 'date_start';
    public const FIELD_DATE_END = 'date_end';
    public const FIELD_MINIMUM_VOTES_COUNT = 'minimum_votes_count';
    public const FIELD_NAME = 'name';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_APPEAL = 'appeal';
    public const FIELD_PHASE_TO_PROVIDERS = 'phase_to_providers';
    public const FIELD_VOTES = 'votes';
    public const FIELD_IDENTITY_PROVIDERS = 'identity_providers';
    public const FIELD_ALLOWS_VOTING = 'allows_voting';

    public function isCurrentlyActive(): bool
    {
        if (empty($this->date_start) || empty($this->date_end)) {
            return false;
        }
        $now = FrozenDate::now();
        return $this->date_start->lessThanOrEquals($now) && $this->date_end->greaterThanOrEquals($now);
    }
}
