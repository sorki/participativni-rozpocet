<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Organization Entity
 *
 * @property int $id
 * @property int $admin_user_id
 * @property string $name
 * @property bool $is_enabled
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property User $user
 * @property Appeal[] $appeals
 * @property CustomUserCategory[] $custom_user_categories
 * @property CustomUser[] $custom_users
 * @property Domain[] $domains
 * @property OrganizationSetting[] $organization_settings
 * @property OrganizationsToUser[] $organizations_to_users
 * @property ProjectCategory[] $project_categories
 * @property ProjectStatus[] $project_status
 */
class Organization extends Entity
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'admin_user_id' => true,
        'name' => true,
        'is_enabled' => true,
        'modified' => true,
        'created' => true,
        'user' => true,
        'appeals' => true,
        'custom_user_categories' => true,
        'custom_users' => true,
        'domains' => true,
        'organization_settings' => true,
        'organizations_to_users' => true,
        'project_categories' => true,
        'project_status' => true,
    ];

    public function loadAppeals(): self
    {
        if (!is_array($this->appeals)) {
            $organizationsTable = $this->getTableLocator()->get('Organizations');
            $organizationsTable->loadInto($this, ['Appeals']);
        }
        return $this;
    }

    public function loadDomains(): self
    {
        if (!is_array($this->domains)) {
            $organizationsTable = $this->getTableLocator()->get('Organizations');
            $organizationsTable->loadInto($this, ['Domains']);
        }
        return $this;
    }

    public const FIELD_ID = 'id';
    public const FIELD_ADMIN_USER_ID = 'admin_user_id';
    public const FIELD_NAME = 'name';
    public const FIELD_IS_ENABLED = 'is_enabled';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_USER = 'user';
    public const FIELD_APPEALS = 'appeals';
    public const FIELD_CUSTOM_USER_CATEGORIES = 'custom_user_categories';
    public const FIELD_CUSTOM_USERS = 'custom_users';
    public const FIELD_DOMAINS = 'domains';
    public const FIELD_ORGANIZATION_SETTINGS = 'organization_settings';
    public const FIELD_ORGANIZATIONS_TO_USERS = 'organizations_to_users';
    public const FIELD_PROJECT_CATEGORIES = 'project_categories';
    public const FIELD_PROJECT_STATUS = 'project_status';

    public function isDomainAuthorized(string $domain): bool
    {
        if (empty($this->domains) || empty($domain)) {
            return false;
        }
        foreach ($this->domains as $configured_domain) {
            if (strcmp(mb_strtolower($domain), mb_strtolower($configured_domain)) === 0) {
                return true;
            }
        }
        return false;
    }
}
