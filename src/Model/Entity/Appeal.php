<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Appeal Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string|null $info_link
 * @property string $year
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Organization $organization
 * @property Phase[] $phases
 * @property Project[] $projects
 */
class Appeal extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'info_link' => true,
        'year' => true,
        'modified' => true,
        'created' => true,
        'organization' => true,
        'phases' => true,
        'projects' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_ORGANIZATION_ID = 'organization_id';
    public const FIELD_INFO_LINK = 'info_link';
    public const FIELD_YEAR = 'year';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_ORGANIZATION = 'organization';
    public const FIELD_PHASES = 'phases';
    public const FIELD_PROJECTS = 'projects';

    /**
     * @return Phase[]
     */
    public function getCurrentPhases(): iterable
    {
        $phases = [];
        foreach ($this->phases ?? [] as $phase) {
            if ($phase->isCurrentlyActive()) {
                $phases[$phase->id] = $phase;
            }
        }
        return $phases;
    }
}
