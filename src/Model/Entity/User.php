<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Authentication\IdentityInterface;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $verification_code
 * @property bool $is_enabled
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property OrganizationsToUser[] $organizations_to_users
 * @property Organization[] $organizations
 * @property Organization[] $shared_organizations
 */
class User extends Entity implements IdentityInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'password' => true,
        'verification_code' => true,
        'is_enabled' => true,
        'modified' => true,
        'created' => true,
        'organizations_to_users' => true,
        'organizations' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    public function getIdentifier()
    {
        return $this->id;
    }

    public function getOriginalData()
    {
        return $this;
    }

    public function setPassword(string $plaintext)
    {
        $this->{self::FIELD_PASSWORD} = password_hash($plaintext, PASSWORD_ARGON2ID);
    }

    public function isManagerToOrganization(int $organization_id): bool
    {
        foreach ($this->organizations ?? [] as $organization) {
            if ($organization->id === $organization_id) {
                return true;
            }
        }
        foreach ($this->shared_organizations ?? [] as $organization) {
            if ($organization->id === $organization_id) {
                return true;
            }
        }
        return false;
    }

    public const FIELD_ID = 'id';
    public const FIELD_EMAIL = 'email';
    public const FIELD_PASSWORD = 'password';
    public const FIELD_VERIFICATION_CODE = 'verification_code';
    public const FIELD_IS_ENABLED = 'is_enabled';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_ORGANIZATIONS_TO_USERS = 'organizations_to_users';
}
