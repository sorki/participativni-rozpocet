<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * CustomUsersToCustomCategory Entity
 *
 * @property int $id
 * @property int $custom_user_id
 * @property int $custom_user_category_id
 * @property FrozenTime|null $modified
 *
 * @property CustomUser $custom_user
 * @property CustomUserCategory $custom_user_category
 */
class CustomUsersToCustomCategory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'custom_user_id' => true,
        'custom_user_category_id' => true,
        'modified' => true,
        'custom_user' => true,
        'custom_user_category' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_CUSTOM_USER_ID = 'custom_user_id';
    public const FIELD_CUSTOM_USER_CATEGORY_ID = 'custom_user_category_id';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CUSTOM_USER = 'custom_user';
    public const FIELD_CUSTOM_USER_CATEGORY = 'custom_user_category';
}
