<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * ProjectLog Entity
 *
 * @property int $id
 * @property int $project_id
 * @property int $project_log_type_id
 * @property FrozenDate $date_when
 * @property string $title
 * @property string|null $description
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Project $project
 * @property ProjectLogType $project_log_type
 */
class ProjectLog extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'project_id' => true,
        'project_log_type_id' => true,
        'date_when' => true,
        'title' => true,
        'description' => true,
        'modified' => true,
        'created' => true,
        'project' => true,
        'project_log_type' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_PROJECT_ID = 'project_id';
    public const FIELD_PROJECT_LOG_TYPE_ID = 'project_log_type_id';
    public const FIELD_DATE_WHEN = 'date_when';
    public const FIELD_TITLE = 'title';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_PROJECT = 'project';
    public const FIELD_PROJECT_LOG_TYPE = 'project_log_type';
}
