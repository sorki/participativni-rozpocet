<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * ProjectCategory Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property FrozenTime|null $modified
 *
 * @property Organization $organization
 */
class ProjectCategory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'name' => true,
        'modified' => true,
        'organization' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_ORGANIZATION_ID = 'organization_id';
    public const FIELD_NAME = 'name';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_ORGANIZATION = 'organization';
}
