<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrganizationsToUser Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property int $user_id
 * @property int $user_role_id
 *
 * @property Organization $organization
 * @property User $user
 * @property UserRole $user_role
 */
class OrganizationsToUser extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'user_id' => true,
        'user_role_id' => true,
        'organization' => true,
        'user' => true,
        'user_role' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_ORGANIZATION_ID = 'organization_id';
    public const FIELD_USER_ID = 'user_id';
    public const FIELD_USER_ROLE_ID = 'user_role_id';
    public const FIELD_ORGANIZATION = 'organization';
    public const FIELD_USER = 'user';
    public const FIELD_USER_ROLE = 'user_role';
}
