<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Project Entity
 *
 * @property int $id
 * @property int $appeal_id
 * @property int|null $title_image_id
 * @property int $proposed_by_contact_id
 * @property int $project_status_id
 * @property string $name
 * @property string|null $description
 * @property string|null $public_interest
 * @property string|null $gps_longitude
 * @property string|null $gps_latitude
 * @property string $address
 * @property int $progress
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Appeal $appeal
 * @property Attachment[] $attachments
 * @property Contact $contact
 * @property ProjectStatus $project_status
 * @property ProjectBudgetItem[] $project_budget_items
 * @property ProjectLog[] $project_logs
 * @property ProjectsToCategory[] $projects_to_categories
 * @property Vote[] $votes
 * @property ProjectCategory[] $project_categories
 */
class Project extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'appeal_id' => true,
        'title_image_id' => true,
        'proposed_by_contact_id' => true,
        'project_status_id' => true,
        'name' => true,
        'description' => true,
        'public_interest' => true,
        'gps_longitude' => true,
        'gps_latitude' => true,
        'address' => true,
        'progress' => true,
        'modified' => true,
        'created' => true,
        'appeal' => true,
        'attachments' => true,
        'contact' => true,
        'project_status' => true,
        'project_budget_items' => true,
        'project_logs' => true,
        'projects_to_categories' => true,
        'votes' => true,
        'project_categories' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_APPEAL_ID = 'appeal_id';
    public const FIELD_TITLE_IMAGE_ID = 'title_image_id';
    public const FIELD_PROPOSED_BY_CONTACT_ID = 'proposed_by_contact_id';
    public const FIELD_PROJECT_STATUS_ID = 'project_status_id';
    public const FIELD_NAME = 'name';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_PUBLIC_INTEREST = 'public_interest';
    public const FIELD_GPS_LONGITUDE = 'gps_longitude';
    public const FIELD_GPS_LATITUDE = 'gps_latitude';
    public const FIELD_ADDRESS = 'address';
    public const FIELD_PROGRESS = 'progress';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_APPEAL = 'appeal';
    public const FIELD_ATTACHMENTS = 'attachments';
    public const FIELD_CONTACT = 'contact';
    public const FIELD_PROJECT_STATUS = 'project_status';
    public const FIELD_PROJECT_BUDGET_ITEMS = 'project_budget_items';
    public const FIELD_PROJECT_LOGS = 'project_logs';
    public const FIELD_PROJECTS_TO_CATEGORIES = 'projects_to_categories';
    public const FIELD_VOTES = 'votes';
    public const FIELD_PROJECT_CATEGORIES = 'project_categories';

    /**
     * @param int $logType
     * @return ProjectLog[]
     */
    public function getLogsByType(int $logType): iterable
    {
        $logs = [];
        foreach ($this->project_logs as $log) {
            if ($log->project_log_type_id === $logType) {
                $logs[$log->id] = $log;
            }
        }
        return $logs;
    }

    public function getAttachmentById(int $attachment_id): ?Attachment
    {
        foreach ($this->attachments ?? [] as $attachment) {
            if ($attachment->id === $attachment_id) {
                return $attachment;
            }
        }
        return null;
    }
}
