<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * ProjectBudgetItem Entity
 *
 * @property int $id
 * @property int $project_id
 * @property bool $is_final
 * @property int $total_price
 * @property int $item_price
 * @property int $item_count
 * @property string $description
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Project $project
 */
class ProjectBudgetItem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'project_id' => true,
        'is_final' => true,
        'total_price' => true,
        'item_price' => true,
        'item_count' => true,
        'description' => true,
        'modified' => true,
        'created' => true,
        'project' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_PROJECT_ID = 'project_id';
    public const FIELD_IS_FINAL = 'is_final';
    public const FIELD_TOTAL_PRICE = 'total_price';
    public const FIELD_ITEM_PRICE = 'item_price';
    public const FIELD_ITEM_COUNT = 'item_count';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_PROJECT = 'project';
}
