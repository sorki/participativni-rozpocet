<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Vote Entity
 *
 * @property int $id
 * @property int $identity_provider_id
 * @property int $phase_id
 * @property int $project_id
 * @property string $identity_data
 * @property string $identity_key
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property IdentityProvider $identity_provider
 * @property Phase $phase
 * @property Project $project
 */
class Vote extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'identity_provider_id' => true,
        'phase_id' => true,
        'project_id' => true,
        'identity_data' => true,
        'identity_key' => true,
        'modified' => true,
        'created' => true,
        'identity_provider' => true,
        'phase' => true,
        'project' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_IDENTITY_PROVIDER_ID = 'identity_provider_id';
    public const FIELD_PHASE_ID = 'phase_id';
    public const FIELD_PROJECT_ID = 'project_id';
    public const FIELD_IDENTITY_DATA = 'identity_data';
    public const FIELD_IDENTITY_KEY = 'identity_key';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_IDENTITY_PROVIDER = 'identity_provider';
    public const FIELD_PHASE = 'phase';
    public const FIELD_PROJECT = 'project';
}
