<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * ProjectsToCategory Entity
 *
 * @property int $id
 * @property int $project_id
 * @property int $category_id
 * @property FrozenTime|null $modified
 *
 * @property Project $project
 * @property ProjectCategory $category
 */
class ProjectsToCategory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'project_id' => true,
        'category_id' => true,
        'modified' => true,
        'project' => true,
        'project_category' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_PROJECT_ID = 'project_id';
    public const FIELD_CATEGORY_ID = 'category_id';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_PROJECT = 'project';
    public const FIELD_PROJECT_CATEGORY = 'project_category';
}
