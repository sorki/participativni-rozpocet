<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * PhaseToProvider Entity
 *
 * @property int $id
 * @property int $phase_id
 * @property int $identity_provider_id
 * @property FrozenTime|null $modified
 *
 * @property Phase $phase
 * @property IdentityProvider $identity_provider
 */
class PhaseToProvider extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'phase_id' => true,
        'identity_provider_id' => true,
        'modified' => true,
        'phase' => true,
        'identity_provider' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_PHASE_ID = 'phase_id';
    public const FIELD_IDENTITY_PROVIDER_ID = 'identity_provider_id';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_PHASE = 'phase';
    public const FIELD_IDENTITY_PROVIDER = 'identity_provider';
}
