<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CustomUser Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property int $contact_id
 * @property string $username
 * @property string $password
 *
 * @property Organization $organization
 * @property Contact $contact
 * @property CustomUsersToCustomCategory[] $custom_users_to_custom_categories
 */
class CustomUser extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'contact_id' => true,
        'username' => true,
        'password' => true,
        'organization' => true,
        'contact' => true,
        'custom_users_to_custom_categories' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    public const FIELD_ID = 'id';
    public const FIELD_ORGANIZATION_ID = 'organization_id';
    public const FIELD_CONTACT_ID = 'contact_id';
    public const FIELD_USERNAME = 'username';
    public const FIELD_PASSWORD = 'password';
    public const FIELD_ORGANIZATION = 'organization';
    public const FIELD_CONTACT = 'contact';
    public const FIELD_CUSTOM_USERS_TO_CUSTOM_CATEGORIES = 'custom_users_to_custom_categories';
}
