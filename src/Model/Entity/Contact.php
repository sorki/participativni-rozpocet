<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * Contact Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string|null $phone
 * @property int|null $identity_provider_id
 * @property string|null $identity_key
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property IdentityProvider $identity_provider
 * @property CustomUser[] $custom_users
 */
class Contact extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'email' => true,
        'phone' => true,
        'identity_provider_id' => true,
        'identity_key' => true,
        'modified' => true,
        'created' => true,
        'identity_provider' => true,
        'custom_users' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_EMAIL = 'email';
    public const FIELD_PHONE = 'phone';
    public const FIELD_IDENTITY_PROVIDER_ID = 'identity_provider_id';
    public const FIELD_IDENTITY_KEY = 'identity_key';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_IDENTITY_PROVIDER = 'identity_provider';
    public const FIELD_CUSTOM_USERS = 'custom_users';
}
