<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\ORM\Entity;
use finfo;
use InvalidArgumentException;
use Psr\Http\Message\UploadedFileInterface;
use Throwable;

/**
 * Attachment Entity
 *
 * @property int $id
 * @property int $project_id
 * @property int $attachment_type_id
 * @property int $filesize
 * @property string $original_filename
 * @property string $title
 * @property string $filepath
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Project $project
 * @property AttachmentType $attachment_type
 */
class Attachment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'project_id' => true,
        'attachment_type_id' => true,
        'modified' => true,
        'created' => true,
        'project' => true,
        'attachment_type' => true,
        'filesize' => true,
        'original_filename' => true,
        'title' => true,
        'filepath' => true
    ];

    public const FIELD_ID = 'id';
    public const FIELD_PROJECT_ID = 'project_id';
    public const FIELD_ATTACHMENT_TYPE_ID = 'attachment_type_id';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_PROJECT = 'project';
    public const FIELD_FILESIZE = 'filesize';
    public const FIELD_ORIGINAL_FILENAME = 'original_filename';
    public const FIELD_TITLE = 'title';
    public const FIELD_FILEPATH = 'filepath';
    public const FIELD_ATTACHMENT_TYPE = 'attachment_type';

    /**
     * Moves file into /var/www/html/files/$organization_id/$project_id/`year`/`month`/`timestamp`
     *
     * @param UploadedFileInterface|null $filedata filedata as provided in Request->getData()
     * @param int $organization_id id of owning organization
     * @param int $project_id id of project attachment belongs to
     * @return bool|string false if operation failed, filepath relative to folder files otherwise
     */
    public function fileUploadMove(?UploadedFileInterface $filedata, int $organization_id, int $project_id)
    {
        if (empty($filedata) || $filedata->getError() !== UPLOAD_ERR_OK || $filedata->getSize() > getMaximumFileUploadSize()) {
            throw new InvalidArgumentException();
        }
        $year = date('Y');
        $month = date('n');
        $timestamp = time();
        $targetDirectory = ROOT . DS . 'files' . DS . $organization_id . DS . $project_id . DS . $year . DS . $month . DS;
        if (!file_exists($targetDirectory)) {
            mkdir($targetDirectory, 0770, true);
        }
        try {
            $filedata->moveTo($targetDirectory . $timestamp);
            return $organization_id . DS . $project_id . DS . $year . DS . $month . DS . $timestamp;
        } catch (Throwable $t) {
            Log::error($t->getMessage());
            Log::error($t->getTraceAsString());
        }
        return false;
    }

    /**
     * @return bool
     */
    public function deletePhysically(): bool
    {
        if (!empty($this->filepath)) {
            $filename = $this->getRealPath();
            if (is_file($filename)) {
                return unlink($filename);
            }
        }

        return false;
    }

    public function getRealPath(): string
    {
        return ROOT . DS . 'files' . DS . $this->filepath;
    }

    public function getMimeType(): string
    {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        return $finfo->file($this->getRealPath());
    }
}
