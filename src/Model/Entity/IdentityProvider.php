<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * IdentityProvider Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Contact[] $contacts
 * @property PhaseToProvider[] $phase_to_providers
 * @property Vote[] $votes
 */
class IdentityProvider extends Entity
{
    public const PROVIDER_UNIQUE_IP = 1,
        PROVIDER_NIA = 2,
        PROVIDER_CUSTOM_USERS = 3,
        PROVIDER_REGISTERED_USERS = 4,
        PROVIDER_PORTAL_MANAGERS = 5;

    public const PROVIDERS_SUITABLE_FOR_PROJECT_PROPOSALS = [
        self::PROVIDER_NIA,
        self::PROVIDER_CUSTOM_USERS,
        self::PROVIDER_REGISTERED_USERS,
        self::PROVIDER_PORTAL_MANAGERS,
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'modified' => true,
        'created' => true,
        'contacts' => true,
        'phase_to_providers' => true,
        'votes' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_CONTACTS = 'contacts';
    public const FIELD_PHASE_TO_PROVIDERS = 'phase_to_providers';
    public const FIELD_VOTES = 'votes';
}
