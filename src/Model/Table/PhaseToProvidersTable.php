<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\PhaseToProvider;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PhaseToProviders Model
 *
 * @property PhasesTable&BelongsTo $Phases
 * @property IdentityProvidersTable&BelongsTo $IdentityProviders
 *
 * @method PhaseToProvider newEmptyEntity()
 * @method PhaseToProvider newEntity(array $data, array $options = [])
 * @method PhaseToProvider[] newEntities(array $data, array $options = [])
 * @method PhaseToProvider get($primaryKey, $options = [])
 * @method PhaseToProvider findOrCreate($search, ?callable $callback = null, $options = [])
 * @method PhaseToProvider patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method PhaseToProvider[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method PhaseToProvider|false save(EntityInterface $entity, $options = [])
 * @method PhaseToProvider saveOrFail(EntityInterface $entity, $options = [])
 * @method PhaseToProvider[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method PhaseToProvider[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method PhaseToProvider[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method PhaseToProvider[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class PhaseToProvidersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('phase_to_providers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Phases', [
            'foreignKey' => 'phase_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('IdentityProviders', [
            'foreignKey' => 'identity_provider_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['phase_id'], 'Phases'), ['errorField' => 'phase_id']);
        $rules->add($rules->existsIn(['identity_provider_id'], 'IdentityProviders'), ['errorField' => 'identity_provider_id']);

        return $rules;
    }
}
