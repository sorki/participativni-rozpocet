<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ProjectLogType;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProjectLogTypes Model
 *
 * @property ProjectLogsTable&HasMany $ProjectLogs
 *
 * @method ProjectLogType newEmptyEntity()
 * @method ProjectLogType newEntity(array $data, array $options = [])
 * @method ProjectLogType[] newEntities(array $data, array $options = [])
 * @method ProjectLogType get($primaryKey, $options = [])
 * @method ProjectLogType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method ProjectLogType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ProjectLogType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method ProjectLogType|false save(EntityInterface $entity, $options = [])
 * @method ProjectLogType saveOrFail(EntityInterface $entity, $options = [])
 * @method ProjectLogType[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method ProjectLogType[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method ProjectLogType[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method ProjectLogType[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProjectLogTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('project_log_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('ProjectLogs', [
            'foreignKey' => 'project_log_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
