<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Appeal;
use App\Model\Entity\ProjectCategory;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProjectCategories Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 *
 * @method ProjectCategory newEmptyEntity()
 * @method ProjectCategory newEntity(array $data, array $options = [])
 * @method ProjectCategory[] newEntities(array $data, array $options = [])
 * @method ProjectCategory get($primaryKey, $options = [])
 * @method ProjectCategory findOrCreate($search, ?callable $callback = null, $options = [])
 * @method ProjectCategory patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ProjectCategory[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method ProjectCategory|false save(EntityInterface $entity, $options = [])
 * @method ProjectCategory saveOrFail(EntityInterface $entity, $options = [])
 * @method ProjectCategory[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method ProjectCategory[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method ProjectCategory[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method ProjectCategory[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProjectCategoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('project_categories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);

        return $rules;
    }

    public function getWithOrganization(int $category_id, int $organization_id, array $contain = [])
    {
        return $this->get($category_id, [
            'conditions' => [
                'ProjectCategories.' . ProjectCategory::FIELD_ORGANIZATION_ID => $organization_id
            ],
            'contain' => $contain
        ]);
    }
}
