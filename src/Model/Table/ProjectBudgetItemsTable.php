<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ProjectBudgetItem;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProjectBudgetItems Model
 *
 * @property ProjectsTable&BelongsTo $Projects
 *
 * @method ProjectBudgetItem newEmptyEntity()
 * @method ProjectBudgetItem newEntity(array $data, array $options = [])
 * @method ProjectBudgetItem[] newEntities(array $data, array $options = [])
 * @method ProjectBudgetItem get($primaryKey, $options = [])
 * @method ProjectBudgetItem findOrCreate($search, ?callable $callback = null, $options = [])
 * @method ProjectBudgetItem patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ProjectBudgetItem[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method ProjectBudgetItem|false save(EntityInterface $entity, $options = [])
 * @method ProjectBudgetItem saveOrFail(EntityInterface $entity, $options = [])
 * @method ProjectBudgetItem[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method ProjectBudgetItem[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method ProjectBudgetItem[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method ProjectBudgetItem[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProjectBudgetItemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('project_budget_items');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('is_final')
            ->requirePresence('is_final', 'create')
            ->notEmptyString('is_final');

        $validator
            ->integer('total_price')
            ->requirePresence('total_price', 'create')
            ->greaterThan('total_price', 0)
            ->notEmptyString('total_price');

        $validator
            ->integer('item_price')
            ->requirePresence('item_price', 'create')
            ->greaterThan('item_price', 0)
            ->notEmptyString('item_price');

        $validator
            ->integer('item_count')
            ->requirePresence('item_count', 'create')
            ->greaterThan('item_count', 0)
            ->notEmptyString('item_count');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['project_id'], 'Projects'), ['errorField' => 'project_id']);

        return $rules;
    }

    public function getWithProject(int $item_id, int $project_id, array $contain = [])
    {
        return $this->get($item_id, [
            'conditions' => [
                'ProjectBudgetItems.' . ProjectBudgetItem::FIELD_PROJECT_ID => $project_id
            ],
            'contain' => $contain
        ]);
    }
}
