<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\OrganizationsToUser;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrganizationsToUsers Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property UsersTable&BelongsTo $Users
 * @property UserRolesTable&BelongsTo $UserRoles
 *
 * @method OrganizationsToUser newEmptyEntity()
 * @method OrganizationsToUser newEntity(array $data, array $options = [])
 * @method OrganizationsToUser[] newEntities(array $data, array $options = [])
 * @method OrganizationsToUser get($primaryKey, $options = [])
 * @method OrganizationsToUser findOrCreate($search, ?callable $callback = null, $options = [])
 * @method OrganizationsToUser patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method OrganizationsToUser[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method OrganizationsToUser|false save(EntityInterface $entity, $options = [])
 * @method OrganizationsToUser saveOrFail(EntityInterface $entity, $options = [])
 * @method OrganizationsToUser[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method OrganizationsToUser[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method OrganizationsToUser[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method OrganizationsToUser[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OrganizationsToUsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('organizations_to_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('UserRoles', [
            'foreignKey' => 'user_role_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['user_role_id'], 'UserRoles'), ['errorField' => 'user_role_id']);

        return $rules;
    }
}
