<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Appeal;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Appeals Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property PhasesTable&HasMany $Phases
 * @property ProjectsTable&HasMany $Projects
 *
 * @method Appeal newEmptyEntity()
 * @method Appeal newEntity(array $data, array $options = [])
 * @method Appeal[] newEntities(array $data, array $options = [])
 * @method Appeal get($primaryKey, $options = [])
 * @method Appeal findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Appeal patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Appeal[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Appeal|false save(EntityInterface $entity, $options = [])
 * @method Appeal saveOrFail(EntityInterface $entity, $options = [])
 * @method Appeal[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Appeal[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Appeal[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Appeal[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class AppealsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('appeals');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Phases', [
            'foreignKey' => 'appeal_id',
        ]);
        $this->hasMany('Projects', [
            'foreignKey' => 'appeal_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('info_link')
            ->maxLength('info_link', 255)
            ->allowEmptyString('info_link');

        $validator
            ->scalar('year')
            ->requirePresence('year', 'create')
            ->notEmptyString('year');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);

        return $rules;
    }

    public function getWithOrganization(int $appeal_id, int $organization_id, array $contain = [])
    {
        return $this->get($appeal_id, [
            'conditions' => [
                'Appeals.' . Appeal::FIELD_ORGANIZATION_ID => $organization_id
            ],
            'contain' => $contain
        ]);
    }
}
