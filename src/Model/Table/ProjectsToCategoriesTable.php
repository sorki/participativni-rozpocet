<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ProjectsToCategory;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProjectsToCategories Model
 *
 * @property ProjectsTable&BelongsTo $Projects
 * @property ProjectCategoriesTable&BelongsTo $ProjectCategories
 *
 * @method ProjectsToCategory newEmptyEntity()
 * @method ProjectsToCategory newEntity(array $data, array $options = [])
 * @method ProjectsToCategory[] newEntities(array $data, array $options = [])
 * @method ProjectsToCategory get($primaryKey, $options = [])
 * @method ProjectsToCategory findOrCreate($search, ?callable $callback = null, $options = [])
 * @method ProjectsToCategory patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ProjectsToCategory[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method ProjectsToCategory|false save(EntityInterface $entity, $options = [])
 * @method ProjectsToCategory saveOrFail(EntityInterface $entity, $options = [])
 * @method ProjectsToCategory[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method ProjectsToCategory[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method ProjectsToCategory[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method ProjectsToCategory[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProjectsToCategoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('projects_to_categories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Categories', [
            'className' => 'ProjectCategories',
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['project_id'], 'Projects'), ['errorField' => 'project_id']);
        $rules->add($rules->existsIn(['category_id'], 'ProjectCategories'), ['errorField' => 'category_id']);

        return $rules;
    }
}
