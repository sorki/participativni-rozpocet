<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ProjectLog;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProjectLogs Model
 *
 * @property ProjectsTable&BelongsTo $Projects
 * @property ProjectLogTypesTable&BelongsTo $ProjectLogTypes
 *
 * @method ProjectLog newEmptyEntity()
 * @method ProjectLog newEntity(array $data, array $options = [])
 * @method ProjectLog[] newEntities(array $data, array $options = [])
 * @method ProjectLog get($primaryKey, $options = [])
 * @method ProjectLog findOrCreate($search, ?callable $callback = null, $options = [])
 * @method ProjectLog patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ProjectLog[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method ProjectLog|false save(EntityInterface $entity, $options = [])
 * @method ProjectLog saveOrFail(EntityInterface $entity, $options = [])
 * @method ProjectLog[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method ProjectLog[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method ProjectLog[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method ProjectLog[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProjectLogsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('project_logs');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ProjectLogTypes', [
            'foreignKey' => 'project_log_type_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('date_when')
            ->requirePresence('date_when', 'create')
            ->notEmptyDate('date_when');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['project_id'], 'Projects'), ['errorField' => 'project_id']);
        $rules->add($rules->existsIn(['project_log_type_id'], 'ProjectLogTypes'), ['errorField' => 'project_log_type_id']);

        return $rules;
    }

    public function getWithProject(int $log_id, int $project_id, array $contain = [])
    {
        return $this->get($log_id, [
            'conditions' => [
                'ProjectLogs.' . ProjectLog::FIELD_PROJECT_ID => $project_id
            ],
            'contain' => $contain
        ]);
    }
}
