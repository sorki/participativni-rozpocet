<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ProjectCategory;
use App\Model\Entity\ProjectStatus;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProjectStatus Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property ProjectsTable&HasMany $Projects
 *
 * @method ProjectStatus newEmptyEntity()
 * @method ProjectStatus newEntity(array $data, array $options = [])
 * @method ProjectStatus[] newEntities(array $data, array $options = [])
 * @method ProjectStatus get($primaryKey, $options = [])
 * @method ProjectStatus findOrCreate($search, ?callable $callback = null, $options = [])
 * @method ProjectStatus patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ProjectStatus[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method ProjectStatus|false save(EntityInterface $entity, $options = [])
 * @method ProjectStatus saveOrFail(EntityInterface $entity, $options = [])
 * @method ProjectStatus[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method ProjectStatus[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method ProjectStatus[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method ProjectStatus[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProjectStatusTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('project_status');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Projects', [
            'foreignKey' => 'project_status_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->boolean('is_public')
            ->requirePresence('is_public', 'create')
            ->notEmptyString('is_public');

        $validator
            ->boolean('is_voting_enabled')
            ->requirePresence('is_voting_enabled', 'create')
            ->notEmptyString('is_voting_enabled');

        $validator
            ->boolean('is_default_for_proposals')
            ->requirePresence('is_default_for_proposals', 'create')
            ->notEmptyString('is_default_for_proposals');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);

        return $rules;
    }

    public function getWithOrganization(int $status_id, int $organization_id, array $contain = [])
    {
        return $this->get($status_id, [
            'conditions' => [
                'ProjectStatus.' . ProjectStatus::FIELD_ORGANIZATION_ID => $organization_id
            ],
            'contain' => $contain
        ]);
    }
}
