<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\OrganizationSetting;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrganizationSettings Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 *
 * @method OrganizationSetting newEmptyEntity()
 * @method OrganizationSetting newEntity(array $data, array $options = [])
 * @method OrganizationSetting[] newEntities(array $data, array $options = [])
 * @method OrganizationSetting get($primaryKey, $options = [])
 * @method OrganizationSetting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method OrganizationSetting patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method OrganizationSetting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method OrganizationSetting|false save(EntityInterface $entity, $options = [])
 * @method OrganizationSetting saveOrFail(EntityInterface $entity, $options = [])
 * @method OrganizationSetting[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method OrganizationSetting[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method OrganizationSetting[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method OrganizationSetting[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class OrganizationSettingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('organization_settings');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('value')
            ->allowEmptyString('value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);

        return $rules;
    }
}
