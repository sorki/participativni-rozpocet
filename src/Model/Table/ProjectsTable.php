<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Projects Model
 *
 * @property AppealsTable&BelongsTo $Appeals
 * @property AttachmentsTable&BelongsTo $TitleImageAttachment
 * @property ContactsTable&BelongsTo $Contacts
 * @property ProjectStatusTable&BelongsTo $ProjectStatus
 * @property AttachmentsTable&HasMany $Attachments
 * @property ProjectBudgetItemsTable&HasMany $ProjectBudgetItems
 * @property ProjectLogsTable&HasMany $ProjectLogs
 * @property ProjectsToCategoriesTable&HasMany $ProjectsToCategories
 * @property VotesTable&HasMany $Votes
 * @property ProjectCategoriesTable&BelongsToMany $ProjectCategories
 *
 * @method Project newEmptyEntity()
 * @method Project newEntity(array $data, array $options = [])
 * @method Project[] newEntities(array $data, array $options = [])
 * @method Project get($primaryKey, $options = [])
 * @method Project findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Project patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Project[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Project|false save(EntityInterface $entity, $options = [])
 * @method Project saveOrFail(EntityInterface $entity, $options = [])
 * @method Project[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Project[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Project[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Project[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ProjectsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('projects');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Appeals', [
            'foreignKey' => 'appeal_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('TitleImageAttachment', [
            'foreignKey' => 'title_image_id',
        ]);
        $this->belongsTo('Contacts', [
            'foreignKey' => 'proposed_by_contact_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ProjectStatus', [
            'foreignKey' => 'project_status_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Attachments', [
            'foreignKey' => 'project_id',
        ]);
        $this->hasMany('ProjectBudgetItems', [
            'foreignKey' => 'project_id',
        ]);
        $this->hasMany('ProjectLogs', [
            'foreignKey' => 'project_id',
        ]);
        $this->hasMany('ProjectsToCategories', [
            'foreignKey' => 'project_id',
        ]);
        $this->belongsToMany('ProjectCategories', [
            'through' => 'ProjectsToCategories',
            'targetForeignKey' => 'category_id'
        ]);
        $this->hasMany('Votes', [
            'foreignKey' => 'project_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('public_interest')
            ->allowEmptyString('public_interest');

        $validator
            ->decimal('gps_longitude')
            ->allowEmptyString('gps_longitude');

        $validator
            ->decimal('gps_latitude')
            ->allowEmptyString('gps_latitude');

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->requirePresence('address', 'create')
            ->notEmptyString('address');

        $validator
            ->integer('progress')
            ->notEmptyString('progress')
            ->greaterThanOrEqual('progress', 0)
            ->lessThanOrEqual('progress', 100);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['appeal_id'], 'Appeals'), ['errorField' => 'appeal_id']);
        $rules->add($rules->existsIn(['title_image_id'], 'Attachments'), ['errorField' => 'title_image_id']);
        $rules->add($rules->existsIn(['proposed_by_contact_id'], 'Contacts'), ['errorField' => 'proposed_by_contact_id']);
        $rules->add($rules->existsIn(['project_status_id'], 'ProjectStatus'), ['errorField' => 'project_status_id']);

        return $rules;
    }

    public function getWithAppeal(int $project_id, int $appeal_id, array $contain = [])
    {
        return $this->get($project_id, [
            'conditions' => [
                'Projects.' . Project::FIELD_APPEAL_ID => $appeal_id
            ],
            'contain' => $contain
        ]);
    }
}
