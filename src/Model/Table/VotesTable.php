<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Vote;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Votes Model
 *
 * @property IdentityProvidersTable&BelongsTo $IdentityProviders
 * @property PhasesTable&BelongsTo $Phases
 * @property ProjectsTable&BelongsTo $Projects
 *
 * @method Vote newEmptyEntity()
 * @method Vote newEntity(array $data, array $options = [])
 * @method Vote[] newEntities(array $data, array $options = [])
 * @method Vote get($primaryKey, $options = [])
 * @method Vote findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Vote patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Vote[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Vote|false save(EntityInterface $entity, $options = [])
 * @method Vote saveOrFail(EntityInterface $entity, $options = [])
 * @method Vote[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Vote[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Vote[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Vote[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class VotesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('votes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('IdentityProviders', [
            'foreignKey' => 'identity_provider_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Phases', [
            'foreignKey' => 'phase_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('identity_data')
            ->requirePresence('identity_data', 'create')
            ->notEmptyString('identity_data');

        $validator
            ->scalar('identity_key')
            ->maxLength('identity_key', 255)
            ->requirePresence('identity_key', 'create')
            ->notEmptyString('identity_key');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['identity_provider_id'], 'IdentityProviders'), ['errorField' => 'identity_provider_id']);
        $rules->add($rules->existsIn(['phase_id'], 'Phases'), ['errorField' => 'phase_id']);
        $rules->add($rules->existsIn(['project_id'], 'Projects'), ['errorField' => 'project_id']);

        return $rules;
    }
}
