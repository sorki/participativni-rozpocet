<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\CustomUser;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CustomUsers Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property ContactsTable&BelongsTo $Contacts
 * @property CustomUsersToCustomCategoriesTable&HasMany $CustomUsersToCustomCategories
 *
 * @method CustomUser newEmptyEntity()
 * @method CustomUser newEntity(array $data, array $options = [])
 * @method CustomUser[] newEntities(array $data, array $options = [])
 * @method CustomUser get($primaryKey, $options = [])
 * @method CustomUser findOrCreate($search, ?callable $callback = null, $options = [])
 * @method CustomUser patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method CustomUser[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method CustomUser|false save(EntityInterface $entity, $options = [])
 * @method CustomUser saveOrFail(EntityInterface $entity, $options = [])
 * @method CustomUser[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method CustomUser[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method CustomUser[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method CustomUser[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CustomUsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('custom_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Contacts', [
            'foreignKey' => 'contact_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('CustomUsersToCustomCategories', [
            'foreignKey' => 'custom_user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmptyString('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 128)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);
        $rules->add($rules->existsIn(['contact_id'], 'Contacts'), ['errorField' => 'contact_id']);

        return $rules;
    }
}
