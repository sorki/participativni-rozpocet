<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\IdentityProvider;
use App\Model\Entity\Phase;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Phases Model
 *
 * @property AppealsTable&BelongsTo $Appeals
 * @property PhaseToProvidersTable&HasMany $PhaseToProviders
 * @property VotesTable&HasMany $Votes
 * @property IdentityProvidersTable&BelongsToMany $IdentityProviders
 *
 * @method Phase newEmptyEntity()
 * @method Phase newEntity(array $data, array $options = [])
 * @method Phase[] newEntities(array $data, array $options = [])
 * @method Phase get($primaryKey, $options = [])
 * @method Phase findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Phase patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Phase[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Phase|false save(EntityInterface $entity, $options = [])
 * @method Phase saveOrFail(EntityInterface $entity, $options = [])
 * @method Phase[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Phase[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Phase[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Phase[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class PhasesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('phases');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Appeals', [
            'foreignKey' => 'appeal_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('PhaseToProviders', [
            'foreignKey' => 'phase_id',
        ]);
        $this->belongsToMany('IdentityProviders', [
            'through' => 'PhaseToProviders'
        ]);
        $this->hasMany('Votes', [
            'foreignKey' => 'phase_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('allows_proposals_submission')
            ->notEmptyString('allows_proposals_submission');

        $validator
            ->date('date_start')
            ->requirePresence('date_start', 'create')
            ->notEmptyDate('date_start');

        $validator
            ->date('date_end')
            ->requirePresence('date_end', 'create')
            ->notEmptyDate('date_end');

        $validator
            ->integer('minimum_votes_count')
            ->allowEmptyString('minimum_votes_count');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['appeal_id'], 'Appeals'), ['errorField' => 'appeal_id']);
        $rules->add(function (Phase $entity) {
            if (empty($entity->date_start)) {
                return false;
            }
            if (empty($entity->date_end)) {
                return false;
            }
            if ($entity->date_start->greaterThan($entity->date_end)) {
                return __('Datum začátku musí být menší nebo stejné jako datum konce fáze');
            }
            return true;
        }, 'startDate', ['errorField' => Phase::FIELD_DATE_START, 'message' => __('Datum začátku není platné')]);
        $rules->add(function (Phase $entity) {
            if ($entity->allows_proposals_submission === false && $entity->allows_voting === false) {
                return true;
            }
            if (empty($entity->identity_providers)) {
                return __('Pokud je povoleno hlasování nebo návrh projektů, musí být vybrán alespoň 1 způsob ověření uživatelů');
            }
            if ($entity->allows_proposals_submission) {
                $hasOnlyProvidersSuitableForProposals = true;
                $unsuitableProviders = [];
                foreach ($entity->identity_providers as $identity_provider) {
                    if (!in_array($identity_provider->id, IdentityProvider::PROVIDERS_SUITABLE_FOR_PROJECT_PROPOSALS, true)) {
                        $hasOnlyProvidersSuitableForProposals = false;
                        $unsuitableProviders[] = $identity_provider->name;
                    }
                }
                if (!$hasOnlyProvidersSuitableForProposals) {
                    return sprintf("%s: %s", __('Jeden nebo více způsobů ověření není možné použít pro návrh projektů, konkrétně'), join(', ', $unsuitableProviders));
                }
            }
            return true;
        }, 'hasProvidersWhenRequired', ['errorField' => Phase::FIELD_ALLOWS_PROPOSALS_SUBMISSION, 'message' => __('Nelze povolit navrhování projektů')]);

        return $rules;
    }

    public function getWithAppeal(int $phase_id, int $appeal_id, array $contain = [])
    {
        return $this->get($phase_id, [
            'conditions' => [
                'Phases.' . Phase::FIELD_APPEAL_ID => $appeal_id
            ],
            'contain' => $contain
        ]);
    }
}
