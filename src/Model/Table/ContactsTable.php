<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Contact;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contacts Model
 *
 * @property IdentityProvidersTable&BelongsTo $IdentityProviders
 * @property CustomUsersTable&HasMany $CustomUsers
 *
 * @method Contact newEmptyEntity()
 * @method Contact newEntity(array $data, array $options = [])
 * @method Contact[] newEntities(array $data, array $options = [])
 * @method Contact get($primaryKey, $options = [])
 * @method Contact findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Contact patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Contact[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Contact|false save(EntityInterface $entity, $options = [])
 * @method Contact saveOrFail(EntityInterface $entity, $options = [])
 * @method Contact[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Contact[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Contact[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Contact[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class ContactsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('contacts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('IdentityProviders', [
            'foreignKey' => 'identity_provider_id',
        ]);
        $this->hasMany('CustomUsers', [
            'foreignKey' => 'contact_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 20)
            ->allowEmptyString('phone');

        $validator
            ->scalar('identity_key')
            ->maxLength('identity_key', 255)
            ->allowEmptyString('identity_key');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['identity_provider_id'], 'IdentityProviders'), ['errorField' => 'identity_provider_id']);

        return $rules;
    }
}
