<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Organization;
use Cake\Database\Query;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Organizations Model
 *
 * @property UsersTable&BelongsTo $Users
 * @property AppealsTable&HasMany $Appeals
 * @property CustomUserCategoriesTable&HasMany $CustomUserCategories
 * @property CustomUsersTable&HasMany $CustomUsers
 * @property DomainsTable&HasMany $Domains
 * @property OrganizationSettingsTable&HasMany $OrganizationSettings
 * @property OrganizationsToUsersTable&HasMany $OrganizationsToUsers
 * @property ProjectCategoriesTable&HasMany $ProjectCategories
 * @property ProjectStatusTable&HasMany $ProjectStatus
 *
 * @method Organization newEmptyEntity()
 * @method Organization newEntity(array $data, array $options = [])
 * @method Organization[] newEntities(array $data, array $options = [])
 * @method Organization get($primaryKey, $options = [])
 * @method Organization findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Organization patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Organization[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Organization|false save(EntityInterface $entity, $options = [])
 * @method Organization saveOrFail(EntityInterface $entity, $options = [])
 * @method Organization[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Organization[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Organization[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Organization[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class OrganizationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('organizations');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'admin_user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Appeals', [
            'foreignKey' => 'organization_id',
        ]);
        $this->hasMany('CustomUserCategories', [
            'foreignKey' => 'organization_id',
        ]);
        $this->hasMany('CustomUsers', [
            'foreignKey' => 'organization_id',
        ]);
        $this->hasMany('Domains', [
            'foreignKey' => 'organization_id',
        ]);
        $this->hasMany('OrganizationSettings', [
            'foreignKey' => 'organization_id',
        ]);
        $this->hasMany('OrganizationsToUsers', [
            'foreignKey' => 'organization_id',
        ]);
        $this->hasMany('ProjectCategories', [
            'foreignKey' => 'organization_id',
        ]);
        $this->hasMany('ProjectStatus', [
            'foreignKey' => 'organization_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn([Organization::FIELD_ADMIN_USER_ID], 'Users'), ['errorField' => Organization::FIELD_ADMIN_USER_ID]);

        return $rules;
    }

    /**
     * This method will fail hard, throwing \Cake\Datasource\Exception\RecordNotFoundException
     * if the organization by ID does not exist or if it does not belong to the specified user
     *
     * Used as protection call in most of the places
     *
     * @param int $organization_id
     * @param int $user_id
     * @param array $contain
     * @return Organization
     */
    public function getOrganizationWithUser(int $organization_id, int $user_id, array $contain = [])
    {
        /** @var Organization $org */
        $org = $this->find('all', [
            'conditions' => [
                'Organizations.' . Organization::FIELD_ID => $organization_id
            ],
            'contain' => array_merge(['OrganizationsToUsers'], $contain)
        ])->firstOrFail();
        if ($org->admin_user_id === $user_id) {
            return $org;
        }
        foreach ($org->organizations_to_users as $share_link) {
            if ($share_link->user_id === $user_id) {
                return $org;
            }
        }
        throw new RecordNotFoundException();
    }

    public function findAllWithUser(int $currentUserId): Query
    {
        return $this->find('all')->innerJoinWith('OrganizationsToUsers')->where([
            'OR' => [
                'Organizations.admin_user_id' => $currentUserId,
                'OrganizationsToUsers.user_id' => $currentUserId
            ]
        ])->distinct('Organizations.id');
    }
}
