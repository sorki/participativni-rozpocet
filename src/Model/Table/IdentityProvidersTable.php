<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\IdentityProvider;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * IdentityProviders Model
 *
 * @property ContactsTable&HasMany $Contacts
 * @property PhaseToProvidersTable&HasMany $PhaseToProviders
 * @property VotesTable&HasMany $Votes
 *
 * @method IdentityProvider newEmptyEntity()
 * @method IdentityProvider newEntity(array $data, array $options = [])
 * @method IdentityProvider[] newEntities(array $data, array $options = [])
 * @method IdentityProvider get($primaryKey, $options = [])
 * @method IdentityProvider findOrCreate($search, ?callable $callback = null, $options = [])
 * @method IdentityProvider patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method IdentityProvider[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method IdentityProvider|false save(EntityInterface $entity, $options = [])
 * @method IdentityProvider saveOrFail(EntityInterface $entity, $options = [])
 * @method IdentityProvider[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method IdentityProvider[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method IdentityProvider[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method IdentityProvider[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class IdentityProvidersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('identity_providers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Contacts', [
            'foreignKey' => 'identity_provider_id',
        ]);
        $this->hasMany('PhaseToProviders', [
            'foreignKey' => 'identity_provider_id',
        ]);
        $this->hasMany('Votes', [
            'foreignKey' => 'identity_provider_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
