<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Domain;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Domains Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 *
 * @method Domain newEmptyEntity()
 * @method Domain newEntity(array $data, array $options = [])
 * @method Domain[] newEntities(array $data, array $options = [])
 * @method Domain get($primaryKey, $options = [])
 * @method Domain findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Domain patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Domain[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Domain|false save(EntityInterface $entity, $options = [])
 * @method Domain saveOrFail(EntityInterface $entity, $options = [])
 * @method Domain[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Domain[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Domain[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Domain[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class DomainsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('domains');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('domain')
            ->maxLength('domain', 255)
            ->requirePresence('domain', 'create')
            ->notEmptyString('domain')
            ->add('domain', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->boolean('is_enabled')
            ->notEmptyString('is_enabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['domain']), ['errorField' => 'domain']);
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);

        return $rules;
    }

    public function getWithOrganization(int $domain_id, int $organization_id, array $contain = [])
    {
        return $this->get($domain_id, [
            'conditions' => [
                'Domains.' . Domain::FIELD_ORGANIZATION_ID => $organization_id
            ],
            'contain' => $contain
        ]);
    }
}
