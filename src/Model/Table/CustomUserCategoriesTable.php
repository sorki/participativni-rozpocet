<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\CustomUserCategory;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CustomUserCategories Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 * @property CustomUsersToCustomCategoriesTable&HasMany $CustomUsersToCustomCategories
 *
 * @method CustomUserCategory newEmptyEntity()
 * @method CustomUserCategory newEntity(array $data, array $options = [])
 * @method CustomUserCategory[] newEntities(array $data, array $options = [])
 * @method CustomUserCategory get($primaryKey, $options = [])
 * @method CustomUserCategory findOrCreate($search, ?callable $callback = null, $options = [])
 * @method CustomUserCategory patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method CustomUserCategory[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method CustomUserCategory|false save(EntityInterface $entity, $options = [])
 * @method CustomUserCategory saveOrFail(EntityInterface $entity, $options = [])
 * @method CustomUserCategory[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method CustomUserCategory[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method CustomUserCategory[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method CustomUserCategory[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class CustomUserCategoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('custom_user_categories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('CustomUsersToCustomCategories', [
            'foreignKey' => 'custom_user_category_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);

        return $rules;
    }
}
