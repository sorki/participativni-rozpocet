<?php
declare(strict_types=1);

namespace App\IdentityProvider;


use App\Controller\Component\VotingComponent;
use App\Model\Entity\Organization;
use Cake\Http\Response;
use Cake\Http\ServerRequest;

abstract class AbstractIdentityProvider implements IdentityProviderInterface
{

    protected VotingComponent $_component;

    public function startup(VotingComponent $component)
    {
        $this->_component = $component;
    }

    /**
     * Dummy implementation, does not modify voting result
     *
     * @param VotingResult $initialResult
     * @param ServerRequest $request
     * @return VotingResult
     */
    public function processRequest(VotingResult $initialResult, ServerRequest $request): VotingResult
    {
        return $initialResult;
    }

    public function getInteractiveTemplatePath(): ?string
    {
        return null;
    }

    public function allowsNonInteractive(): bool
    {
        return false;
    }

    public function hasMetadata(): bool
    {
        return false;
    }

    public function renderMetadata(Organization $organization, ServerRequest $request, Response $response): Response
    {
        return $response;
    }
}
