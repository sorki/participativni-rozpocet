<?php
declare(strict_types=1);

namespace App\IdentityProvider;

use App\Controller\Component\VotingComponent;
use App\Model\Entity\Organization;
use Cake\Http\Response;
use Cake\Http\ServerRequest;

interface IdentityProviderInterface
{
    /**
     * Get self ID, which should be known in Model\Entity\IdentityProvider constants
     *
     * @return int
     */
    public function getProviderId(): int;

    /**
     * Inits the provider with VotingComponent
     *
     * @param VotingComponent $component
     * @return mixed
     */
    public function startup(VotingComponent $component);

    /**
     * Initial method of voting process, records vote if already possible,
     * or requests redirect
     *
     * @param VotingResult $initialResult initially filled from VotingComponent
     * @return VotingResult
     */
    public function vote(VotingResult $initialResult): VotingResult;

    /**
     * For identity providers, that need user interaction (either local or 3rd party), this callback allows
     * to process request (either interactive form submit or data returned from 3rd party site)
     *
     * @param VotingResult $initialResult
     * @param ServerRequest $request
     * @return VotingResult
     */
    public function processRequest(VotingResult $initialResult, ServerRequest $request): VotingResult;

    /**
     * If implementing identity provider provides interactive, return appropriate template path for rendering UI
     *
     * @return string|null
     */
    public function getInteractiveTemplatePath(): ?string;

    /**
     * Whether provider allows processing non-interactive callbacks from 3rd parties
     *
     * @return bool
     */
    public function allowsNonInteractive(): bool;

    /**
     * If provider has metadata, ie. SeP SAML2 metadata
     *
     * @return bool
     */
    public function hasMetadata(): bool;

    /**
     * Hands off the whole request/response to provider, to provide it's own metadata response
     *
     * @param Organization $organization
     * @param ServerRequest $request
     * @param Response $response
     * @return Response
     */
    public function renderMetadata(Organization $organization, ServerRequest $request, Response $response): Response;
}
