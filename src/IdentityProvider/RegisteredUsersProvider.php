<?php
declare(strict_types=1);

namespace App\IdentityProvider;

use App\Controller\Component\VotingComponent;
use App\Model\Entity\IdentityProvider;
use App\Model\Entity\User;
use Cake\Http\ServerRequest;
use Cake\ORM\Locator\LocatorAwareTrait;

class RegisteredUsersProvider extends AbstractIdentityProvider
{
    use LocatorAwareTrait;

    public const PERSIST_CURRENT_PHASE = 'voting.registered_users.phase';
    public const PERSIST_USER_EMAIL = 'voting.registered_users.email';
    const PHASE_BEFORE_LOGIN = 'before-login';
    const PHASE_AFTER_LOGIN = 'after-login';

    public function vote(VotingResult $initialResult): VotingResult
    {
        $currentPhase = $this->_component->retrieve(self::PERSIST_CURRENT_PHASE, true, self::PHASE_BEFORE_LOGIN);
        if ($currentPhase !== self::PHASE_AFTER_LOGIN) {
            return $initialResult->setNeedsRedirect(true)
                ->setRedirectUrl($this->getAuthUrl());
        }
        $email = $this->_component->retrieve(self::PERSIST_USER_EMAIL, true, "");
        return $initialResult->setNeedsRedirect(false)
            ->setResultCode(empty($email) ? VotingComponent::VOTE_FAILED_AUTH : VotingComponent::VOTE_SUCCESS)
            ->setUniqueIdentityKey($email);
    }

    public function processRequest(VotingResult $initialResult, ServerRequest $request): VotingResult
    {
        if ($request->is(['post', 'put', 'patch'])) {

            $user = $this->getVerifiedUser($request);

            if ($user instanceof User) {
                $this->_component->persist([
                    self::PERSIST_USER_EMAIL => $user->email,
                    self::PERSIST_CURRENT_PHASE => self::PHASE_AFTER_LOGIN,
                ]);
                $initialResult->setUniqueIdentityKey($user->email)
                    ->setResultCode(VotingComponent::VOTE_SUCCESS);
            } else {
                $initialResult->setResultCode(VotingComponent::VOTE_FAILED_AUTH);
            }
        }

        return $initialResult;
    }

    protected function getVerifiedUser(ServerRequest $request)
    {
        $usersTable = $this->getTableLocator()->get('Users');

        $user = $usersTable->find('all', [
            'conditions' => [
                'Users.' . User::FIELD_EMAIL => $request->getData('username')
            ]
        ])->first();

        if ($user instanceof User) {
            $passwordOK = password_verify($request->getData('password'), $user->password);
            if (!$passwordOK) {
                $user = null;
            }
        }

        return $user;
    }

    private function getAuthUrl(): array
    {
        return [
            '_name' => 'voting_interactive',
            'provider_id' => $this->getProviderId(),
            'otp' => $this->_component->getOTP(),
        ];
    }

    public function getInteractiveTemplatePath(): ?string
    {
        return '/IdentityProviders/registered_users';
    }

    public function getProviderId(): int
    {
        return IdentityProvider::PROVIDER_REGISTERED_USERS;
    }
}
