<?php
declare(strict_types=1);

namespace App\IdentityProvider;


use App\Controller\Component\VotingComponent;
use App\Model\Entity\Vote;

class VotingResult
{

    private bool $_needsRedirect;
    private $_redirect;
    private int $_resultCode = 0;
    private int $_projectId;
    private int $_phaseId;
    private int $_identityProviderId;
    private ?string $_fullIdentityData = null;
    private string $_uniqueIdentityKey;
    private ?Vote $_resultVote;

    public function __construct(int $identityProviderId, int $projectId, int $phaseId, int $resultCode = null, string $uniqueIdentityKey = null, string $fullIdentityData = null, ?array $redirect = null, ?Vote $resultVote = null)
    {
        $this->setIdentityProviderId($identityProviderId);
        $this->setProjectId($projectId);
        $this->setPhaseId($phaseId);
        if (!empty($uniqueIdentityKey)) {
            $this->setUniqueIdentityKey($uniqueIdentityKey);
        }
        if (!empty($fullIdentityData)) {
            $this->setFullIdentityData($fullIdentityData);
        }
        $this->setNeedsRedirect(!empty($redirect));
        if (!empty($redirect)) {
            $this->setRedirectUrl($redirect);
        }
        if (!empty($resultCode)) {
            $this->setResultCode($resultCode);
        }
        if (!empty($resultVote)) {
            $this->setResultVote($resultVote);
        }
    }

    public function setNeedsRedirect(bool $needsRedirect): self
    {
        $this->_needsRedirect = $needsRedirect;
        return $this;
    }

    public function needsRedirect(): bool
    {
        return $this->_needsRedirect;
    }

    public function setRedirectUrl($redirectUrl): self
    {
        $this->_redirect = $redirectUrl;
        return $this;
    }

    public function getRedirectUrl()
    {
        return $this->_redirect;
    }

    public function setResultCode(int $resultCode): self
    {
        $this->_resultCode = $resultCode;
        return $this;
    }

    public function getResultCode(): int
    {
        return $this->_resultCode;
    }

    public function setProjectId(int $projectId): self
    {
        $this->_projectId = $projectId;
        return $this;
    }

    public function getProjectId(): int
    {
        return $this->_projectId;
    }

    public function setUniqueIdentityKey(string $uniqueIdentityKey): self
    {
        $this->_uniqueIdentityKey = $uniqueIdentityKey;
        return $this;
    }

    public function getUniqueIdentityKey(): string
    {
        return $this->_uniqueIdentityKey;
    }

    public function setPhaseId(int $phaseId): self
    {
        $this->_phaseId = $phaseId;
        return $this;
    }

    public function getPhaseId(): int
    {
        return $this->_phaseId;
    }

    public function setFullIdentityData(string $fullIdentityData): self
    {
        $this->_fullIdentityData = $fullIdentityData;
        return $this;
    }

    public function getFullIdentityData(): ?string
    {
        return $this->_fullIdentityData;
    }

    public function setIdentityProviderId(int $providerId): self
    {
        $this->_identityProviderId = $providerId;
        return $this;
    }

    public function getIdentityProviderId(): int
    {
        return $this->_identityProviderId;
    }

    public function setResultVote(Vote $vote): self
    {
        $this->_resultVote = $vote;
        return $this;
    }

    public function getResultVote(): ?Vote
    {
        return $this->_resultVote;
    }

    public function isError()
    {
        return $this->getResultCode() !== VotingComponent::VOTE_SUCCESS;
    }

    public function isFinal()
    {
        return !$this->needsRedirect();
    }

    public function isInitial()
    {
        return $this->getResultCode() === 0;
    }

}
