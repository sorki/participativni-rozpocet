<?php
declare(strict_types=1);

namespace App\IdentityProvider;


use App\Controller\Component\VotingComponent;
use App\Model\Entity\IdentityProvider;
use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\Saml\NiaContainer;
use App\Saml\NiaExtensions;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Routing\Router;
use Cake\Utility\Text;
use RobRichards\XMLSecLibs\XMLSecurityDSig;
use RobRichards\XMLSecLibs\XMLSecurityKey;
use SAML2\AuthnRequest;
use SAML2\Certificate\Key;
use SAML2\Compat\ContainerSingleton;
use SAML2\Constants;
use SAML2\DOMDocumentFactory;
use SAML2\EncryptedAssertion;
use SAML2\Utils;
use SAML2\XML\Chunk;
use SAML2\XML\ds\KeyInfo;
use SAML2\XML\ds\X509Certificate;
use SAML2\XML\ds\X509Data;
use SAML2\XML\md\EntityDescriptor;
use SAML2\XML\md\IndexedEndpointType;
use SAML2\XML\md\KeyDescriptor;
use SAML2\XML\md\SPSSODescriptor;
use SAML2\XML\saml\Issuer;

class NIAIdentityProvider extends AbstractIdentityProvider
{
    use LocatorAwareTrait;

    const SETTING_PRIVATE_KEY = 'nia_private_key';
    const SETTING_PUBLIC_KEY = 'nia_public_key';
    const SETTING_CERTIFICATE = 'nia_certificate';

    const TARGET_URL = 'https://tnia.eidentita.cz/FPSTS/saml2/basic';

    const TNIA_TEST_CERTIFICATE = '-----BEGIN CERTIFICATE-----
MIIH0jCCBbqgAwIBAgIEAVDtYDANBgkqhkiG9w0BAQsFADBpMQswCQYDVQQGEwJD
WjEXMBUGA1UEYRMOTlRSQ1otNDcxMTQ5ODMxHTAbBgNVBAoMFMSMZXNrw6EgcG/F
oXRhLCBzLnAuMSIwIAYDVQQDExlQb3N0U2lnbnVtIFF1YWxpZmllZCBDQSA0MB4X
DTIwMDIxOTA4NDE0MloXDTIxMDMxMDA4NDE0MloweTELMAkGA1UEBhMCQ1oxFzAV
BgNVBGETDk5UUkNaLTcyMDU0NTA2MScwJQYDVQQKDB5TcHLDoXZhIHrDoWtsYWRu
w61jaCByZWdpc3Ryxa8xFjAUBgNVBAMMDUdHX0ZQU1RTX1RFU1QxEDAOBgNVBAUT
B1MyNzU3MzAwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC1ehtkDsxm
4RMIvPZAL8axrZIAisT29kkxsi0I7dAiih2fEvWHcG5jMl8hdcO40h/RVOZEjIGy
Cz4zXCdHwqmuFJCRpTBEJuPmoLYjIFddB9KptR7KJZqH1ANGk9beCbmFByNTR5mT
xnUm7l9lWOfB4kS8bmPhawn3EuCgzI2gVN7nfwfdPPxG7HS+BUz88wWxASiSZhBb
DZzM3XL+zRkgrCs7CuqEP4/WnGJfRPRJhPIRxJRAeZm/MncVUY8tXKLx65zz7wly
lS/Jw4j0CnM81Hrc7rh5BYFHlQ1e37RH5LeWK5/CdK1bf6u6MPFECnn9tyl7pAjH
6g/JQU+IgxdRAgMBAAGjggNwMIIDbDCCASYGA1UdIASCAR0wggEZMIIBCgYJZ4EG
AQQBEoFIMIH8MIHTBggrBgEFBQcCAjCBxhqBw1RlbnRvIGt2YWxpZmlrb3Zhbnkg
Y2VydGlmaWthdCBwcm8gZWxla3Ryb25pY2tvdSBwZWNldCBieWwgdnlkYW4gdiBz
b3VsYWR1IHMgbmFyaXplbmltIEVVIGMuIDkxMC8yMDE0LlRoaXMgaXMgYSBxdWFs
aWZpZWQgY2VydGlmaWNhdGUgZm9yIGVsZWN0cm9uaWMgc2VhbCBhY2NvcmRpbmcg
dG8gUmVndWxhdGlvbiAoRVUpIE5vIDkxMC8yMDE0LjAkBggrBgEFBQcCARYYaHR0
cDovL3d3dy5wb3N0c2lnbnVtLmN6MAkGBwQAi+xAAQEwgZsGCCsGAQUFBwEDBIGO
MIGLMAgGBgQAjkYBATBqBgYEAI5GAQUwYDAuFihodHRwczovL3d3dy5wb3N0c2ln
bnVtLmN6L3Bkcy9wZHNfZW4ucGRmEwJlbjAuFihodHRwczovL3d3dy5wb3N0c2ln
bnVtLmN6L3Bkcy9wZHNfY3MucGRmEwJjczATBgYEAI5GAQYwCQYHBACORgEGAjB9
BggrBgEFBQcBAQRxMG8wOwYIKwYBBQUHMAKGL2h0dHA6Ly9jcnQucG9zdHNpZ251
bS5jei9jcnQvcHNxdWFsaWZpZWRjYTQuY3J0MDAGCCsGAQUFBzABhiRodHRwOi8v
b2NzcC5wb3N0c2lnbnVtLmN6L09DU1AvUUNBNC8wDgYDVR0PAQH/BAQDAgXgMB8G
A1UdJQQYMBYGCCsGAQUFBwMEBgorBgEEAYI3CgMMMB8GA1UdIwQYMBaAFA8ofD42
ADgQUK49uCGXi/dgXGF4MIGxBgNVHR8EgakwgaYwNaAzoDGGL2h0dHA6Ly9jcmwu
cG9zdHNpZ251bS5jei9jcmwvcHNxdWFsaWZpZWRjYTQuY3JsMDagNKAyhjBodHRw
Oi8vY3JsMi5wb3N0c2lnbnVtLmN6L2NybC9wc3F1YWxpZmllZGNhNC5jcmwwNaAz
oDGGL2h0dHA6Ly9jcmwucG9zdHNpZ251bS5ldS9jcmwvcHNxdWFsaWZpZWRjYTQu
Y3JsMB0GA1UdDgQWBBTyYwg9iyfBRKJo3XW3pm71cA149jANBgkqhkiG9w0BAQsF
AAOCAgEAJoWNxo50/RiOKiYA9+OZ+39wJOrMf6P1EoSTXPKGgFSHtBgJ5X7C3YSf
JwrCbbgBjFdU4HEJOQeTl2zqJlh9DqrUxzAuLbKKbMdDn8MSWBjSb2EaQ2z/oBoC
CtR/ThPc5qH30k29M/CREstTgnBTPBwiJ33MvsPY7I1g6WHgZpma55ERKvdsavrS
/TvXel5/TXWZkc0EOpn6qn1XISwD1NRn+7k4n+xQ81A0R1/Xs/ZKOZshPyabIoOB
11w7LX3KtJpppn5+gr0CeQzC482f5I3smgkr2PUODjOsC7SceCLqVagP6O2vwgXL
DN0X3qRT+UU6iCl8m8GA3iofyNiXCm3ZHhni7dHesnW09BFjJkCzYsn6CM4W8Zg2
Mtz3EKzXEYS1X0XZ5ukXie51zfjwvEssLVco1XSOnE+cW9+ZIpIcWUcmFe5YN3AT
+/Z/GVUUeMXbUi6PeKMPtxj6g3Vdx68WOIl2wIuG7FthPy4heTpVjN7nniPpPbt4
6sVhyjwPtBDzSooFhe+lh4RaMqMzIMKJrH0PwZ6p3u/vy2+xTMDspjA+DbkjOiir
5L0JpzsIsH6yhDLkvlyRTOGkMFVHYAuLS5z160usMywWJRcnyioriUxn6reKqvyJ
VuwR71QV2jhnuIjB23dYTJqo0rwBcrlMfImDatLX5Ts5TIN31Mc=
-----END CERTIFICATE-----';

    public function __construct()
    {
        ContainerSingleton::setContainer(new NiaContainer());
    }

    public function getProviderId(): int
    {
        return IdentityProvider::PROVIDER_NIA;
    }

    public function vote(VotingResult $initialResult): VotingResult
    {
        if ($initialResult->isInitial()) {
            $authRequest = $this->generateAuthnRequest();
            $query = gzdeflate($authRequest);
            $query = base64_encode($query);
            $query = urlencode($query);

            $initialResult->setNeedsRedirect(true)
                ->setRedirectUrl(self::TARGET_URL . (parse_url(self::TARGET_URL, PHP_URL_QUERY) ? '&' : '?') . 'SAMLRequest=' . $query);
        }
        return $initialResult;
    }

    public function processRequest(VotingResult $initialResult, ServerRequest $request): VotingResult
    {
        if ($request->is(['post', 'put', 'patch'])) {
            $raw_response = $request->getData('SAMLResponse');
            $raw_response = base64_decode($raw_response);
            if (empty($raw_response)) {
                return $initialResult->setResultCode(VotingComponent::VOTE_FAILED_AUTH);
            }
            $post_dom = DOMDocumentFactory::fromString($raw_response);
            $response = new \SAML2\Response($post_dom->documentElement);

            $tnia_public_key = new XMLSecurityKey(XMLSecurityKey::RSA_SHA256, ['type' => 'public']);
            //$tnia_cert = preg_replace("/\r|\n| /", '', self::TNIA_TEST_CERTIFICATE);
            //dd($tnia_cert);
            $tnia_public_key->loadKey(self::TNIA_TEST_CERTIFICATE, false, true);

            $isValid = true;
            try {
                if (!$response->validate($tnia_public_key)) {
                    $isValid = false;
                }
            } catch (\Throwable $e) {
                $isValid = false;
            }
            if (!$isValid) {
                return $initialResult->setResultCode(VotingComponent::VOTE_FAILED_AUTH);
            }

            list ($pkey, $pubkey, $cert) = $this->ensureCertificateData($this->_component->getOrganization()->id);
            $local_private_key = new XMLSecurityKey(XMLSecurityKey::RSA_OAEP_MGF1P, ['type' => 'private']);
            $local_private_key->loadKey($pkey, false, false);

            $decrypted = false;
            foreach ($response->getAssertions() as $assertion) {
                if ($assertion instanceof EncryptedAssertion) {
                    $decrypted = $assertion->getAssertion($local_private_key);
                }
            }

            if (!$decrypted) {
                return $initialResult->setResultCode(VotingComponent::VOTE_FAILED_AUTH);
            }

            return $initialResult->setResultCode(VotingComponent::VOTE_SUCCESS)
                ->setUniqueIdentityKey($decrypted->getNameId()->getValue())
                ->setFullIdentityData($decrypted->toXML()->ownerDocument->saveXML())
                ->setNeedsRedirect(false);
        }
        return parent::processRequest($initialResult, $request);
    }

    public function allowsNonInteractive(): bool
    {
        return true;
    }

    public function hasMetadata(): bool
    {
        return true;
    }

    private function generateAuthnRequest(): string
    {
        $auth_request = new AuthnRequest();
        $auth_request->setId('_' . Text::uuid());
        $issuer = new Issuer();
        $issuer->setValue(Router::url(['_name' => 'public_organization_detail', 'organization_id' => $this->_component->getOrganization()->id], true));
        $auth_request->setIssuer($issuer);
        // explicitní deklarace příjemce zprávy
        $auth_request->setDestination(self::TARGET_URL);
        // adresa kam se má uživatel přesměrovat při dokončení procesu na straně IdP
        $auth_request->setAssertionConsumerServiceURL(Router::url(['_name' => 'voting_non_interactive', 'provider_id' => $this->getProviderId()], true));
        // vyžadovaná úroveň ověření identity
        // LOW dovoluje využít NIA jméno+heslo+sms, stejně jako datovou schránku FO nebo identitu zahraničního občana
        // SUBSTANTIAL pak dovoluje méně variant
        // HIGH dovoluje pouze elektronický občanský průkaz
        $auth_request->setRequestedAuthnContext([
            'AuthnContextClassRef' => ['http://eidas.europa.eu/LoA/high'],
            'Comparison' => 'minimum'
        ]);

        // vygenerování nepodepsaného požadavku
        $auth_request_xml_domelement = $auth_request->toUnsignedXML();
        // přidání vyžadovaných atributů (informací o uživateli), element samlp:Extensions
        $exts = new NiaExtensions($auth_request_xml_domelement);
        $exts->addRequestedAttributeParams(
            NiaExtensions::$ATTR_AGE['Name'], NiaExtensions::NAME_FORMAT_URI, true
        );
        $exts->addRequestedAttributeParams(
            NiaExtensions::$ATTR_CZMORIS_TR_ADRESA_ID['Name'], NiaExtensions::NAME_FORMAT_URI, true
        );
        $auth_request_xml_domelement = $exts->toXML();

        $auth_request_xml = $auth_request_xml_domelement->ownerDocument->saveXML($auth_request_xml_domelement);
        $auth_request_xml_domelement = DOMDocumentFactory::fromString($auth_request_xml);

        list($pkey, $pub, $cert) = $this->ensureCertificateData($this->_component->getOrganization()->id);

        // vložení vlastního podpisu naším privátním klíčem
        $auth_request_xml_domelement = $this->insertSignature($auth_request_xml_domelement->documentElement, $pkey, $cert, false);

        return $auth_request_xml_domelement->ownerDocument->saveXML();
    }

    public function renderMetadata(Organization $organization, ServerRequest $request, Response $response): Response
    {
        $descriptor = new EntityDescriptor();
        $acs = new IndexedEndpointType();
        $acs->setIsDefault(true);
        $acs->setBinding(Constants::BINDING_HTTP_POST);
        $acs->setIndex(1);
        $acs->setLocation(Router::url(['_name' => 'voting_non_interactive', 'provider_id' => $this->getProviderId()], true));

        $spsso = new SPSSODescriptor();
        $spsso->setAuthnRequestsSigned(true);
        $spsso->setWantAssertionsSigned(true);
        $spsso->addProtocolSupportEnumeration('urn:oasis:names:tc:SAML:2.0:protocol');
        $spsso->addAssertionConsumerService($acs);
        $spsso->setNameIDFormat([
            Constants::NAMEFORMAT_BASIC,
            Constants::NAMEFORMAT_UNSPECIFIED,
            Constants::NAMEFORMAT_URI
        ]);

        list($privKey, $pubKey, $certificate) = $this->ensureCertificateData($organization->id);

        $local_cert_x509_cert = new X509Certificate();
        $local_cert_x509_cert->setCertificate($this->getCertificateData($certificate));
        $local_cert_x509_data = new X509Data();
        $local_cert_x509_data->setData([$local_cert_x509_cert]);

        $key_info = new KeyInfo();
        $key_info->addInfo($local_cert_x509_data);

        $doc = DOMDocumentFactory::create();
        $enc_method_dom = $doc->createElementNS('urn:oasis:names:tc:SAML:2.0:metadata', 'EncryptionMethod');
        $enc_method_dom->setAttribute('Algorithm', XMLSecurityKey::AES256_CBC);
        $enc_method = new Chunk($enc_method_dom);

        $sign_key_descriptor = new KeyDescriptor();
        $sign_key_descriptor->setUse(Key::USAGE_SIGNING);
        $sign_key_descriptor->setKeyInfo($key_info);

        $enc_key_descriptor = new KeyDescriptor();
        $enc_key_descriptor->setUse(Key::USAGE_ENCRYPTION);
        $enc_key_descriptor->setKeyInfo($key_info);

        $enc_key_descriptor->setEncryptionMethod([$enc_method]);

        $spsso->addKeyDescriptor($sign_key_descriptor);
        $spsso->addKeyDescriptor($enc_key_descriptor);

        $descriptor->addRoleDescriptor($spsso);
        $descriptor->setID('_' . Text::uuid());
        $descriptor->setEntityID(Router::url(['_name' => 'public_organization_detail', 'organization_id' => $organization->id], true));
        $descriptor->setValidUntil(strtotime('tomorrow'));

        $metadata_dom = $descriptor->toXML();

        $extensions = $metadata_dom->ownerDocument->createElementNS('urn:oasis:names:tc:SAML:2.0:metadata', 'md:Extensions');
        $sptype = $metadata_dom->ownerDocument->createElementNS('http://eidas.europa.eu/saml-extensions', 'eidas:SPType');
        $sptype->nodeValue = 'public';
        $extensions->appendChild($sptype);
        $digest_method = $metadata_dom->ownerDocument->createElementNS('urn:oasis:names:tc:SAML:metadata:algsupport', 'alg:DigestMethod');
        $digest_method->setAttribute('Algorithm', XMLSecurityDSig::SHA256);
        $extensions->appendChild($digest_method);
        $signing_method = $metadata_dom->ownerDocument->createElementNS('urn:oasis:names:tc:SAML:metadata:algsupport', 'alg:SigningMethod');
        $signing_method->setAttribute('MinKeySize', '256');
        $signing_method->setAttribute('Algorithm', XMLSecurityKey::RSA_SHA256);
        $extensions->appendChild($signing_method);

        $metadata_dom->appendChild($extensions);

        $this->insertSignature($metadata_dom, $privKey, $certificate);

        return $response->withType('xml')->withStringBody($metadata_dom->ownerDocument->saveXML());
    }

    private function ensureCertificateData(int $organization_id): array
    {
        $settingsTable = $this->getTableLocator()->get('OrganizationSettings');
        /** @var OrganizationSetting $privateKey */
        $privateKey = $settingsTable->findOrCreate([
            OrganizationSetting::FIELD_ORGANIZATION_ID => $organization_id,
            OrganizationSetting::FIELD_NAME => self::SETTING_PRIVATE_KEY
        ], function (OrganizationSetting $setting) {
            $res = openssl_pkey_new([
                'digest_alg' => 'sha512',
                'private_key_bits' => 4096,
                'private_key_type' => OPENSSL_KEYTYPE_RSA
            ]);
            openssl_pkey_export($res, $privKey);
            $setting->value = $privKey;
        });
        /** @var OrganizationSetting $publicKey */
        $publicKey = $settingsTable->findOrCreate([
            OrganizationSetting::FIELD_ORGANIZATION_ID => $organization_id,
            OrganizationSetting::FIELD_NAME => self::SETTING_PUBLIC_KEY
        ], function (OrganizationSetting $setting) use ($privateKey) {
            $res = openssl_pkey_get_private($privateKey->value);
            $pubKey = openssl_pkey_get_details($res);
            $setting->value = $pubKey['key'];
        });
        /** @var OrganizationSetting $certificate */
        $certificate = $settingsTable->findOrCreate([
            OrganizationSetting::FIELD_ORGANIZATION_ID => $organization_id,
            OrganizationSetting::FIELD_NAME => self::SETTING_CERTIFICATE
        ], function (OrganizationSetting $setting) use ($privateKey) {

            $res = openssl_pkey_get_private($privateKey->value);

            $dn = [
                'countryName' => 'CZ',
                'stateOrProvinceName' => 'Prague',
                'organizationName' => 'Otevřená Města',
                'commonName' => 'PaRo2',
                'emailAddress' => 'paro2@otevrenamesta.cz',
            ];

            $csr = openssl_csr_new($dn, $res, ['digest_alg' => 'sha512']);
            $x509 = openssl_csr_sign($csr, null, $res, 365, ['digest_alg' => 'sha512']);
            openssl_x509_export($x509, $certout);

            $setting->value = $certout;
        });

        return [$privateKey->value, $publicKey->value, $certificate->value];
    }

    private function getCertificateData(string $cert_data): ?string
    {
        $prefix = '-----BEGINCERTIFICATE-----';
        $suffix = '----ENDCERTIFICATE-----';
        $cert_data = trim(preg_replace('/\s+/', '', $cert_data));
        $cert_data = mb_substr($cert_data, strpos($cert_data, $prefix) + strlen($prefix));
        $cert_data = mb_substr($cert_data, 0, strpos($cert_data, $suffix) - 1);
        return trim($cert_data);
    }

    public function insertSignature(\DOMElement $domelement, string $privateKey, string $certificate, $insertCertificates = true)
    {
        $local_private_key = new XMLSecurityKey(XMLSecurityKey::RSA_SHA256, ['type' => 'private']);
        $local_private_key->loadKey($privateKey);

        $insertAfter = $domelement->firstChild;
        if ($domelement->getElementsByTagName('Issuer')->length > 0) {
            $insertAfter = $domelement->getElementsByTagName('Issuer')->item(0)->nextSibling;
        }

        Utils::insertSignature($local_private_key, $insertCertificates ? [$certificate] : [], $domelement, $insertAfter);

        return $domelement;
    }
}
