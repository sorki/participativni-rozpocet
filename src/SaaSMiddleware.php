<?php
declare(strict_types=1);

namespace App;

use App\Model\Entity\Domain;
use Cake\Cache\Cache;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Routing\Router;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SaaSMiddleware implements MiddlewareInterface
{
    use LocatorAwareTrait;

    private string $cacheConfig;
    /**
     * @var iterable|Domain[]|null
     */
    private ?iterable $cacheData = null;
    public const CACHE_KEY = 'paro2_saas_domains';
    public const ALLOWED_CONTROLLERS = ['Portal', 'Api', 'Voting'];
    public static ?Domain $domain = null;

    public function __construct(string $cacheConfig = 'default')
    {
        $this->cacheConfig = $cacheConfig;
    }

    private function getConfiguredDomain(string $domain): ?Domain
    {
        $this->cacheData = Cache::read(self::CACHE_KEY, $this->cacheConfig);
        if ($this->cacheData === null) {
            $domainsTable = $this->getTableLocator()->get('Domains');
            $this->cacheData = $domainsTable->find('all', [
                'fields' => [
                    Domain::FIELD_DOMAIN,
                    Domain::FIELD_ID,
                    Domain::FIELD_IS_ENABLED,
                    Domain::FIELD_ORGANIZATION_ID,
                ],
            ])->toArray();
            Cache::write(self::CACHE_KEY, $this->cacheData, $this->cacheConfig);
        }
        foreach ($this->cacheData ?? [] as $cachedDomain) {
            if ($cachedDomain->domain === $domain) {
                self::$domain = $cachedDomain;
                break;
            }
        }
        return self::$domain;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $domain = trim($_SERVER['HTTP_HOST'] ?? '');
        if (empty($domain)) {
            return new Response(['status' => 404]);
        }
        $configuredDomain = $this->getConfiguredDomain($domain);
        if ($configuredDomain instanceof Domain) {
            if (!$configuredDomain->is_enabled) {
                // if domain is not enabled, disable any response
                return (new Response())->withStatus(403, 'Domain is currently disabled');
            } elseif ($request instanceof ServerRequest && !in_array($request->getParam('controller'), self::ALLOWED_CONTROLLERS, true)) {
                // if domain is enabled and requested resource is not handled by controllers allowed for portal presentations, redirect to portal index
                return (new Response())->withLocation(Router::url(['_name' => 'portal_index']));
            }
        }
        // default fallback
        return $handler->handle($request);
    }
}
