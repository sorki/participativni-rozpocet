<?php
declare(strict_types=1);

namespace App\Controller;

use App\Controller\Component\VotingComponent;
use App\Model\Table\OrganizationsTable;
use Cake\Event\EventInterface;
use Cake\Http\Cookie\Cookie;

/**
 * @property-read VotingComponent $Voting
 * @property-read OrganizationsTable $Organizations
 */
class VotingController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadComponent('Voting');
        $this->Authentication->allowUnauthenticated([$this->getRequest()->getParam('action')]);
        $this->viewBuilder()->setLayout('voting');
    }

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);

        if ($this->getRequest()->getParam('_name') === 'voting_non_interactive') {
            $this->FormProtection->setConfig('validate', false);
        }
    }

    public function result()
    {
        $resultMessage = $this->Voting->retrieve(VotingComponent::PERSIST_MESSAGE, true);
        $allowedRedirectUrl = $this->Voting->retrieve(VotingComponent::PERSIST_REDIRECT_URL, true);
        $resultIsError = $this->Voting->retrieve(VotingComponent::PERSIST_IS_ERROR, true) === true
            || empty($resultMessage);

        $this->set(compact('resultMessage', 'allowedRedirectUrl', 'resultIsError'));
    }

    public function chooseProvider()
    {
        $this->Voting->restoreFromPersistence();
        $providers = $this->Voting->getEnabledVotingProviders();

        if (empty($providers)) {
            $this->Voting->persistAbort(VotingComponent::VOTE_INCORRECT_PARAMS);
            $this->redirect(['_name' => 'voting_final_status']);

            return;
        }

        $this->Voting->persistSuccess();

        if (count($providers) === 1) {
            $provider_id = array_flip($providers);
            $provider_id = reset($provider_id);

            $this->redirect(['_name' => 'voting_interactive', 'otp' => $this->Voting->getOTP(), 'provider_id' => $provider_id]);

            return;
        }

        $this->set(compact('providers'));
        $this->set('project', $this->Voting->getProject());
        $this->set('component', $this->Voting);
        $this->set('allowedRedirectUrl', $this->Voting->getAuthorizedRedirect());
    }

    public function interactive(int $provider_id, string $otp = null)
    {
        $provider = $this->Voting->getIdentityProvider($provider_id);
        if ($this->getRequest()->getParam('_name') === 'voting_interactive' && $provider->allowsNonInteractive()) {
            $this->setResponse($this->getResponse()
                ->withCookie(
                    new Cookie('last_project_id', strval($this->Voting->retrieve(VotingComponent::PERSIST_PROJECT)), null, '/', null, true, true, 'None')
                )->withCookie(
                    new Cookie('target_redirect', strval($this->Voting->retrieve(VotingComponent::PERSIST_REDIRECT_URL)), null, '/', null, true, true, 'None')
                ));
        } else if ($this->getRequest()->getParam('_name') === 'voting_non_interactive' && $provider->allowsNonInteractive()) {
            $last_project_id = $this->getRequest()->getCookie('last_project_id');
            $project = $this->Organizations->Appeals->Projects->get($last_project_id, [
                'contain' => [
                    'Appeals'
                ]
            ]);
            $this->Voting->isProjectVotable($project->appeal->organization_id, $project->appeal_id, $project->id);
            $this->Voting->persist([VotingComponent::PERSIST_REDIRECT_URL => $this->getRequest()->getCookie('target_redirect')]);
            $this->Voting->persistSuccess();
        }
        $this->Voting->restoreFromPersistence();

        if (
            (!empty($otp) && $this->Voting->ensureOTP($otp) !== true)
            || (empty($otp) && !$provider->allowsNonInteractive())
        ) {
            // OTP verification failed
            $this->Voting->persistAbort(VotingComponent::VOTE_INCORRECT_PARAMS);
            $this->redirect(['_name' => 'voting_final_status']);

            return;
        }

        $result = $provider->processRequest($this->Voting->getInitialVotingResult($provider_id), $this->getRequest());
        $result = $provider->vote($result);

        if ($result->isInitial()) {
            $this->Voting->persistSuccess();
        } elseif ($result->isFinal() || $result->isError()) {
            if ($result->isFinal()) {
                $this->Voting->persistVote($result);
            }
            if ($result->isError()) {
                $this->Voting->persistAbort($result->getResultCode());
            } else {
                $this->Voting->persistSuccess();
                $this->Voting->persist([
                    VotingComponent::PERSIST_IS_ERROR => false,
                    VotingComponent::PERSIST_MESSAGE => __('Váš hlas byl zaznamenán, děkujeme'),
                ]);
            }

            if ($this->getRequest()->getParam('_name') === 'voting_interactive') {
                $this->redirect(['_name' => 'voting_final_status']);
            } else {
                $this->result();
                $this->viewBuilder()->setTemplate('result');
            }

            return;
        }

        if ($result->needsRedirect() && $provider->allowsNonInteractive()) {
            $this->Voting->persistSuccess();
            $this->redirect($result->getRedirectUrl());

            return;
        }

        $this->set('component', $this->Voting);
        $this->viewBuilder()->setTemplate($provider->getInteractiveTemplatePath());
    }

    public function metadata(int $organization_id, int $provider_id)
    {
        $organization = $this->Organizations->get($organization_id);
        $provider = $this->Voting->getIdentityProvider($provider_id);
        if (!$organization->is_enabled || !$provider->hasMetadata()) {
            return $this->getResponse()->withStatus(404);
        }
        return $provider->renderMetadata($organization, $this->getRequest(), $this->getResponse());
    }

}
