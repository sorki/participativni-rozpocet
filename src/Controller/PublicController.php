<?php
declare(strict_types=1);

namespace App\Controller;

class PublicController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index']);
    }

    public function index()
    {

    }

}
