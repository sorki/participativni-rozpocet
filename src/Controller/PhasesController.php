<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Phase;
use App\Model\Table\AppealsTable;
use App\Model\Table\IdentityProvidersTable;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\PhasesTable;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read AppealsTable $Appeals
 * @property-read PhasesTable $Phases
 * @property-read IdentityProvidersTable $IdentityProviders
 */
class PhasesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('Appeals');
        $this->loadModel('Phases');
        $this->loadModel('IdentityProviders');
    }

    public function addModify(int $organization_id, int $appeal_id, int $phase_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization_id);
        $phase = $phase_id > 0 ? $this->Phases->getWithAppeal($phase_id, $appeal_id, ['IdentityProviders']) :
            $this->Phases->newEmptyEntity();
        $identity_providers = $this->IdentityProviders->find('list');

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $phase = $this->Phases->patchEntity($phase, $this->getRequest()->getData() + [
                    Phase::FIELD_APPEAL_ID => $appeal->id,
                ]
            );

            if ($phase->allows_voting && !empty($phase->date_start) && !empty($phase->identity_providers)) {
                $conflicting_phases = $this->Phases->find('all', [
                    'conditions' => [
                        'Phases.' . Phase::FIELD_APPEAL_ID => $appeal->id,
                        'Phases.' . Phase::FIELD_DATE_START . ' <=' => $phase->date_start,
                        'Phases.' . Phase::FIELD_DATE_END . ' >=' => $phase->date_start,
                    ],
                    'contain' => [
                        'IdentityProviders',
                    ],
                ])->filter(function (Phase $candidate) use ($phase) {
                    return !empty($candidate->identity_providers) && ($phase->isNew() || $candidate->id !== $phase->id)
                        && (($candidate->allows_proposals_submission && $phase->allows_proposals_submission)
                            || ($candidate->allows_voting && $phase->allows_voting)
                        );
                })->toList();
                if (!empty($conflicting_phases)) {
                    $phase->setError(Phase::FIELD_DATE_START, sprintf("%s (%s)", __('Začátek fáze se překrývá s jinou fází, ve které je povoleno hlasování'), $conflicting_phases[0]->name));
                }
            }

            if ($phase->allows_voting && !empty($phase->date_end) && !empty($phase->identity_providers)) {
                $conflicting_phases = $this->Phases->find('all', [
                    'conditions' => [
                        'Phases.' . Phase::FIELD_APPEAL_ID => $appeal->id,
                        'Phases.' . Phase::FIELD_DATE_START . ' <=' => $phase->date_end,
                        'Phases.' . Phase::FIELD_DATE_END . ' >=' => $phase->date_end,
                    ],
                    'contain' => [
                        'IdentityProviders',
                    ],
                ])->filter(function (Phase $candidate) use ($phase) {
                    return !empty($candidate->identity_providers) && ($phase->isNew() || $candidate->id !== $phase->id)
                        && (($candidate->allows_proposals_submission && $phase->allows_proposals_submission)
                            || ($candidate->allows_voting && $phase->allows_voting)
                        );
                })->toList();
                if (!empty($conflicting_phases)) {
                    $phase->setError(Phase::FIELD_DATE_END, sprintf("%s (%s)", __('Konec fáze se překrývá s jinou fází, ve které je povoleno hlasování'), $conflicting_phases[0]->name));
                }
            }

            if ($this->Phases->save($phase)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_APPEALS, [$organization->id, $appeal->id]);
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_PROJECTS, [$organization->id, $appeal->id]);
                $this->redirect(['_name' => 'appeal_detail', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'appeal', 'phase', 'identity_providers'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id], sprintf("Ročník %d", $appeal->year) => ['_name' => 'appeal_detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id]]);
    }
}
