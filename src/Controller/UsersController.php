<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use Cake\Http\Response;

/**
 * @property UsersTable $Users
 */
class UsersController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->Authentication->allowUnauthenticated(['login', 'register', 'passwordReset', 'passwordResetGo']);
    }

    public function register(): void
    {
        $user = $this->Users->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $entityData = $this->getRequest()->getData();
            $entityData[User::FIELD_VERIFICATION_CODE] = random_str('alphanum', 32);
            $user = $this->Users->newEntity($entityData);
            $this->set(compact('user'));

            if ($user->hasErrors()) {
                return;
            }

            // set hashed version
            $user->setPassword($user->password);

            if ($this->Users->save($user)) {
                $this->Flash->success('Registrace proběhla úspěšně, nyní se můžete přihlásit');
                $this->redirect(['_name' => 'login']);
            } else {
                $this->Flash->error('Registrace neproběhla úspěšně, prosím zkuste to znovu');
            }
        }

        $this->set(compact('user'));
    }

    public function login(): ?Response
    {
        $result = $this->Authentication->getResult();

        if (!$this->getRequest()->is(['post', 'put', 'patch']) && $this->getCurrentUser() instanceof User) {
            return $this->redirect('/');
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $user = $result->getData();

            if (!$result->isValid() || !($user instanceof User)) {
                $this->Flash->error('Neplatné uživatelské jméno nebo heslo');

                return null;
            }

            if ($user->is_enabled !== true) {
                $this->Flash->error(__('Váš účet je aktuálně zakázán'));
                $this->Authentication->logout();

                return null;
            }

            $redirect = '/';
            if (!empty($user->organizations)) {
                if (count($user->organizations) > 1) {
                    $redirect = ['_name' => 'my_organizations'];
                } else {
                    $firstOrganization = reset($user->organizations);
                    $redirect = ['_name' => 'organization_detail', 'organization_id' => $firstOrganization->id];
                }
            }

            return $this->redirect($this->Authentication->getLoginRedirect() ?? $redirect);
        }
        $this->set('user', $this->Users->newEmptyEntity());
        return null;
    }

    public function logout(): ?Response
    {
        $this->Authentication->logout();
        return $this->redirect('/');
    }

}
