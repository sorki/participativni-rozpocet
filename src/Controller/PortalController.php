<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Organization;
use App\Model\Table\OrganizationsTable;
use App\SaaSMiddleware;
use Cake\Core\Configure;

/**
 * @property-read OrganizationsTable $Organizations
 */
class PortalController extends AppController
{
    private ?Organization $organization = null;

    public function initialize(): void
    {
        Configure::write('debug', false);
        parent::initialize();
        $this->loadModel('Organizations');
        $this->Authentication->allowUnauthenticated([$this->getRequest()->getParam('action')]);
        $this->_viewBuilder = $this->viewBuilder()->setLayout('portal');
        $this->organization = $this->Organizations->get(SaaSMiddleware::$domain->organization_id);
        $this->set('organization', $this->organization);
        $this->set('title', $this->organization->name);
    }

    public function index()
    {
    }

}
