<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Domain;
use App\Model\Table\DomainsTable;
use App\Model\Table\OrganizationsTable;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read DomainsTable $Domains
 */
class DomainsController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('Domains');
    }

    public function addModify(int $organization_id, ?int $domain_id = null)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $domain = $domain_id > 0 ? $this->Domains->getWithOrganization($domain_id, $organization->id)
            : $this->Domains->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $domain = $this->Domains->patchEntity($domain, $this->getRequest()->getData() + [
                    Domain::FIELD_ORGANIZATION_ID => $organization->id,
                ]
            );
            $parsed = parse_url($domain->domain);
            if (is_array($parsed) && isset($parsed['host'])) {
                $domain->domain = $parsed['host'];
            }
            if ($this->Domains->save($domain)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['controller' => 'Organizations', 'action' => 'detail', 'organization_id' => $organization_id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('domain', 'organization'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function delete(int $organization_id, int $domain_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $domain = $this->Domains->getWithOrganization($domain_id, $organization_id);
        if ($this->Domains->delete($domain)) {
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }
        return $this->redirect(['action' => 'detail', 'controller' => 'Organizations', 'organization_id' => $organization->id]);
    }

}
