<?php
declare(strict_types=1);

namespace App\Controller;

use App\Controller\Component\VotingComponent;
use App\Model\Entity\Appeal;
use App\Model\Entity\Attachment;
use App\Model\Entity\AttachmentType;
use App\Model\Entity\Domain;
use App\Model\Entity\IdentityProvider;
use App\Model\Entity\Organization;
use App\Model\Entity\Phase;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectCategory;
use App\Model\Entity\ProjectLog;
use App\Model\Entity\ProjectLogType;
use App\Model\Entity\ProjectStatus;
use App\Model\Table\AppealsTable;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\ProjectsTable;
use Cake\Cache\Cache;
use Cake\Collection\Collection;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Filesystem\File;
use Cake\I18n\FrozenDate;
use Cake\Routing\Router;
use Psr\Http\Message\MessageInterface;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read AppealsTable $Appeals
 * @property-read ProjectsTable $Projects
 * @property-read VotingComponent $Voting
 */
class ApiController extends AppController
{

    public const CACHE_CONFIG = 'api';
    public const CACHE_KEY_PREFIX_INFO = 'v1_info';
    public const CACHE_KEY_PREFIX_ORGANIZATIONS = 'v1_organizations';
    public const CACHE_KEY_PREFIX_ORGANIZATION_DETAIL = 'v1_organization';
    public const CACHE_KEY_PREFIX_APPEALS = 'v1_organization_appeals';
    public const CACHE_KEY_PREFIX_PROJECTS = 'v1_appeal_projects';
    public const ACTION_TO_CACHE_KEY_PREFIX_MAP = [
        'info' => self::CACHE_KEY_PREFIX_INFO,
        'organizations' => self::CACHE_KEY_PREFIX_ORGANIZATIONS,
        'organization' => self::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL,
        'appeal' => self::CACHE_KEY_PREFIX_APPEALS,
        'appealProjects' => self::CACHE_KEY_PREFIX_PROJECTS
    ];

    public static function resetCache(string $action, array $arguments = []): bool
    {
        switch ($action) {
            case self::CACHE_KEY_PREFIX_ORGANIZATIONS:
            case self::CACHE_KEY_PREFIX_INFO:
                return Cache::delete($action, self::CACHE_CONFIG);
            case self::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL:
            case self::CACHE_KEY_PREFIX_PROJECTS:
            case self::CACHE_KEY_PREFIX_APPEALS:
                return Cache::delete($action . '_' . md5(serialize(self::normalizeArguments($arguments))), self::CACHE_CONFIG);
        }
        return false;
    }

    public function initialize(): void
    {
        Configure::write('debug', false);
        parent::initialize();
        // this whole controller should be public, no authentication required
        $this->Authentication->allowUnauthenticated([$this->getRequest()->getParam('action')]);
        $this->loadModel('Organizations');
        $this->loadModel('Appeals');
        $this->loadModel('Projects');
        $this->loadComponent('Voting');
        $this->setResponse($this->getResponse()->withType('json'));
    }

    public function info()
    {
        $info = [
            'api' => [
                'version' => 1,
                'description' => [
                    'urls' => [
                        '/api/v1/info.json' => 'This help',
                        '/api/v1/organizations.json' => 'Get list of organizations',
                        '/api/v1/organization/{organization_id}/info.json' => 'Get detailed information about organization, including all past, present and future appeals, organization wide project categories and organization wide project states',
                        '/api/v1/organization/{organization_id}/appeal/{appeal_id}/info.json' => 'Get specific appeal info',
                        '/api/v1/organization/{organization_id}/appeal/{appeal_id}/projects.json' => 'Get all projects contained in specific appeal',
                        '/api/v1/organization/{organization_id}/appeal/{appeal_id}/project/{project_id}/add_vote' => 'This URL provides no data, only redirects user to appropriate location depending on current appeal phase',
                        '/api/v1/organization/{organization_id}/appeal/{appeal_id}/add_project' => 'This URL provides no data, only redirects user to appropriate location depending on current appeal phase',
                    ],
                    'notice' => 'This API provides organization specific data only for allowed domains, you might encounter CORS issues, if your domain is not authorized by organization',
                ],
                'software' => [
                    'license' => 'AGPLv3',
                    'homepage' => 'https://gitlab.com/otevrenamesta/participativni-rozpocet',
                ],
            ]
        ];

        return $this->getApiResponse(json_encode($info), false, '+1 years');
    }

    /**
     * @param string|null $stringBody
     * @param bool $isError
     * @param string $expires
     * @param string[] $corsDomains
     * @return MessageInterface
     */
    private function getApiResponse(?string $stringBody, bool $isError = false, $expires = '+1 minute', $corsDomains = ['*']): MessageInterface
    {
        return $this->getResponse()
            ->withType('json')
            ->withStringBody($stringBody)
            ->withExpires($expires)
            ->withStatus($isError ? 404 : 200)
            ->cors($this->getRequest())
            ->allowMethods(['GET'])
            ->allowOrigin($corsDomains)
            ->build();
    }

    public function organizations()
    {
        $cache_key = $this->getCacheKey(false);
        $organizations = Cache::read($cache_key, self::CACHE_CONFIG);
        if ($organizations === null) {
            $organizations = $this->Organizations->find('list', [
                'conditions' => [
                    Organization::FIELD_IS_ENABLED => true,
                ]
            ])->map(function (string $value, int $key) {
                return [
                    'id' => $key,
                    'name' => $value,
                    'url' => Router::url(['_name' => 'api_organization_info', 'organization_id' => $key])
                ];
            })->toList();
            $organizations = [
                'organizations' => $organizations,
                'expires' => strtotime(Cache::getConfig(self::CACHE_CONFIG)['duration'])
            ];
            Cache::write($cache_key, $organizations, self::CACHE_CONFIG);
        }

        return $this->getApiResponse(json_encode($organizations), false, $organizations['expires'], '*');
    }

    /**
     * @param bool $withArguments whether
     * @return string
     */
    private function getCacheKey(bool $withArguments = true): string
    {
        $requestAction = $this->getRequest()->getParam('action');
        $cacheKeyPrefix = self::ACTION_TO_CACHE_KEY_PREFIX_MAP[$requestAction] ?? sprintf('apiv1_unknown_%s', $requestAction);
        // add passed params checksum to key prefix if requested
        return $cacheKeyPrefix . ($withArguments ? '_' . md5(serialize(self::normalizeArguments($this->getRequest()->getParam('pass')))) : '');
    }

    public static function normalizeArguments(array $arguments = [])
    {
        $arguments = array_map('strval', $arguments);
        sort($arguments);
        return $arguments;
    }

    public function organization(int $organization_id)
    {
        $cache_key = $this->getCacheKey(true);
        list($data, $allowed_domains) = Cache::read($cache_key, self::CACHE_CONFIG);
        if ($data === null || $allowed_domains === null) {
            /** @var Organization $organization */
            $organization = $this->Organizations->find('all', [
                'conditions' => [
                    Organization::FIELD_ID => $organization_id,
                    Organization::FIELD_IS_ENABLED => true,
                ],
                'contain' => [
                    'Domains',
                    'Appeals',
                    'ProjectCategories',
                    'ProjectStatus'
                ]
            ])->first();
            if (empty($organization)) {
                return $this->getApiResponse(null, true);
            }
            $data = [
                'organization' => [
                    'id' => $organization->id,
                    'name' => $organization->name,
                    'appeals' => (new Collection($organization->appeals))->map(function (Appeal $value) use ($organization_id) {
                        return [
                            'id' => $value->id,
                            'year' => intval($value->year),
                            'info_link' => $value->info_link,
                            'last_update' => $value->modified->timestamp,
                            'url ' => Router::url(['_name' => 'api_appeal_info', 'organization_id' => $organization_id, 'appeal_id' => $value->id]),
                        ];
                    })->toList(),
                    'project_categories' => (new Collection($organization->project_categories))->map(function (ProjectCategory $category) {
                        return [
                            'id' => $category->id,
                            'name' => $category->name,
                        ];
                    })->combine('id', 'name')->toArray(),
                    'project_states' => (new Collection($organization->project_status))->map(function (ProjectStatus $status) {
                        return [
                            'id' => $status->id,
                            'name' => $status->name,
                        ];
                    })->combine('id', 'name')->toArray(),
                ],
            ];
            $allowed_domains = (new Collection($organization->domains))->filter(function (Domain $domain) {
                return $domain->is_enabled;
            })->extract('domain')->toList();
            Cache::write($cache_key, [$data, $allowed_domains], self::CACHE_CONFIG);
        }

        return $this->getApiResponse(json_encode($data), false, '+1 hour', $allowed_domains);
    }

    public function appeal(int $organization_id, int $appeal_id)
    {
        $cache_key = $this->getCacheKey(true);
        list($data, $allowed_domains) = Cache::read($cache_key, self::CACHE_CONFIG);

        if ($data === null || $allowed_domains === null) {
            $organization = $this->getOrganization($organization_id);
            if (empty($organization)) {
                return $this->getApiResponse(null, true);
            }
            $appeal = $this->getAppeal($organization->id, $appeal_id, [
                'Phases',
                'Projects',
                'Projects.ProjectStatus',
                'Projects.ProjectCategories',
                'Projects.Attachments',
                'Phases.IdentityProviders'
            ]);
            if (empty($appeal)) {
                return $this->getApiResponse(null, true);
            }
            $allowed_domains = $this->getAllowedDomains($organization->domains);
            $now = FrozenDate::now();
            $data = [
                'appeal' => [
                    'id' => $appeal->id,
                    'year' => intval($appeal->year),
                    'info_link' => $appeal->info_link,
                    'last_update' => $appeal->modified->timestamp,
                    'proposals' => [
                        'url' => Router::url(['_name' => 'api_appeal_propose_project', 'organization_id' => $organization_id, 'appeal_id' => $appeal_id]),
                        'description' => 'If current phase of appeal allows, redirect user to proposal UI, otherwise, redirect back with status code and message',
                        'currently_open' => (new Collection($appeal->phases))->filter(function (Phase $phase) use ($now) {
                            return $phase->date_start->lessThanOrEquals($now) && $phase->date_end->greaterThanOrEquals($now);
                        })->reduce(function (bool $accumulated, Phase $phase) {
                            return $accumulated || $phase->allows_proposals_submission;
                        }, false),
                    ],
                    'phases' => (new Collection($appeal->phases))->map(function (Phase $phase) use ($organization, $now) {
                        return [
                            'id' => $phase->id,
                            'currently_active' => $phase->date_start->lessThanOrEquals($now) && $phase->date_end->greaterThanOrEquals($now),
                            'allows_proposals_submission' => $phase->allows_proposals_submission,
                            'allows_voting' => $phase->allows_voting,
                            'date_start' => $phase->date_start->timestamp,
                            'date_end' => $phase->date_end->timestamp,
                            'minimum_votes_for_project_success' => $phase->minimum_votes_count,
                            'name' => $phase->name,
                            'description' => $phase->description,
                            'authentication_providers' => (new Collection($phase->identity_providers))->map(function (IdentityProvider $provider) {
                                return [
                                    'provider_name' => $provider->name
                                ];
                            })->toList()
                        ];
                    })->sortBy(function ($phase) {
                        return $phase['date_end'] - $phase['date_start'];
                    }, SORT_DESC)->toList(),
                    'projects_count' => count($appeal->projects),
                    'projects' => $this->getProjectsInfo($appeal->projects, $organization, $appeal)
                ]
            ];

            Cache::write($cache_key, [$data, $allowed_domains], self::CACHE_CONFIG);
        }

        return $this->getApiResponse(json_encode($data), false, '+1 hour', $allowed_domains);
    }

    public function appealProjects(int $organization_id, int $appeal_id)
    {
        $cache_key = $this->getCacheKey(true);
        list($data, $allowed_domains) = Cache::read($cache_key, self::CACHE_CONFIG);

        if ($data === null || $allowed_domains === null) {
            $organization = $this->getOrganization($organization_id);
            if (empty($organization)) {
                return $this->getApiResponse(null, true);
            }
            $appeal = $this->getAppeal($organization->id, $appeal_id, [
                'Projects',
                'Projects.ProjectStatus',
                'Projects.ProjectCategories',
                'Projects.Attachments',
            ]);
            if (empty($appeal)) {
                return $this->getApiResponse(null, true);
            }
            $allowed_domains = $this->getAllowedDomains($organization->domains);
            $data = [
                'projects' => $this->getProjectsInfo($appeal->projects, $organization, $appeal),
            ];

            Cache::write($cache_key, [$data, $allowed_domains], self::CACHE_CONFIG);
        }

        return $this->getApiResponse(json_encode($data), false, '+1 hour', $allowed_domains);
    }

    public function projectAttachmentDownload(int $organization_id, int $appeal_id, int $project_id, int $attachment_id)
    {
        $organization = $this->getOrganization($organization_id);
        if (empty($organization)) {
            return $this->getApiResponse(null, true);
        }

        $appeal = $this->getAppeal($organization->id, $appeal_id);
        if (empty($appeal)) {
            return $this->getApiResponse(null, true);
        }

        $project = $this->Projects->getWithAppeal($project_id, $appeal->id, ['Attachments', 'ProjectStatus']);
        if (empty($project) || !$project->project_status->is_public) {
            return $this->getApiResponse(null, true);
        }

        $attachment = $project->getAttachmentById($attachment_id);
        $path = $attachment ? $attachment->getRealPath() : null;
        if (empty($attachment) || !is_file($path)) {
            return $this->getApiResponse(null, true);
        }

        return $this->getResponse()->withType($attachment->getMimeType())->withFile($path, [
            'download' => boolval($this->getRequest()->getQuery('download', true)),
            'name' => urlencode($attachment->original_filename)
        ]);
    }

    /**
     * @param int $organization_id
     * @param array $contain
     * @return Organization|EntityInterface|null
     */
    private function getOrganization(int $organization_id, array $contain = ['Domains',]): ?Organization
    {
        return $this->Organizations->find('all', [
            'conditions' => [
                Organization::FIELD_ID => $organization_id,
                Organization::FIELD_IS_ENABLED => true,
            ],
            'contain' => $contain,
        ])->first();
    }

    /**
     * @param int $organization_id
     * @param int $appeal_id
     * @param array $contain
     * @return Appeal|EntityInterface|null
     */
    private function getAppeal(int $organization_id, int $appeal_id, array $contain = []): ?Appeal
    {
        return $this->Appeals->find('all', [
            'conditions' => [
                Appeal::FIELD_ORGANIZATION_ID => $organization_id,
                Appeal::FIELD_ID => $appeal_id
            ],
            'contain' => $contain,
        ])->first();
    }

    private function getAllowedDomains(iterable $domains)
    {
        return (new Collection($domains))->filter(function (Domain $domain) {
            return $domain->is_enabled;
        })->extract('domain')->toList();
    }

    /**
     * Format projects into API output array
     *
     * @param Project[] $projects
     * @param Organization $organization
     * @param Appeal $appeal
     * @return iterable
     */
    private function getProjectsInfo(iterable $projects, Organization $organization, Appeal $appeal): iterable
    {
        if (!empty($projects)) {
            $this->Projects->loadInto($projects, [
                'ProjectStatus',
                'ProjectCategories',
                'Attachments',
                'ProjectLogs',
                'ProjectBudgetItems',
                'Contacts',
                'Votes'
            ]);
        }
        return (new Collection($projects))->map(function (Project $project) use ($organization, $appeal) {
            $project_data = [
                'id' => $project->id,
                'is_public' => $project->project_status->is_public,
                'is_currently_votable' => $project->project_status->is_voting_enabled,
                'current_status_text' => $project->project_status->name,
                'current_status_id' => $project->project_status_id,
            ];
            $votes = [];
            foreach ($project->votes as $vote) {
                $key = $vote->phase_id;
                if (!isset($votes[$key])) {
                    $votes[$key] = 0;
                }
                $votes[$key]++;
            }
            $project_data['votes_overall'] = array_sum($votes);
            $project_data['votes'] = $votes;
            if ($project_data['is_public']) {
                $title_image = null;
                if ($project->title_image_id > 0) {
                    $title_image = Router::url([
                        '_name' => 'api_project_download_attachment',
                        'organization_id' => $organization->id,
                        'appeal_id' => $appeal->id,
                        'project_id' => $project->id,
                        'attachment_id' => $project->title_image_id,
                        '?' => ['download' => false],
                    ]);
                }

                $proposed_price_total = 0;
                $approved_price_total = 0;
                $budget_items_proposed = [];
                $budget_items_approved = [];
                foreach ($project->project_budget_items as $item) {
                    $itemdata = [
                        'description' => $item->description,
                        'item_price' => $item->item_price,
                        'item_count' => $item->item_count,
                        'total_price' => $item->total_price,
                        'currency' => 'CZK'
                    ];
                    if ($item->is_final) {
                        $approved_price_total += $item->total_price;
                        $budget_items_approved[] = $itemdata;
                    } else {
                        $proposed_price_total += $item->total_price;
                        $budget_items_proposed[] = $itemdata;
                    }
                }

                $project_data += [
                    'name' => $project->name,
                    'description' => $project->description,
                    'address' => $project->address,
                    'image' => $title_image,
                    'proposed_price_total' => $proposed_price_total,
                    'approved_price_total' => $approved_price_total,
                    'proposer' => [
                        'name' => $project->contact->name
                    ],
                    'budget' => [
                        'proposed' => $budget_items_proposed,
                        'approved' => $budget_items_approved,
                    ],
                    'gps' => [
                        'latitude' => $project->gps_latitude,
                        'longitude' => $project->gps_longitude,
                    ],
                    'categories' => (new Collection($project->project_categories))->combine('id', 'name')->toArray(),
                    'attachments' => (new Collection($project->attachments))->map(function (Attachment $attachment) use ($organization, $appeal) {
                        return [
                            'type' => AttachmentType::getLabel($attachment->attachment_type_id),
                            'type_id' => $attachment->attachment_type_id,
                            'name' => $attachment->title,
                            'url' => Router::url(['_name' => 'api_project_download_attachment', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $attachment->project_id, 'attachment_id' => $attachment->id]),
                        ];
                    })->toList(),
                    'project_history' => (new Collection($project->getLogsByType(ProjectLogType::TYPE_PROJECT_HISTORY)))->map(function (ProjectLog $log) {
                        return [
                            'title' => $log->title,
                            'text' => $log->description,
                            'timestamp' => $log->date_when->timestamp,
                        ];
                    })->toList(),
                    'feasibility_statements' => (new Collection($project->getLogsByType(ProjectLogType::TYPE_FEASIBILITY_STATEMENT)))->map(function (ProjectLog $log) {
                        return [
                            'title' => $log->title,
                            'text' => $log->description,
                            'timestamp' => $log->date_when->timestamp,
                        ];
                    })->toList(),
                ];
            }
            if ($project_data['is_currently_votable']) {
                $project_data += [
                    'voting' => [
                        'description' => 'If current phase of appeal allows, redirect user to voting UI, otherwise, redirect back with status code and message',
                        'url' => Router::url(['_name' => 'api_project_add_vote', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id]),
                    ]
                ];
            }
            return $project_data;
        })->toList();
    }

    public function projectAddVote(int $organization_id, int $appeal_id, int $project_id)
    {
        $projectIsVotable = $this->Voting->isProjectVotable($organization_id, $appeal_id, $project_id);
        $canProceed = $projectIsVotable === VotingComponent::VOTE_CAN_PROCEED;

        if (!$canProceed) {
            $this->Voting->persistAbort($projectIsVotable);
            $this->redirect(['_name' => 'voting_final_status']);
        } else {
            $this->Voting->persist([
                VotingComponent::PERSIST_REDIRECT_URL => $this->Voting->getAuthorizedRedirect(),
            ]);
            $this->Voting->persistSuccess();
            $this->redirect(['_name' => 'voting_choose_provider']);
        }
    }
}
