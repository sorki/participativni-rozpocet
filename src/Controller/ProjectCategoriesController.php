<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\ProjectCategory;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\ProjectCategoriesTable;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read ProjectCategoriesTable $ProjectCategories
 */
class ProjectCategoriesController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('ProjectCategories');
    }

    public function index(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $categories = $this->ProjectCategories->find('all', [
            'conditions' => [
                ProjectCategory::FIELD_ORGANIZATION_ID => $organization->id
            ],
        ])->toArray();
        $this->set(compact('organization', 'categories'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function addModify(int $organization_id, int $category_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $category = $category_id > 0 ? $this->ProjectCategories->getWithOrganization($category_id, $organization->id) :
            $this->ProjectCategories->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $category = $this->ProjectCategories->patchEntity($category, $this->getRequest()->getData() + [
                    ProjectCategory::FIELD_ORGANIZATION_ID => $organization->id
                ]
            );

            if ($this->ProjectCategories->save($category)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization->id]);
                $this->redirect(['action' => 'index', 'organization_id' => $organization->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'category'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id], __('Kategorie projektů') => ['action' => 'index', 'organization_id' => $organization->id]]);
    }

    public function delete(int $organization_id, int $category_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $category = $this->ProjectCategories->getWithOrganization($category_id, $organization->id);
        if ($this->ProjectCategories->delete($category)) {
            ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization->id]);
            $this->Flash->success(self::FLASH_SUCCESS_SAVE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_SAVE);
        }
        return $this->redirect($this->referer());
    }

}
