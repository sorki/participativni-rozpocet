<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\ProjectStatus;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\ProjectStatusTable;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read ProjectStatusTable $ProjectStatus
 */
class ProjectStatusController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('ProjectStatus');
    }

    public function index(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $status = $this->ProjectStatus->find('all', [
            'conditions' => [
                ProjectStatus::FIELD_ORGANIZATION_ID => $organization->id
            ],
        ])->toArray();
        $this->set(compact('organization', 'status'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function addModify(int $organization_id, int $status_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $status = $status_id > 0 ? $this->ProjectStatus->getWithOrganization($status_id, $organization->id) :
            $this->ProjectStatus->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $status = $this->ProjectStatus->patchEntity($status, $this->getRequest()->getData() + [
                    ProjectStatus::FIELD_ORGANIZATION_ID => $organization->id
                ]
            );

            if ($this->ProjectStatus->save($status)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization->id]);
                $this->redirect($this->retrieveReferer(['action' => 'index', 'organization_id' => $organization->id]));
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'status'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id], __('Stavy projektů') => ['action' => 'index', 'organization_id' => $organization->id]]);
        $this->persistReferer();
    }

    public function delete(int $organization_id, int $status_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $status = $this->ProjectStatus->getWithOrganization($status_id, $organization->id);
        if ($this->ProjectStatus->delete($status)) {
            ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization->id]);
            $this->Flash->success(self::FLASH_SUCCESS_SAVE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_SAVE);
        }
        return $this->redirect($this->referer());
    }

}
