<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Organization;
use App\Model\Table\OrganizationsTable;

/**
 * @property-read OrganizationsTable $Organizations
 */
class OrganizationsController extends AppController
{

    public function index()
    {
        $this->set('organizations', $this->Organizations->findAllWithUser($this->getCurrentUserId()));
    }

    public function addModify(?int $organization_id = null)
    {
        $organization = $organization_id > 0 ? $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId()) : $this->Organizations->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $organization = $this->Organizations->patchEntity($organization, $this->getRequest()->getData() + [
                    Organization::FIELD_ADMIN_USER_ID => $organization->isNew() ? $this->getCurrentUserId() : $organization->admin_user_id
                ]
            );

            if ($this->Organizations->save($organization)) {
                $this->Flash->success(__(self::FLASH_SUCCESS_SAVE));
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATIONS);
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization_id]);
                $this->redirect(['action' => 'detail', 'organization_id' => $organization->id]);
            } else {
                $this->Flash->error(__(self::FLASH_FAILURE_SAVE));
            }
        }

        $this->set(compact('organization'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function detail(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId(), ['ProjectCategories', 'ProjectStatus', 'Domains', 'Appeals']);

        $this->set(compact('organization'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations']);
    }

    public function delete(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        if ($this->Organizations->delete($organization)) {
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }
        return $this->redirect(['action' => 'index']);
    }

}
