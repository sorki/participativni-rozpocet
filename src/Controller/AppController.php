<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Model\Entity\User;
use ArrayAccess;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Controller\Controller;
use Closure;
use Exception;
use ReflectionException;
use ReflectionFunction;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 * @property AuthenticationComponent $Authentication
 */
class AppController extends Controller
{
    const FLASH_SUCCESS_SAVE = 'Uloženo úspěšně';
    const FLASH_FAILURE_SAVE = 'Formulář obsahuje chyby';
    const FLASH_SUCCESS_DELETE = 'Smazáno úspěšně';
    const FLASH_FAILURE_DELETE = 'Nebylo možné smazat';

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     * @throws Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('FormProtection');
        $this->loadComponent('Authentication.Authentication');
    }

    public function getCurrentUserId(): int
    {
        if (!$this->Authentication || !$this->Authentication->getIdentity()) {
            return 0;
        }
        return intval($this->Authentication->getIdentity()->getIdentifier());
    }

    /**
     * @return null|User|ArrayAccess|array
     */
    public function getCurrentUser(): ?ArrayAccess
    {
        if (!$this->Authentication || !$this->Authentication->getIdentity()) {
            return null;
        }
        return $this->Authentication->getIdentity()->getOriginalData();
    }


    /**
     * Allows action methods to have strong-typed arguments
     *
     * @param Closure $action
     * @param array $args
     * @throws ReflectionException
     */
    public function invokeAction(Closure $action, array $args): void
    {
        $reflection = new ReflectionFunction($action);
        $passedArgs = [];
        $counter = 0;
        foreach ($reflection->getParameters() as $reflectionParameter) {
            if (!$reflectionParameter->hasType()) {
                $passedArgs[] = $args[$counter];
            } elseif (!isset($args[$counter]) && $reflectionParameter->allowsNull()) {
                $passedArgs[] = null;
            } else {
                switch ($reflectionParameter->getType()->getName()) {
                    default:
                        dump(__METHOD__, $reflectionParameter->getType()->getName());
                        die();
                    case "int":
                        $passedArgs[] = isset($args[$counter]) ? intval($args[$counter]) : 0;
                        break;
                    case "float":
                        $passedArgs[] = isset($args[$counter]) ? floatval($args[$counter]) : 0;
                        break;
                    case "string":
                        $passedArgs[] = isset($args[$counter]) ? strval($args[$counter]) : "";
                        break;
                    case "bool":
                        $passedArgs[] = isset($args[$counter]) ? boolval($args[$counter]) : false;
                        break;
                }
            }
            $counter++;
        }

        parent::invokeAction($action, $passedArgs);
    }

    protected function persistReferer()
    {
        $this->getRequest()->getSession()->write('refer.' . md5($this->getRequest()->getRequestTarget()), $this->referer());
    }

    protected function retrieveReferer($default = null)
    {
        return $this->getRequest()->getSession()->read('refer.' . md5($this->getRequest()->getRequestTarget())) ?? $default ?? $this->referer();
    }
}
