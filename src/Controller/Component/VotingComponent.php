<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Controller\ApiController;
use App\IdentityProvider\IdentityProviderInterface;
use App\IdentityProvider\ManagerUsersProvider;
use App\IdentityProvider\NIAIdentityProvider;
use App\IdentityProvider\RegisteredUsersProvider;
use App\IdentityProvider\UniqueIPProvider;
use App\IdentityProvider\VotingResult;
use App\Model\Entity\Appeal;
use App\Model\Entity\IdentityProvider;
use App\Model\Entity\Organization;
use App\Model\Entity\Phase;
use App\Model\Entity\Project;
use App\Model\Entity\Vote;
use App\Model\Table\AppealsTable;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\ProjectsTable;
use App\Model\Table\VotesTable;
use Cake\Controller\Component;
use Cake\Datasource\EntityInterface;
use Cake\Routing\Router;
use Error;
use InvalidArgumentException;
use Laminas\Diactoros\Uri;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read AppealsTable $Appeals
 * @property-read ProjectsTable $Projects
 * @property-read VotesTable $Votes
 */
class VotingComponent extends Component
{
    public const ERROR_INCORRECT_CALL_ORDER = 'Špatné pořadí volání metod VotingComponent';
    public const ERROR_UNKNOWN_PROVIDER = 'Neznámý poskytovatel hlasovacích identit';

    public const PERSIST_MESSAGE = 'voting.message';
    public const PERSIST_REDIRECT_URL = 'voting.redirect';
    public const PERSIST_IS_ERROR = 'voting.is_error';
    public const PERSIST_ORGANIZATION = 'voting.organization';
    public const PERSIST_PROJECT = 'voting.project';
    public const PERSIST_PROVIDER = 'voting.identity_provider';
    public const PERSIST_PHASE = 'voting.phase';
    public const PERSIST_APPEAL = 'voting.appeal';
    public const PERSIST_OTP = 'voting.otp';

    // general error, incorrect org/appeal/project id for example
    public const VOTE_INCORRECT_PARAMS = 1000;
    // organization is not currently public/enabled
    public const VOTE_ORGANIZATION_DISABLED = 1001;
    // phase does not allow voting
    public const VOTE_PHASE_NOT_VOTABLE = 1002;
    // non-public projects cannot be voted for
    public const VOTE_PROJECT_NOT_PUBLIC = 1003;
    // phase does not allow voting
    public const VOTE_PROJECT_NOT_VOTABLE = 1004;
    // phase is votable but has no vote providers
    public const VOTE_PHASE_INCORRECT_SETTINGS = 1005;
    // return states for vote proceeding
    public const VOTE_SUCCESS = 999;
    // identity already voted for this project
    public const VOTE_ALREADY_VOTED = 2001;
    // when selected voting provider failed to authenticate user
    public const VOTE_FAILED_AUTH = 2002;
    // positive return status for `canVoteProject`
    public const VOTE_CAN_PROCEED = 998;

    private ?Organization $_organization = null;
    private ?Appeal $_appeal = null;
    private ?Project $_project = null;
    private ?Phase $_phase = null;
    private ?IdentityProviderInterface $_provider = null;

    private bool $_organizationLoaded = false;
    private bool $_appealLoaded = false;
    private bool $_projectLoaded = false;
    private bool $_phaseLoaded = false;

    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->getController()->loadModel('Organizations');
        $this->getController()->loadModel('Appeals');
        $this->getController()->loadModel('Projects');
        $this->getController()->loadModel('Votes');
    }

    public function __get(string $name)
    {
        $parentFirst = parent::__get($name);
        if ($parentFirst === null) {
            $controller = $this->getController();
            if (isset($controller->{$name})) {
                return $controller->{$name};
            }
        }
        return $parentFirst;
    }

    public function getErrorMessage(int $error_code = 0): ?string
    {
        switch ($error_code) {
            default:
            case self::VOTE_CAN_PROCEED:
            case self::VOTE_SUCCESS:
                return null;
            case self::VOTE_PHASE_INCORRECT_SETTINGS:
                return __('Není nastaven žádný poskytovatel hlasů');
            case self::VOTE_PHASE_NOT_VOTABLE:
                return __('Aktuální fáze nedovoluje hlasování');
            case self::VOTE_PROJECT_NOT_PUBLIC:
                return __('Projekt není veřejný, takže jej nelze podpořit hlasováním');
            case self::VOTE_PROJECT_NOT_VOTABLE:
                return __('Aktuální stav projektu nedovoluje hlasování');
            case self::VOTE_ORGANIZATION_DISABLED:
                return __('Organizace je aktuálně zakázána, není možné hlasovat');
            case self::VOTE_INCORRECT_PARAMS:
                return __('Špatné parametry pro volání');
            case self::VOTE_ALREADY_VOTED:
                return __('Pro tento projekt jste již hlasovali');
            case self::VOTE_FAILED_AUTH:
                return __('Ověření selhalo, hlas nebyl přidán');
        }
    }

    /**
     * Returns result code whether requested project/appeal/organization is currently votable
     *
     * @param int $organization_id
     * @param int $appeal_id
     * @param int $project_id
     * @return int result code (error or success)
     */
    public function isProjectVotable(int $organization_id, int $appeal_id, int $project_id): int
    {
        $organization = $this->getOrganization($organization_id);
        $appeal = $this->getAppeal($appeal_id, $organization);
        $project = $this->getProject($project_id, $appeal);

        if (empty($organization) || empty($appeal) || empty($project)) {
            return self::VOTE_INCORRECT_PARAMS;
        }

        if (!$organization->is_enabled) {
            return self::VOTE_ORGANIZATION_DISABLED;
        }

        if (!$project->project_status->is_public) {
            return self::VOTE_PROJECT_NOT_PUBLIC;
        }

        if (!$project->project_status->is_voting_enabled) {
            return self::VOTE_PROJECT_NOT_VOTABLE;
        }

        $currentVotablePhase = $this->getCurrentVotablePhase();

        if (!$currentVotablePhase) {
            return self::VOTE_PHASE_NOT_VOTABLE;
        }

        if (empty($currentVotablePhase->identity_providers)) {
            return self::VOTE_PHASE_INCORRECT_SETTINGS;
        }

        return self::VOTE_CAN_PROCEED;
    }

    /**
     * Returns null if Organization with requested ID is not found
     *
     * @param int|null $organization_id
     * @return Organization|null
     */
    public function getOrganization(?int $organization_id = null): ?Organization
    {
        if (!$this->_organization && $organization_id > 0) {
            $this->_organizationLoaded = true;
            $this->_organization = $this->Organizations->find('all', [
                'conditions' => [
                    'Organizations.' . Organization::FIELD_ID => $organization_id
                ]
            ])->contain(['Domains'])->first();
        }
        if ($organization_id < 1 && !$this->_organizationLoaded) {
            throw new InvalidArgumentException(self::ERROR_INCORRECT_CALL_ORDER);
        }
        return $this->_organization;
    }

    /**
     * Returns null if Appeal with requested ID/Organization.id is not found
     *
     * @param int|null $appeal_id
     * @param Organization|null $organization
     * @return Appeal|null
     */
    public function getAppeal(?int $appeal_id = null, ?Organization $organization = null): ?Appeal
    {
        if (!$this->_appeal && $appeal_id > 0 && $organization instanceof Organization) {
            $this->_appealLoaded = true;
            $this->_appeal = $this->Appeals->find('all', [
                'conditions' => [
                    'Appeals.' . Appeal::FIELD_ID => $appeal_id,
                    'Appeals.' . Appeal::FIELD_ORGANIZATION_ID => $organization->id,
                ]
            ])->contain(['Phases', 'Phases.IdentityProviders'])->first();
        }
        if ($appeal_id < 1 && !$this->_appealLoaded) {
            throw new InvalidArgumentException(self::ERROR_INCORRECT_CALL_ORDER);
        }
        return $this->_appeal;
    }

    /**
     * Returns null if project with requested ID/Appeal.id is not found
     *
     * @param int|null $project_id
     * @param Appeal|null $appeal
     * @return Project|array|EntityInterface|null
     */
    public function getProject(?int $project_id = null, ?Appeal $appeal = null)
    {
        if (!$this->_project && $project_id > 0 && $appeal instanceof Appeal) {
            $this->_projectLoaded = true;
            $this->_project = $this->Projects->find('all', [
                'conditions' => [
                    'Projects.' . Project::FIELD_ID => $project_id,
                    'Projects.' . Project::FIELD_APPEAL_ID => $appeal->id
                ]
            ])->contain(['ProjectStatus'])->first();
        }
        if ($project_id < 1 && !$this->_projectLoaded) {
            throw new InvalidArgumentException(self::ERROR_INCORRECT_CALL_ORDER);
        }
        return $this->_project;
    }

    public function getCurrentVotablePhase(): ?Phase
    {
        if (!$this->_phase && !$this->_phaseLoaded && $this->getAppeal() instanceof Appeal) {
            $this->_phaseLoaded = true;
            $currentPhases = $this->getAppeal()->getCurrentPhases();
            foreach ($currentPhases as $phase) {
                if ($phase->allows_voting) {
                    $this->_phase = $phase;
                    break;
                }
            }
        }
        if (!$this->_phase && !$this->_phaseLoaded) {
            throw new InvalidArgumentException(self::ERROR_INCORRECT_CALL_ORDER);
        }
        return $this->_phase;
    }

    /**
     * Get referer, if domain is authorized by organization
     * or public projects gallery if referer is local/not defined/not authorized
     *
     * @return string
     */
    public function getAuthorizedRedirect(): string
    {
        $requestReferer = $this->getController()->getRequest()->referer();

        $defaultRedirect = null;
        if ($this->getOrganization() instanceof Organization) {
            $defaultRedirect = Router::url([
                'organization_id' => $this->getOrganization()->id,
                '_name' => 'public_organization_detail'
            ]);
        } else {
            $defaultRedirect = Router::url(['_name' => 'public_gallery']);
        }

        if (empty($requestReferer)) {
            return $defaultRedirect;
        }

        $domain = (new Uri($requestReferer))->getHost();
        $organization = $this->getOrganization();

        if ($organization instanceof Organization && $organization->isDomainAuthorized($domain)) {
            return $requestReferer;
        }

        return $defaultRedirect;
    }

    public function persist(array $persistedData = [])
    {
        foreach ($persistedData as $key => $value) {
            $this->getController()->getRequest()->getSession()->write($key, $value);
        }
    }

    /**
     * @param string $key
     * @param bool $consume
     * @param mixed $default if key is not found, return this default
     * @return mixed|null
     */
    public function retrieve(string $key, bool $consume = false, $default = null)
    {
        return $this->getController()->getRequest()->getSession()->{$consume ? 'consume' : 'read'}($key, $default);
    }

    /**
     * Returns array of providers (provider_id => provider_name) for currently active voting phase
     *
     * @return array
     */
    public function getEnabledVotingProviders(): array
    {
        $providers = [];
        if ($this->getCurrentVotablePhase() instanceof Phase) {
            foreach ($this->getCurrentVotablePhase()->identity_providers as $provider) {
                $providers[$provider->id] = $provider->name;
            }
        }
        return $providers;
    }

    /**
     * Ensures that one-time-password (OTP) is stored with current persistence
     * If optional $otp provided, will check against stored, and return bool or throw Error, if they do not match
     * On successfull check, the new OTP will be generated
     *
     * @param string|null $otp
     * @param bool $throw
     * @return bool
     */
    public function ensureOTP(string $otp = null, bool $throw = false): bool
    {
        $stored_otp = $this->retrieve(self::PERSIST_OTP);
        if (!empty($otp)) {
            if (strcmp($otp, $stored_otp) !== 0) {
                if ($throw) {
                    throw new Error();
                }
                return false;
            }
            $stored_otp = null;
        }
        if (empty($stored_otp)) {
            $stored_otp = random_str('alphanum', 16);
            $this->persist([self::PERSIST_OTP => $stored_otp]);
        }
        return true;
    }

    /**
     * Gets current one-time-password (OTP) from persistence
     * If no OTP present in persistence, this call will ensure one is generated and persisted
     *
     * @return string
     */
    public function getOTP(): string
    {
        $this->ensureOTP();
        return $this->retrieve(self::PERSIST_OTP);
    }

    public function persistAbort(int $reason)
    {
        $this->persist([
            self::PERSIST_MESSAGE => $this->getErrorMessage($reason),
            self::PERSIST_REDIRECT_URL => $this->getAuthorizedRedirect(),
            self::PERSIST_IS_ERROR => true,
        ]);
    }

    public function persistSuccess()
    {
        $this->persist([
            self::PERSIST_ORGANIZATION => $this->getOrganization()->id,
            self::PERSIST_PROJECT => $this->getProject()->id,
            self::PERSIST_APPEAL => $this->getAppeal()->id,
            self::PERSIST_PHASE => $this->getCurrentVotablePhase()->id,
        ]);
        $this->ensureOTP();
    }

    public function restoreFromPersistence()
    {
        $this->isProjectVotable(
            $this->retrieve(self::PERSIST_ORGANIZATION, true),
            $this->retrieve(self::PERSIST_APPEAL, true),
            $this->retrieve(self::PERSIST_PROJECT, true),
        );
        assert($this->getCurrentVotablePhase()->id === $this->retrieve(self::PERSIST_PHASE));
    }

    public function getInitialVotingResult(int $providerId)
    {
        return new VotingResult(
            $providerId,
            $this->getProject()->id,
            $this->getCurrentVotablePhase()->id
        );
    }

    public function getIdentityProvider(?int $provider_id = null): IdentityProviderInterface
    {
        if ($this->_provider === null) {
            switch ($provider_id) {
                default:
                    throw new Error(self::ERROR_UNKNOWN_PROVIDER);
                case IdentityProvider::PROVIDER_UNIQUE_IP:
                    $this->_provider = new UniqueIPProvider();
                    break;
                case IdentityProvider::PROVIDER_REGISTERED_USERS:
                    $this->_provider = new RegisteredUsersProvider();
                    break;
                case IdentityProvider::PROVIDER_PORTAL_MANAGERS:
                    $this->_provider = new ManagerUsersProvider();
                    break;
                case IdentityProvider::PROVIDER_NIA:
                    $this->_provider = new NIAIdentityProvider();
                    break;
            }
            $this->_provider->startup($this);
        }
        if ($this->_provider === null && $provider_id < 1) {
            throw new InvalidArgumentException(self::ERROR_INCORRECT_CALL_ORDER);
        }
        return $this->_provider;
    }

    public function persistVote(VotingResult $result): VotingResult
    {
        $originalResultCode = $result->getResultCode();
        $result->setResultCode(self::VOTE_ALREADY_VOTED);

        $voteEntity = $this->Votes->findOrCreate([
            Vote::FIELD_PROJECT_ID => $result->getProjectId(),
            Vote::FIELD_PHASE_ID => $result->getPhaseId(),
            Vote::FIELD_IDENTITY_KEY => $result->getUniqueIdentityKey(),
            Vote::FIELD_IDENTITY_PROVIDER_ID => $result->getIdentityProviderId(),
        ], function (Vote $vote) use ($result, $originalResultCode) {
            $vote->identity_data = $result->getFullIdentityData();
            $result->setResultCode($originalResultCode);
        });

        $result->setResultVote($voteEntity);
        if ($result->getResultCode() !== self::VOTE_ALREADY_VOTED) {
            $result->setResultCode(VotingComponent::VOTE_SUCCESS);
        }

        ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_APPEALS, [$this->getOrganization()->id, $this->getAppeal()->id]);
        ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_PROJECTS, [$this->getOrganization()->id, $this->getAppeal()->id]);

        return $result;
    }

}
