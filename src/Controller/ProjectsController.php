<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Appeal;
use App\Model\Entity\AttachmentType;
use App\Model\Entity\Organization;
use App\Model\Entity\ProjectLogType;
use App\Model\Table\AppealsTable;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\ProjectsTable;
use Laminas\Diactoros\UploadedFile;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read AppealsTable $Appeals
 * @property-read ProjectsTable $Projects
 */
class ProjectsController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('Appeals');
    }

    private function resetAppropriateCache(Organization $organization, Appeal $appeal)
    {
        ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_PROJECTS, [$organization->id, $appeal->id]);
        ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_APPEALS, [$organization->id, $appeal->id]);
    }

    /**
     * Organization manager interface to add/modify project basic info
     *
     * @param int $organization_id
     * @param int $appeal_id
     * @param int|null $project_id
     */
    public function addModify(int $organization_id, int $appeal_id, ?int $project_id = 0)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        $project = $project_id > 0 ? $this->Projects->getWithAppeal($project_id, $appeal->id, [
            'Contacts', 'ProjectCategories'
        ]) : $this->Projects->newEmptyEntity();

        $projectStates = $this->Projects->ProjectStatus->findByOrganizationId($organization->id)->find('list')->toArray();
        $projectCategories = $this->Projects->ProjectCategories->findByOrganizationId($organization->id)->find('list')->toArray();

        if (empty($projectStates)) {
            $this->Flash->error(__('Musíte vytvořit alespoň 1 stav, do kterého projekt zařadíte'));
            $this->redirect(['_name' => 'project_status_add', 'organization_id' => $organization_id]);

            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $project = $this->Projects->patchEntity($project, $this->getRequest()->getData() + [
                    'appeal_id' => $appeal->id,
                ]
            );
            if ($this->Projects->save($project)) {
                $this->resetAppropriateCache($organization, $appeal);
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['action' => 'detail', 'project_id' => $project->id, 'appeal_id' => $project->appeal_id, 'organization_id' => $organization->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'appeal', 'project', 'projectCategories', 'projectStates'));
        $this->set('crumbs', [
            __('Moje organizace') => 'my_organizations',
            $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id],
            sprintf("%s %d", __('Ročník'), $appeal->year) => ['_name' => 'appeal_detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id]
        ]);

    }

    public function detail(int $organization_id, int $appeal_id, int $project_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        $project = $this->Projects->getWithAppeal($project_id, $appeal->id, ['Appeals', 'Contacts', 'ProjectStatus', 'Attachments', 'ProjectBudgetItems', 'ProjectLogs', 'ProjectCategories', 'Votes']);

        $emptyLog = $this->Projects->ProjectLogs->newEmptyEntity();
        $emptyBudgetItem = $this->Projects->ProjectBudgetItems->newEmptyEntity();
        $emptyAttachment = $this->Projects->Attachments->newEmptyEntity();
        $attachmentTypes = $this->Projects->Attachments->AttachmentTypes->find('list');

        $this->set(compact('organization', 'appeal', 'project', 'emptyLog', 'emptyBudgetItem', 'emptyAttachment', 'attachmentTypes'));
        $this->set('crumbs', [
            __('Moje organizace') => 'my_organizations',
            $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id],
            sprintf("%s %d", __('Ročník'), $appeal->year) => ['_name' => 'appeal_detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id]
        ]);
    }

    public function addModifyLog(int $organization_id, int $appeal_id, int $project_id, int $log_type_id, ?int $log_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        $project = $this->Projects->getWithAppeal($project_id, $appeal->id);
        $log = $log_id > 0 ? $this->Projects->ProjectLogs->getWithProject($log_id, $project->id) : $this->Projects->ProjectLogs->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $log = $this->Projects->ProjectLogs->patchEntity($log, $this->getRequest()->getData() + [
                    'project_id' => $project->id,
                    'project_log_type_id' => $log_type_id,
                ]
            );
            if ($this->Projects->ProjectLogs->save($log)) {
                $this->resetAppropriateCache($organization, $appeal);
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['action' => 'detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'appeal', 'project', 'log'));
        $this->set('crumbs', [
            __('Moje organizace') => 'my_organizations',
            $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id],
            sprintf("%s %d", __('Ročník'), $appeal->year) => ['_name' => 'appeal_detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id],
            $project->name => ['action' => 'detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id],
            ProjectLogType::getLabel($log_type_id) => '#',
        ]);
    }

    public function deleteLog(int $organization_id, int $appeal_id, int $project_id, int $log_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        $project = $this->Projects->getWithAppeal($project_id, $appeal->id);
        $log = $this->Projects->ProjectLogs->getWithProject($log_id, $project->id);
        if ($this->Projects->ProjectLogs->delete($log)) {
            $this->resetAppropriateCache($organization, $appeal);
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }
        $this->redirect($this->referer());
    }

    public function addModifyBudgetItem(int $organization_id, int $appeal_id, int $project_id, ?int $item_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        $project = $this->Projects->getWithAppeal($project_id, $appeal->id);
        $item = $item_id > 0 ? $this->Projects->ProjectBudgetItems->getWithProject($item_id, $project->id) : $this->Projects->ProjectBudgetItems->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $item = $this->Projects->ProjectBudgetItems->patchEntity($item, $this->getRequest()->getData() + [
                    'project_id' => $project->id
                ]
            );
            $item->total_price = intval($item->item_count) * intval($item->item_price);
            if ($this->Projects->ProjectBudgetItems->save($item)) {
                $this->resetAppropriateCache($organization, $appeal);
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                if ($this->getRequest()->getData('SAVE_AND_CREATE_NEW') !== null) {
                    $this->redirect($this->getRequest()->getRequestTarget());
                } else {
                    $this->redirect(['action' => 'detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id]);
                }
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'appeal', 'project', 'item'));
        $this->set('crumbs', [
            __('Moje organizace') => 'my_organizations',
            $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id],
            sprintf("%s %d", __('Ročník'), $appeal->year) => ['_name' => 'appeal_detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id],
            $project->name => ['action' => 'detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id],
        ]);
    }

    public function deleteBudgetItem(int $organization_id, int $appeal_id, int $project_id, int $item_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        $project = $this->Projects->getWithAppeal($project_id, $appeal->id);
        $item = $this->Projects->ProjectBudgetItems->getWithProject($item_id, $project->id);
        if ($this->Projects->ProjectBudgetItems->delete($item)) {
            $this->resetAppropriateCache($organization, $appeal);
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }
        $this->redirect($this->referer());
    }


    public function addModifyAttachment(int $organization_id, int $appeal_id, int $project_id, ?int $attachment_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        $project = $this->Projects->getWithAppeal($project_id, $appeal->id);
        $attachment = $attachment_id > 0 ? $this->Projects->Attachments->getWithProject($attachment_id, $project->id) : $this->Projects->Attachments->newEmptyEntity();
        $attachmentTypes = $this->Projects->Attachments->AttachmentTypes->find('list');

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $file = $this->getRequest()->getUploadedFile('filedata');
            $attachment = $this->Projects->Attachments->patchEntity($attachment, $this->getRequest()->getData() + [
                    'project_id' => $project->id
                ]
            );
            if ($file instanceof UploadedFile) {
                if ($file->getError() !== UPLOAD_ERR_OK) {
                    $attachment->setError('filedata', __('Soubor se nepodařilo nahrát'));
                } else {
                    $attachment->filepath = $attachment->fileUploadMove($file, $organization->id, $project->id);
                    $attachment->filesize = $file->getSize();
                    $attachment->original_filename = $file->getClientFilename();
                }
            }
            if ($this->Projects->Attachments->save($attachment)) {
                if ($attachment->attachment_type_id === AttachmentType::TYPE_PHOTO && $this->getRequest()->getData('is_title_image')) {
                    $project->title_image_id = $attachment->id;
                    $this->Projects->save($project);
                }
                $this->resetAppropriateCache($organization, $appeal);
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['action' => 'detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id]);
            } else {
                if ($attachment->isNew()) {
                    $attachment->deletePhysically();
                }
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'appeal', 'project', 'attachment', 'attachmentTypes'));
        $this->set('crumbs', [
            __('Moje organizace') => 'my_organizations',
            $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id],
            sprintf("%s %d", __('Ročník'), $appeal->year) => ['_name' => 'appeal_detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id],
            $project->name => ['action' => 'detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id],
        ]);
    }

    public function deleteAttachment(int $organization_id, int $appeal_id, int $project_id, int $attachment_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        $project = $this->Projects->getWithAppeal($project_id, $appeal->id);
        $attachment = $this->Projects->Attachments->getWithProject($attachment_id, $project->id);
        if ($this->Projects->Attachments->delete($attachment)) {
            $attachment->deletePhysically();
            $this->resetAppropriateCache($organization, $appeal);
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }
        $this->redirect($this->referer());
    }

    public function downloadAttachment(int $organization_id, int $appeal_id, int $project_id, int $attachment_id, bool $forceDownload = false)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        $project = $this->Projects->getWithAppeal($project_id, $appeal->id);
        $attachment = $this->Projects->Attachments->getWithProject($attachment_id, $project->id);
        $path = $attachment->getRealPath();
        if (is_file($path)) {
            return $this->getResponse()->withType($attachment->getMimeType())->withFile($attachment->getRealPath(), [
                'download' => $forceDownload,
                'name' => $attachment->original_filename,
            ]);
        }
        return $this->getResponse()->withStatus(404);
    }

}
