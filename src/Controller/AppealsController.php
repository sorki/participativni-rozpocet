<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Appeal;
use App\Model\Table\AppealsTable;
use App\Model\Table\OrganizationsTable;
use Cake\Http\Response;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read AppealsTable $Appeals
 */
class AppealsController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('Appeals');
    }

    public function addModify(int $organization_id, ?int $appeal_id = null)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $appeal_id > 0 ? $this->Appeals->getWithOrganization($appeal_id, $organization->id) : $this->Appeals->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $appeal = $this->Appeals->patchEntity($appeal, $this->getRequest()->getData() + [
                    Appeal::FIELD_ORGANIZATION_ID => $organization->id,
                ]
            );
            if ($this->Appeals->save($appeal)) {
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization->id]);
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_APPEALS, [$organization->id, $appeal->id]);
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['_name' => 'appeal_detail', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'appeal'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function detail(int $organization_id, int $appeal_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id, ['Phases', 'Phases.IdentityProviders', 'Projects', 'Projects.ProjectStatus']);

        $this->set(compact('organization', 'appeal'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    /**
     * Duplicate appeal along with the settings (phases and
     *
     * @param int $organization_id
     * @param int $appeal_id
     * @return Response|null
     */
    public function makeCopy(int $organization_id, int $appeal_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id, ['Phases', 'Phases.IdentityProviders']);

        $appeal->unset(['id', 'created', 'modified'])->setNew(true);
        $appeal->set(Appeal::FIELD_YEAR, '0');
        foreach ($appeal->phases ?? [] as $phase) {
            $phase->unset(['id', 'created', 'modified', 'appeal_id'])->setNew(true);
        }

        if ($this->Appeals->save($appeal)) {
            $this->Flash->success(self::FLASH_SUCCESS_SAVE);
            $this->Flash->set(__('Nastavte prosím ročník této kopie'), ['params' => ['class' => 'alert-info']]);
            return $this->redirect(['action' => 'addModify', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id]);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_SAVE);
            return $this->redirect(['action' => 'detail', 'organization_id' => $organization->id, 'appeal_id' => $appeal_id]);
        }
    }

    public function delete(int $organization_id, int $appeal_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $appeal = $this->Appeals->getWithOrganization($appeal_id, $organization->id);
        if ($this->Appeals->delete($appeal)) {
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }
        return $this->redirect($this->referer());
    }

}
